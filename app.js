const express = require('express');
const cors = require('cors');

global.logger = require('./config/log');

const port = 7002;
const HOST = '0.0.0.0'
const service = 'HRM Service'

const sampleAPIRouter = require('./routes/sampleAPIRoutes');
const employeeRegistrationRouter = require('./routes/employeeRegistrationRoutes');
const commonAPIRouters = require('./routes/commonAPIRouters');
const employeeViewRouter = require('./routes/employeeViewRoutes');
const salaryRouter = require('./routes/salaryRoutes');
const attendanceRouter = require('./routes/attendanceRoutes');
const hrCalendarRouter = require('./routes/hrCalendarRoutes');
const leaveRouter = require('./routes/leaveRoutes');
const matrixRouter = require('./routes/matrixRoutes');
const leaveManagementRoutes = require('./routes/leaveManagementRoutes');
const loanRoutes = require('./routes/loanRoutes');
const assetRoutes = require('./routes/assetRoutes');
const companyRoutes = require('./routes/companyRoutes');
const travelRoutes = require('./routes/travelRoutes');
const metaDataRoutes = require('./routes/metaDataRoutes');
const masterDataRoutes = require('./routes/masterDataRoutes');
const shiftRoutes = require('./routes/shiftRoutes');
const announcementRoutes = require('./routes/announcementRoutes');
const attendanceRoutes = require('./routes/attendanceRoutes');
const groupRoutes = require('./routes/groupRoutes');
const rosterRoutes = require('./routes/rosterRoutes');
const trainingSetupRoutes = require('./routes/trainingSetupRoutes');
const employeeSettlementRoutes = require('./routes/employeeSettlementRoutes');
const dashboardRoutes = require('./routes/dashboardRoutes');



const app = express();
app.use(cors());
app.use(express.json());

app.use((req, res,next)=>{
    var requestedUrl = req.protocol + '://' + req.get('Host') + req.url;
    let log = service + ' recived request. ' + requestedUrl;
    if(req.query && req.query.user){
        log = log + ', by user : ' + req.query.user
    }
    logger.info(log);
    next();
});

app.use('/sampleAPI', sampleAPIRouter);
app.use('/employeeRegistration', employeeRegistrationRouter);
app.use('/commonAPI', commonAPIRouters);
app.use('/employeeView', employeeViewRouter);
app.use('/salary', salaryRouter);
app.use('/attendance', attendanceRouter);
app.use('/calendar', hrCalendarRouter);
app.use('/leave', leaveRouter);
app.use('/matrices', matrixRouter);
app.use('/leaveManagement', leaveManagementRoutes);
app.use('/loan', loanRoutes);
app.use('/asset', assetRoutes);
app.use('/company', companyRoutes);
app.use('/travel',travelRoutes);
app.use('/metaData',metaDataRoutes);
app.use('/masterData',masterDataRoutes);
app.use('/shift',shiftRoutes); 
app.use('/announcement',announcementRoutes);
app.use('/attendance',attendanceRoutes);
app.use('/group',groupRoutes);
app.use('/roster',rosterRoutes); 
app.use('/trainingSetup',trainingSetupRoutes);
app.use('/settleEmployee',employeeSettlementRoutes);
app.use('/dashboard',dashboardRoutes);


app.use((req, res, next) => {
    var requestedUrl = req.protocol + '://' + req.get('Host') + req.url;
    logger.error('Inside \'resource not found\' handler , Req resource: ' + requestedUrl);
    return res.status(404).send({success:false,message:'Url Not found'});
});

// error handler
app.use((err, req, res, next) => {
    logger.error('Error handler:', err);
    return res.status(500).send({success:false, message:'Error'});
});

app.listen(port, HOST, ()=>{
    logger.info(`Running on http://${HOST}:${port}`);
    logger.info('Server started at '+ port);
})
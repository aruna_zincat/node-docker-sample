// // get the client
const mysql = require('mysql2/promise');
 
// Create the connection pool. The pool-specific settings are the defaults
const pool = mysql.createPool({
 //host: '192.169.143.105',
  // host: 'localhost',
  host: '192.169.143.105',
  user: 'root',
   database: 'h2biz_18_dev',
 //database: 'QA_h2biz_hrm',
  waitForConnections: true,
  connectionLimit: 100,
  queueLimit: 0,
  password : 'xyz@XYZ1cat1',
  /* dateStrings: [
    'DATE',
    'DATETIME'
] */
  // password : '1234'
});

pool.on('acquire', function (connection) {
  logger.info('Connection %d acquired', connection.threadId);
});

pool.on('enqueue', function () {
  logger.info('Waiting for available connection slot');
});

pool.on('release', function (connection) {
  logger.info('Connection %d released', connection.threadId);
});






// const pool = mysql.createPool({
//   host: 'localhost',
//   user: 'root',
//   database: 'h2biz_18_dev',
//   waitForConnections: true,
//   connectionLimit: 100,
//   queueLimit: 0,
//   password : '1234'
// })


var getConnection = function (callback) {
  pool.getConnection(function (err, connection) {
      callback(err, connection);
  });
};

module.exports.getConnection = getConnection;
module.exports.pool = pool;
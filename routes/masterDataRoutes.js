var express = require('express');
var router = express.Router();
var pool = require('../config/database').pool;

////////////////////////////////////Designation//////////////////////////////////////////
// get Designation details
router.post('/getDesignations', async function (req, res, next) {
    const API_NAME = 'getDesignations POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Designation
        const [resultData, fields] = await conn.query("select * from hrm_defaults_employee_designation where IS_ACTIVE=1 and DESIGNATION like '%" + receivedObj.searchVal + "%' order by ID_EMPLOYEE_DESIGNATION desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_employee_designation where IS_ACTIVE=1 and DESIGNATION like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT DESIGNATION DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET DESIGNATION DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Designation
router.post('/saveDesignation', async function (req, res, next) {
    const API_NAME = 'saveDesignation POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_employee_designation where IS_ACTIVE=1 and DESIGNATION='" + receivedObj.designation + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('DESIGNATION ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idDesignation || null == receivedObj.idDesignation || 0 == receivedObj.idDesignation) {

            } else {
                // update old Designation details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_designation set IS_ACTIVE=0 where ID_EMPLOYEE_DESIGNATION=" + receivedObj.idDesignation + "");
                logger.info('Successfully Updated Designation detail record ');
            }

            // set data to insert 
            var designation = {
                DESIGNATION: receivedObj.designation,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_employee_designation SET ?', designation);
            logger.info('Successfully saved Designation record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('DESIGNATION CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('DESIGNATION CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete Designation
router.post('/actionDesignation', async function (req, res, next) {
    const API_NAME = 'actionDesignation POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Designation details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_designation set IS_ACTIVE=0 where ID_EMPLOYEE_DESIGNATION=" + receivedObj.idDesignation + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_designation set STATUS=1 where ID_EMPLOYEE_DESIGNATION=" + receivedObj.idDesignation + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_designation set STATUS=0 where ID_EMPLOYEE_DESIGNATION=" + receivedObj.idDesignation + "");
                }

        logger.info('Successfully Updated Designation detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('DESIGNATION ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('DESIGNATION ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

///////////////////////////////////Location//////////////////////////////////////////
// get Location details
router.post('/getLocations', async function (req, res, next) {
    const API_NAME = 'getLocations POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Location
        const [resultData, fields] = await conn.query("select * from hrm_defaults_location where IS_ACTIVE=1 and LOCATION_NAME like '%" + receivedObj.searchVal + "%' order by ID_LOCATION desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_location where IS_ACTIVE=1 and LOCATION_NAME like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT LOCATION DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET LOCATION DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Location
router.post('/saveLocation', async function (req, res, next) {
    const API_NAME = 'saveLocation POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_location where IS_ACTIVE=1 and LOCATION_NAME='" + receivedObj.location + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('LOCATION ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idLocation || null == receivedObj.idLocation || 0 == receivedObj.idLocation) {

            } else {
                // update old Location details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_location set IS_ACTIVE=0 where ID_LOCATION=" + receivedObj.idLocation + "");
                logger.info('Successfully Updated Designation detail record ');
            }

            // set data to insert 
            var location = {
                LOCATION_NAME: receivedObj.location,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_location SET ?', location);
            logger.info('Successfully saved Location record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('LOCATION CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('LOCATION CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete LOCATION
router.post('/actionLocation', async function (req, res, next) {
    const API_NAME = 'actionLocation POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Location details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_location set IS_ACTIVE=0 where ID_LOCATION=" + receivedObj.idLocation + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_location set STATUS=1 where ID_LOCATION=" + receivedObj.idLocation + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_location set STATUS=0 where ID_LOCATION=" + receivedObj.idLocation + "");
                }

        logger.info('Successfully Updated Location detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('LOCATION ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('LOCATION ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

///////////////////////////////////Department//////////////////////////////////////////
// get Department details
router.post('/getDepartments', async function (req, res, next) {
    const API_NAME = 'getDepartments POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Location
        const [resultData, fields] = await conn.query("select * from hrm_defaults_department where IS_ACTIVE=1 and DEPARTMENT like '%" + receivedObj.searchVal + "%' order by ID_DEPARTMENT desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_department where IS_ACTIVE=1 and DEPARTMENT like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT DEPARTMENT DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET DEPARTMENT DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Department
router.post('/saveDepartment', async function (req, res, next) {
    const API_NAME = 'saveDepartment POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_department where IS_ACTIVE=1 and DEPARTMENT='" + receivedObj.department + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('DEPARTMENT ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idDepartment || null == receivedObj.idDepartment || 0 == receivedObj.idDepartment) {

            } else {
                // update old Department details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_department set IS_ACTIVE=0 where ID_DEPARTMENT=" + receivedObj.idDepartment + "");
                logger.info('Successfully Updated Department detail record ');
            }

            // set data to insert 
            var department = {
                DEPARTMENT: receivedObj.department,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_department SET ?', department);
            logger.info('Successfully saved Department record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('DEPARTMENT CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('DEPARTMENT CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete department
router.post('/actionDepartment', async function (req, res, next) {
    const API_NAME = 'actionDepartment POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Department details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_department set IS_ACTIVE=0 where ID_DEPARTMENT=" + receivedObj.idDepartment + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_department set STATUS=1 where ID_DEPARTMENT=" + receivedObj.idDepartment + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_department set STATUS=0 where ID_DEPARTMENT=" + receivedObj.idDepartment + "");
                }

        logger.info('Successfully Updated Department detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('DEPARTMENT ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('DEPARTMENT ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});


///////////////////////////////////Employment//////////////////////////////////////////
// get Employment details
router.post('/getEmployments', async function (req, res, next) {
    const API_NAME = 'getEmployments POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Location
        const [resultData, fields] = await conn.query("select * from hrm_employee_employment where IS_ACTIVE=1 and EMPLOYMENT_NAME like '%" + receivedObj.searchVal + "%' order by ID_EMPLOYMENT desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_employee_employment where IS_ACTIVE=1 and EMPLOYMENT_NAME like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYMENT DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYMENT DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save eMPLOYMENT
router.post('/saveEmployment', async function (req, res, next) {
    const API_NAME = 'saveEmployment POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_employee_employment where IS_ACTIVE=1 and EMPLOYMENT_NAME='" + receivedObj.employment + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('EMPLOYMENT ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idEmployment || null == receivedObj.idEmployment || 0 == receivedObj.idEmployment) {

            } else {
                // update old Employment details row
                const [resultUpdateData, fields] = await conn.query("update hrm_employee_employment set IS_ACTIVE=0 where ID_EMPLOYMENT=" + receivedObj.idEmployment + "");
                logger.info('Successfully Updated Employment detail record ');
            }

            // set data to insert 
            var employment = {
                EMPLOYMENT_NAME: receivedObj.employment,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_employee_employment SET ?', employment);
            logger.info('Successfully saved Employment record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('EMPLOYMENT CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('EMPLOYMENT CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete employment
router.post('/actionEmployment', async function (req, res, next) {
    const API_NAME = 'actionEmployment POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Employment details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_employee_employment set IS_ACTIVE=0 where ID_EMPLOYMENT=" + receivedObj.idEmployment + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_employee_employment set STATUS=1 where ID_EMPLOYMENT=" + receivedObj.idEmployment + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_employee_employment set STATUS=0 where ID_EMPLOYMENT=" + receivedObj.idEmployment + "");
                }

        logger.info('Successfully Updated Employment detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('EMPLOYMENT ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('EMPLOYMENT ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

///////////////////////////////////Grade//////////////////////////////////////////
// get Grade details
router.post('/getGrades', async function (req, res, next) {
    const API_NAME = 'getGrades POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Grade
        const [resultData, fields] = await conn.query("select * from hrm_defaults_employee_grade where IS_ACTIVE=1 and (GRADE_NAME like '%" + receivedObj.searchVal + "%' or DEGREE_EQUIVALENT like '%" + receivedObj.searchVal + "%' or VACATIONAL_TRANING like '%" + receivedObj.searchVal + "%' or HIGHER_DEPLOMA like '%" + receivedObj.searchVal + "%' or DEPLOMA like '%" + receivedObj.searchVal + "%' or AL like '%" + receivedObj.searchVal + "%' or OL like '%" + receivedObj.searchVal + "%') order by ID_EMPLOYEE_GRADE desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select * from hrm_defaults_employee_grade where IS_ACTIVE=1 and (GRADE_NAME like '%" + receivedObj.searchVal + "%' or DEGREE_EQUIVALENT like '%" + receivedObj.searchVal + "%' or VACATIONAL_TRANING like '%" + receivedObj.searchVal + "%' or HIGHER_DEPLOMA like '%" + receivedObj.searchVal + "%' or DEPLOMA like '%" + receivedObj.searchVal + "%' or AL like '%" + receivedObj.searchVal + "%' or OL like '%" + receivedObj.searchVal + "%')");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT GRADE DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET GRADE DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Grade
router.post('/saveGrade', async function (req, res, next) {
    const API_NAME = 'saveGrade POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj);
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');
        var insertGradeId = 0;

        const [resultData, fields] = await conn.query("select * from hrm_defaults_employee_grade where IS_ACTIVE=1 and GRADE_NAME='" + receivedObj.basicValues.grade + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('GRADE ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.basicValues.idGrade || null == receivedObj.basicValues.idGrade || 0 == receivedObj.basicValues.idGrade) {

            } else {
                // update old Grade details row
                const [resultUpdateData, fields1] = await conn.query("update hrm_defaults_employee_grade set IS_ACTIVE=0 where ID_EMPLOYEE_GRADE=" + receivedObj.basicValues.idGrade + "");
                logger.info('Successfully Updated Grade detail record ');
                // insertGradeId = receivedObj.basicValues.idGrade;
            }

            // set basic data to insert 
            var grade = {
                GRADE_NAME: receivedObj.basicValues.grade,
                IS_DEGREE_EQUIVALENT: receivedObj.basicValues.isDegree,
                DEGREE_EQUIVALENT: receivedObj.basicValues.degree,
                IS_VACATIONAL_TRANING: receivedObj.basicValues.isVacatinolTraning,
                VACATIONAL_TRANING: receivedObj.basicValues.vacatinolTraning,
                IS_HIGHER_DEPLOMA: receivedObj.basicValues.isHigherDeploma,
                HIGHER_DEPLOMA: receivedObj.basicValues.higherDeploma,
                IS_DEPLOMA: receivedObj.basicValues.isDeploma,
                DEPLOMA: receivedObj.basicValues.deploma,
                IS_AL: receivedObj.basicValues.isAl,
                AL: receivedObj.basicValues.al,
                IS_OL: receivedObj.basicValues.isOl,
                OL: receivedObj.basicValues.ol,
                IS_EXPERIENCE_TO_SIMILER: receivedObj.basicValues.isExperienceOfSimilerCapacity,
                EXPERIENCE_TO_SIMILER: receivedObj.basicValues.experienceOfSimilerCapacity,
                YEAR: receivedObj.basicValues.year,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields2] = await conn.query('INSERT INTO hrm_defaults_employee_grade SET ?', grade);
            insertGradeId = resultSave.insertId;
            logger.info('Successfully saved Grade record id = ' + resultSave.insertId);

            // set other qualification data to insert
            for (var i = 0; i < receivedObj.otherQualification.length; i++) {
                var otherQualification = {
                    ID_EMPLOYEE_GRADE: insertGradeId,
                    QULIFICATION_NAME: receivedObj.otherQualification[i].QULIFICATION_NAME,
                    QULIFICATION_VALUE: receivedObj.otherQualification[i].QULIFICATION_VALUE,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                }

                // insert data into table
                const [resultOtherQualificationSave, fields1] = await conn.query('INSERT INTO hrm_defaults_employee_grade_other_qulification SET ?', otherQualification);
                logger.info('Successfully saved Other qualification record id = ' + resultOtherQualificationSave.insertId);

            }

            // set offers data to insert
            for (var j = 0; j < receivedObj.offer.length; j++) {
                var offers = {
                    ID_EMPLOYEE_GRADE: insertGradeId,
                    OFFER_NAME: receivedObj.offer[j].OFFER_NAME,
                    OFFER_VALUE: receivedObj.offer[j].OFFER_VALUE,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                }

                // insert data into table
                const [resultOfferSave, fields3] = await conn.query('INSERT INTO hrm_defaults_employee_grade_offers SET ?', offers);
                logger.info('Successfully saved Offer record id = ' + resultOfferSave.insertId);

            }

            await conn.query('COMMIT');
            conn.release();
            logger.info('GRADE CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('GRADE CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// action grade
router.post('/actionGrade', async function (req, res, next) {
    const API_NAME = 'actionGrade POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Grade details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_grade set IS_ACTIVE=0 where ID_EMPLOYEE_GRADE=" + receivedObj.idGrade + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_grade set STATUS=1 where ID_EMPLOYEE_GRADE=" + receivedObj.idGrade + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_employee_grade set STATUS=0 where ID_EMPLOYEE_GRADE=" + receivedObj.idGrade + "");
                }

        logger.info('Successfully Updated Grade detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('GRADE ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('GRADE ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get other qualification data and offer data where grade id
router.post('/getOtherQualificationWhereGradeId', async function (req, res, next) {
    const API_NAME = 'getOtherQualificationWhereGradeId POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Other qualification
        const [resultDataOtherQualifications, fields1] = await conn.query("select * from hrm_defaults_employee_grade_other_qulification where IS_ACTIVE=1 and ID_EMPLOYEE_GRADE=" + receivedObj.idGrade + "");

        // get Offers
        const [resultDataOffers, fields2] = await conn.query("select * from hrm_defaults_employee_grade_offers where IS_ACTIVE=1 and ID_EMPLOYEE_GRADE=" + receivedObj.idGrade + "");

        var resultDataObj = {
            resultDataOtherQualifications: resultDataOtherQualifications,
            resultDataOffers: resultDataOffers
        }

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT GRADE OTHER QUALIFICATION AND OFFERS DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET GRADE OTHER QUALIFICATION AND OFFERS DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;
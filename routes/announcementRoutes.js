var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;




router.post('/createNewAnnouncement', async function (req, res, next) {
    //console.log(req.body);
    const API_NAME = 'createNewAnnouncement post ';
    //logger.info("requested data",req.body)
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

   console.log("requested data",req.body)

   // return res.status(200).send({ success: true, message: ' New Travel Request' });
    try {
    const conn = await pool.getConnection();
    const [announcementDetails,fields]=await conn.query('INSERT INTO hrm_announcement SET ?', req.body);
    logger.info("SUCCESSFULLY INSERTED ANNOUNCEMENT")
   
    await conn.query('COMMIT');
    conn.release();
    return res.status(200).send({ success: true, message: ' NEW ANNOUNCEMENT CREATED' });
    }
    catch (err) {
    logger.error(API_NAME + 'error :' + err);
    await conn.query('ROLLBACK');
    conn.release();
    return res.status(500).send({ success: false, message: 'Query Error' });
    }
    });


router.get('/getAnnouncementCount', async function (req, res, next) {
        const API_NAME = 'getAnnouncementCount get, ';
    
        logger.debug(JSON.stringify(req.body));
    
        //*********always log inside which api*********
        logger.info(API_NAME + 'called');
    
        try {
            const conn = await pool.getConnection();
            
            // Do something with the connection
            var sql = ` SELECT COUNT(ID_ANNOUNCEMENT) AS TOTAL FROM hrm_announcement WHERE IS_ACTIVE = '1'`;
            logger.debug(sql);
    
            var promise1 = conn.execute(sql);
    
            const values = await Promise.all([promise1]);
            logger.debug(promise1.sql);
            conn.release();
    
            return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
        }
    
        catch (err) {
            logger.error(API_NAME + 'error :' + err);
            logger.error('Error ' + JSON.stringify(err));
            return res.status(500).send({ success: false, message: 'Query Error' });
        }
    });


   
    router.post('/getAnnouncement', async function (req, res, next) {
        const API_NAME = 'getAnnouncement post, ';
    
        logger.debug(JSON.stringify(req.body));
    
        //*********always log inside which api*********
        logger.info(API_NAME + 'called');
    
        let sqlFilter = ``;
    
        //*********always check req data before accessing them directly*********
    
        //logic here
        try {
            const conn = await pool.getConnection();
    
            let filtersArray = [];
            let filterColumnsArray = [];
            if (req.body.filters) {
                for (var key in req.body.filters) {
                    if (req.body.filters.hasOwnProperty(key)) {
                        sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                        filtersArray.push(req.body.filters[key]);
                    }
                }
            }
    
            if (req.body.sortOrder && req.body.sortField) {
                if (req.body.sortOrder == 'ASC') {
                    sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
                }
                if (req.body.sortOrder == 'DESC') {
                    sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                    filterColumnsArray.push(req.body.sortField);
                }
            }
            if (req.body.results) {
                sqlFilter = sqlFilter + ` LIMIT ?`;
                filtersArray.push(req.body.results)
            }
            if (req.body.page) {
                sqlFilter = sqlFilter + ` OFFSET ?`;
                filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
            }
    
            logger.debug(sqlFilter);
            logger.debug('Filter array ' + JSON.stringify(filtersArray));
            // Do something with the connection
            var sql = ` SELECT * FROM view_announcement WHERE IS_ACTIVE = '1'   ` + sqlFilter;
            logger.debug(sql);
    
            var promise1 = conn.execute(sql
                , [...filtersArray]);
    
            //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');
    
            const values = await Promise.all([promise1]);
            logger.debug(promise1.sql);
            conn.release();
    
            return res.status(200).send({ success: true, data: values[0][0] });
        }
    
        catch (err) {
            logger.error(API_NAME + 'error :' + err);
            logger.error('Error ' + JSON.stringify(err));
            return res.status(500).send({ success: false, message: 'Query Error' });
        }
    });



/* 
router.get('/getTransportationMethods', async function (req, res) {

    const API_NAME = 'getTransportationMethods GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultTransportationMethods, fields] = await conn.query(` SELECT * FROM hrm_transporation_method WHERE IS_ACTIVE = '1'`);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TRANSPORTATION METHODS UCCESSFULLY...');
        return res.json({ success: true, resultTransportationMethods });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TRANSPORTATION METHODS DUE TO :- ', err);
        return res.json({ success: false });
    }

});



    









//get travel request participants by using travel request id
router.post('/getParticipantsByID', async function (req, res) {

    const API_NAME = 'getParticipantsByID POST, ';
    logger.info(API_NAME + ' called');

    console.log("request data",req.body)

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultParticipants, fields] = await conn.query(` SELECT * FROM view_travel_request_participants WHERE IS_ACTIVE = '1' AND ID_TRAVEL_REQUEST = `+req.body.ID_TRAVEL_REQUEST);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TRANSPORTATION METHODS UCCESSFULLY...');
        return res.json({ success: true, resultParticipants });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TRANSPORTATION METHODS DUE TO :- ', err);
        return res.json({ success: false });
    }

});





router.post('/updateApproval', async function (req, res, next) {
    const API_NAME = 'updateHodApproval post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    console.log(req.body);

   // req.body.data.HOD_STATUS = req.body.data.HOD_STATUS.toString();
  // req.body.data.HOD_COMMENT = req.body.data.HOD_COMMENT.toString();
   



    try {
        const conn = await pool.getConnection();

        
        await conn.query('UPDATE hrm_travel_request SET ? where `ID_TRAVEL_REQUEST`='+req.body.ID, req.body.data);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'HOD Status Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});




router.post('/deleteTravelRequest', async function (req, res, next) {
    const API_NAME = 'deleteTravelRequest post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        await conn.query('UPDATE hrm_travel_request SET `IS_ACTIVE`=0 where `ID_TRAVEL_REQUEST`='+req.body.ID);
        logger.info("HRM TRAVEL REQUEST TABLE UPDATED")

        await conn.query('UPDATE hrm_travel_request_participant SET `IS_ACTIVE`=0 where `ID_TRAVEL_REQUEST`='+req.body.ID);
        logger.info("HRM TRAVEL REQUEST TABLE UPDATED")

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Travel Request Deleted' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});
 */




module.exports = router;
var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

//////////////////////////////////Country////////////////////////////////


// get countries all
router.get('/getAllCountries', async function (req, res) {

    const API_NAME = 'getAllCountries GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get countries
        const [result, fields] = await conn.query('select * from hrm_defaults_country where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL COUNTRIES SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ALL COUNTRIES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Country details
router.post('/getCountries', async function (req, res, next) {
    const API_NAME = 'getCountries POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get countries
        const [resultData, fields] = await conn.query("select * from hrm_defaults_country where IS_ACTIVE=1  and (COUNTRY_CODE like '%" + receivedObj.searchVal + "%' or COUNTRY_NAME like '%" + receivedObj.searchVal + "%') order by ID_COUNTRY desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_country where IS_ACTIVE=1 and (COUNTRY_CODE like '%" + receivedObj.searchVal + "%' or COUNTRY_NAME like '%" + receivedObj.searchVal + "%')");

        // resultData.count = resultDataCount[0].count_of;
        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT COUNTRY DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET COUNTRY DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save country
router.post('/saveCountry', async function (req, res, next) {
    const API_NAME = 'saveCountry POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_country where IS_ACTIVE=1 and COUNTRY_CODE='" + receivedObj.code + "' and COUNTRY_NAME='" + receivedObj.country + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('COUNTRY ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {
            if (undefined == receivedObj.idCountry || null == receivedObj.idCountry || 0 == receivedObj.idCountry) {

            } else {
                // update old country details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_country set IS_ACTIVE=0  where ID_COUNTRY=" + receivedObj.idCountry + "");
                logger.info('Successfully Updated Country detail record ');
            }

            // set data to insert 
            var country = {
                COUNTRY_CODE: receivedObj.code,
                COUNTRY_NAME: receivedObj.country,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_country SET ?', country);
            logger.info('Successfully saved Country record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('COUNTRY CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('COUNTRY CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete country
router.post('/actionCountry', async function (req, res, next) {
    const API_NAME = 'actionCountry POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old country details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_country set IS_ACTIVE=0 where ID_COUNTRY=" + receivedObj.idCountry + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_country set STATUS=1 where ID_COUNTRY=" + receivedObj.idCountry + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_country set STATUS=0 where ID_COUNTRY=" + receivedObj.idCountry + "");
                }

        logger.info('Successfully Updated Country detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('COUNTRY DELETION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('COUNTRY DELETION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

//////////////////////////////////Provence////////////////////////////////
// get Provence details
router.post('/getProvences', async function (req, res, next) {
    const API_NAME = 'getProvences POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log('rec ', receivedObj)
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get countries
        const [resultData, fields] = await conn.query("select * from hrm_defaults_provence where IS_ACTIVE=1 and (PROVENCE_CODE like '%" + receivedObj.searchVal + "%' or PROVENCE_NAME like '%" + receivedObj.searchVal + "%' or COUNTRY like '%" + receivedObj.searchVal + "%') order by ID_PROVENCE desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_provence where IS_ACTIVE=1 and (PROVENCE_CODE like '%" + receivedObj.searchVal + "%' or PROVENCE_NAME like '%" + receivedObj.searchVal + "%' or COUNTRY like '%" + receivedObj.searchVal + "%')");

        // resultData.count = resultDataCount[0].count_of;
        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT PROVENCE DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET PROVENCE DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save PROVENCE
router.post('/saveProvence', async function (req, res, next) {
    const API_NAME = 'saveProvence POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj);
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_provence where IS_ACTIVE=1 and COUNTRY='" + receivedObj.country + "' and PROVENCE_CODE='" + receivedObj.code + "' and PROVENCE_NAME='" + receivedObj.provence + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('PROVENCE ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {
            if (undefined == receivedObj.idProvence || null == receivedObj.idProvence || 0 == receivedObj.idProvence) {

            } else {
                // update old provence details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_provence set IS_ACTIVE=0  where ID_PROVENCE=" + receivedObj.idProvence + "");
                logger.info('Successfully Updated Provence detail record ');
            }

            // set data to insert 
            var provence = {
                COUNTRY: receivedObj.country,
                PROVENCE_CODE: receivedObj.code,
                PROVENCE_NAME: receivedObj.provence,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_provence SET ?', provence);
            logger.info('Successfully saved Provence record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('PROVENCE CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('PROVENCE CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete provence
router.post('/actionProvence', async function (req, res, next) {
    const API_NAME = 'actionProvence POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old country details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_provence set IS_ACTIVE=0 where ID_PROVENCE=" + receivedObj.idProvence + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_provence set STATUS=1 where ID_PROVENCE=" + receivedObj.idProvence + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_provence set STATUS=0 where ID_PROVENCE=" + receivedObj.idProvence + "");
                }

        logger.info('Successfully Updated Provence detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('PROVENCE ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('PROVENCE ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

////////////////////////////////////Currency//////////////////////////////////////////
// get Currency all
router.get('/getAllCurrencies', async function (req, res) {

    const API_NAME = 'getAllCurrencies GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get currencies
        const [result, fields] = await conn.query('select * from hrm_defaults_currency where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL CURRENCIES SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL CURRENCIES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Currency details
router.post('/getCurrencies', async function (req, res, next) {
    const API_NAME = 'getCurrencies POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get Currencies
        const [resultData, fields] = await conn.query("select * from hrm_defaults_currency where IS_ACTIVE=1 and (CURRENCY_CODE like '%" + receivedObj.searchVal + "%' or CURRENCY_NAME like '%" + receivedObj.searchVal + "%') order by ID_CURRENCY desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_currency where IS_ACTIVE=1 and (CURRENCY_CODE like '%" + receivedObj.searchVal + "%' or CURRENCY_NAME like '%" + receivedObj.searchVal + "%')");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT CURRENCY DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET CURRENCY DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save country
router.post('/saveCurrency', async function (req, res, next) {
    const API_NAME = 'saveCurrency POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_currency where IS_ACTIVE=1 and CURRENCY_CODE='" + receivedObj.code + "' and CURRENCY_NAME='" + receivedObj.currency + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('CURRENCY ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idCurrency || null == receivedObj.idCurrency || 0 == receivedObj.idCurrency) {

            } else {
                // update old country details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_currency set IS_ACTIVE=0 where ID_CURRENCY=" + receivedObj.idCurrency + "");
                logger.info('Successfully Updated Currency detail record ');
            }

            // set data to insert 
            var currency = {
                CURRENCY_CODE: receivedObj.code,
                CURRENCY_NAME: receivedObj.currency,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_currency SET ?', currency);
            logger.info('Successfully saved Currency record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('CURRENCY CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CURRENCY CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete country
router.post('/actionCurrency', async function (req, res, next) {
    const API_NAME = 'actionCurrency POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old country details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_currency set IS_ACTIVE=0 where ID_CURRENCY=" + receivedObj.idCurrency + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_currency set STATUS=1 where ID_CURRENCY=" + receivedObj.idCurrency + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_currency set STATUS=0 where ID_CURRENCY=" + receivedObj.idCurrency + "");
                }

        logger.info('Successfully Updated Currency detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('CURRENCY ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CURRENCY ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

////////////////////////////////////Natinolity//////////////////////////////////////////
// get Currency details
router.post('/getNatinolities', async function (req, res, next) {
    const API_NAME = 'getNatinolities POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Natinolities
        const [resultData, fields] = await conn.query("select * from hrm_defaults_natinolity where IS_ACTIVE=1 and NATINOLITY_NAME like '%" + receivedObj.searchVal + "%' order by ID_NATINOLITY desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_natinolity where IS_ACTIVE=1 and NATINOLITY_NAME like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT NATINOLITY DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET NATINOLITY DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Natinolity
router.post('/saveNatinolity', async function (req, res, next) {
    const API_NAME = 'saveNatinolity POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_natinolity where IS_ACTIVE=1 and NATINOLITY_NAME='" + receivedObj.natinolity + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('NATINOLITY ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idNatinolity || null == receivedObj.idNatinolity || 0 == receivedObj.idNatinolity) {

            } else {
                // update old natinolity details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_natinolity set IS_ACTIVE=0 where ID_NATINOLITY=" + receivedObj.idNatinolity + "");
                logger.info('Successfully Updated Natinolity detail record ');
            }

            // set data to insert 
            var natinolity = {
                NATINOLITY_NAME: receivedObj.natinolity,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_natinolity SET ?', natinolity);
            logger.info('Successfully saved Natinolity record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('NATINOLITY CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('NATINOLITY CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete country
router.post('/actionNatinolity', async function (req, res, next) {
    const API_NAME = 'actionNatinolity POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old natinolity details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_natinolity set IS_ACTIVE=0 where ID_NATINOLITY=" + receivedObj.idNatinolity + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_natinolity set STATUS=1 where ID_NATINOLITY=" + receivedObj.idNatinolity + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_natinolity set STATUS=0 where ID_NATINOLITY=" + receivedObj.idNatinolity + "");
                }

        logger.info('Successfully Updated Natinolity detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('NATINOLITY ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('NATINOLITY ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

////////////////////////////////////Ethinicity//////////////////////////////////////////
// get Ethinicity details
router.post('/getEthinicities', async function (req, res, next) {
    const API_NAME = 'getEthinicities POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Ethinicities
        const [resultData, fields] = await conn.query("select * from hrm_defaults_ethincity where IS_ACTIVE=1 and ETHINCITY_NAME like '%" + receivedObj.searchVal + "%' order by ID_ETHINCITY desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_ethincity where IS_ACTIVE=1 and ETHINCITY_NAME like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ETHINICITY DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ETHINICITY DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Ethinicity
router.post('/saveEthinicity', async function (req, res, next) {
    const API_NAME = 'saveEthinicity POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_ethincity where IS_ACTIVE=1 and ETHINCITY_NAME='" + receivedObj.ethinicity + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('ETHINICITY ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idEthinicity || null == receivedObj.idEthinicity || 0 == receivedObj.idEthinicity) {

            } else {
                // update old ethinicity details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_ethincity set IS_ACTIVE=0 where ID_ETHINCITY=" + receivedObj.idEthinicity + "");
                logger.info('Successfully Updated Ethinicity detail record ');
            }

            // set data to insert 
            var ethinicity = {
                ETHINCITY_NAME: receivedObj.ethinicity,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_ethincity SET ?', ethinicity);
            logger.info('Successfully saved Ethinicity record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('ETHINICITY CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('ETHINICITY CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete ethinicity
router.post('/actionEthinicity', async function (req, res, next) {
    const API_NAME = 'actionEthinicity POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old ethinicity details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_ethincity set IS_ACTIVE=0 where ID_ETHINCITY=" + receivedObj.idEthinicity + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_ethincity set STATUS=1 where ID_ETHINCITY=" + receivedObj.idEthinicity + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_ethincity set STATUS=0 where ID_ETHINCITY=" + receivedObj.idEthinicity + "");
                }

        logger.info('Successfully Updated Ehinicity detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('ETHINICITY ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('ETHINICITY ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

////////////////////////////////////ImmigrationStatus//////////////////////////////////////////
// get ImmigrationStatus details
router.post('/getImmigrationStatus', async function (req, res, next) {
    const API_NAME = 'getImmigrationStatus POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get ImmigrationStatus
        const [resultData, fields] = await conn.query("select * from hrm_defaults_immigration_status where IS_ACTIVE=1 and IMMIGRATION_NAME like '%" + receivedObj.searchVal + "%' order by ID_IMMIGRATION desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from hrm_defaults_immigration_status where IS_ACTIVE=1 and IMMIGRATION_NAME like '%" + receivedObj.searchVal + "%'");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT IMMIGRATION DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET IMMIGRATION DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save ImmigrationStatus
router.post('/saveImmigrationStatus', async function (req, res, next) {
    const API_NAME = 'saveImmigrationStatus POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_defaults_immigration_status where IS_ACTIVE=1 and IMMIGRATION_NAME='" + receivedObj.immigrationStatus + "'");

        if (resultData.length > 0) {

            await conn.query('COMMIT');
            conn.release();
            logger.info('IMMIGRATION ALREADY EXIST !');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            if (undefined == receivedObj.idImmigrationStatus || null == receivedObj.idImmigrationStatus || 0 == receivedObj.idImmigrationStatus) {

            } else {
                // update old ImmigrationStatus details row
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_immigration_status set IS_ACTIVE=0 where ID_IMMIGRATION=" + receivedObj.idImmigrationStatus + "");
                logger.info('Successfully Updated ImmigrationStatus detail record ');
            }

            // set data to insert 
            var immigrationStatus = {
                IMMIGRATION_NAME: receivedObj.immigrationStatus,
                STATUS: 1,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_defaults_immigration_status SET ?', immigrationStatus);
            logger.info('Successfully saved immigrationStatus record id = ' + resultSave.insertId);

            await conn.query('COMMIT');
            conn.release();
            logger.info('IMMIGRATION CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('IMMIGRATION CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete ImmigrationStatus
router.post('/actionImmigrationStatus', async function (req, res, next) {
    const API_NAME = 'actionImmigrationStatus POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old ImmigrationStatus details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_defaults_immigration_status set IS_ACTIVE=0 where ID_IMMIGRATION=" + receivedObj.idImmigrationStatus + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_defaults_immigration_status set STATUS=1 where ID_IMMIGRATION=" + receivedObj.idImmigrationStatus + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_defaults_immigration_status set STATUS=0 where ID_IMMIGRATION=" + receivedObj.idImmigrationStatus + "");
                }

        logger.info('Successfully Updated ImmigrationStatus detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('IMMIGRATION ACTION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('IMMIGRATION ACTION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});


module.exports = router;
var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

router.post('/createNewEmployee', async function (req, res, next) {
    const API_NAME = 'createNewEmployee POST, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log("receivedObj.professionalQualificationDetailObj ", receivedObj);

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');
        var insertEmpRegistryId = 0;

        // IF UPDATE
        if (true == receivedObj.edit && 0 != receivedObj.editEmpRegId) {
            // update employee profile id set IS_ACTIVE=0
            const [resultUpdateProfile, fields1] = await conn.query("update hrm_employee_profile set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee profile record registry id is = ' + receivedObj.editEmpRegId);

            // update emergency contact details set IS_ACTIVE=0
            const [resultUpdateEmergencyContact, fields2] = await conn.query("update hrm_emergency_contact set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee emergency contact record registry id is = ' + receivedObj.editEmpRegId);

            // update insurence benifitted details set IS_ACTIVE=0
            const [resultUpdateInsurenceBenifitted, fields3] = await conn.query("update hrm_insurance_benifitted set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee insurence benifitted record registry id is = ' + receivedObj.editEmpRegId);

            // update dependance details set IS_ACTIVE=0
            const [resultUpdateDepndance, fields4] = await conn.query("update hrm_employee_dependence set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee dependance record registry id is = ' + receivedObj.editEmpRegId);

            // update employee higher education quelification details set IS_ACTIVE=0
            const [resultUpdateHigherEdu, fields5] = await conn.query("update hrm_higher_education_qualification set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee higher education quelification record registry id is = ' + receivedObj.editEmpRegId);

            // update employee education quelification details set IS_ACTIVE=0
            const [resultUpdateEdu, fields6] = await conn.query("update hrm_education_qualification set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee education quelification record registry id is = ' + receivedObj.editEmpRegId);

            // update employee proffessional quelification details set IS_ACTIVE=0
            const [resultUpdateProffessional, fields7] = await conn.query("update hrm_proffessional_qualification set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee proffessional quelification record registry id is = ' + receivedObj.editEmpRegId);

            // update employee document details set IS_ACTIVE=0
            const [resultUpdateDocument, fields8] = await conn.query("update hrm_employee_documents set IS_ACTIVE=0 where ID_EMPLOYEE_REGISTRY=" + receivedObj.editEmpRegId + "");
            logger.info('Successfully update employee document record registry id is = ' + receivedObj.editEmpRegId);

            insertEmpRegistryId = receivedObj.editEmpRegId;

        } else {

            // set data to insert 
            var employeeRegestry = {
                REGISTRY_CODE_PREFIX: 'EMP',
                REGISTRY_CODE: '',
                REGISTRY_CODE_SUFFIX: '',
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into employee registry
            const [resultEmployeeRegistry, fields] = await conn.query('INSERT INTO hrm_employee_registry SET ?', employeeRegestry);
            insertEmpRegistryId = resultEmployeeRegistry.insertId;
            logger.info('Successfully saved employee registry record id = ' + insertEmpRegistryId);

        }

        console.log(">>>>>>>>>>>>>>.. ", insertEmpRegistryId);

        // insert data into employee profile
        var employeePreofile = {
            ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
            ID_EMPLOYEE_DESIGNATION: receivedObj.officeDetailsObj.designation,
            ID_DEPARTMENT: receivedObj.officeDetailsObj.department,
            ID_EMPLOYEE_GRADE: receivedObj.officeDetailsObj.grade,
            ID_LOCATION: receivedObj.officeDetailsObj.location,
            ID_MARITAL_STATUS: receivedObj.personalDetailObj.marriedStatus,
            ID_TITLE: receivedObj.personalDetailObj.title,
            ID_EMPLOYMENT: receivedObj.officeDetailsObj.employment,
            REPORTING_TO: receivedObj.officeDetailsObj.reportingTo,
            IS_LEAVE_ACCEPT_AUTHORIZED: receivedObj.officeDetailsObj.canApproveLeave,
            FIRSTNAME: receivedObj.personalDetailObj.firstName,
            MIDDLENAME: receivedObj.personalDetailObj.middleName,
            LASTNAME: receivedObj.personalDetailObj.lastName,
            ETF_NO: receivedObj.personalDetailObj.etfNo,
            INCOME_TAX_NO: receivedObj.personalDetailObj.incomeTaxNo,
            PASSPORT_NO: receivedObj.personalDetailObj.passportNo,
            PASSPORT_COUNTRY: receivedObj.personalDetailObj.passportCountry,
            IS_ID_NIC: 1,
            NIC_NO: receivedObj.personalDetailObj.nic,
            NIC_ISSUED_DATE: receivedObj.personalDetailObj.issuedDate,
            IMAGE_PATH: receivedObj.officeDetailsObj.imgPath,
            DATE_OF_BIRTH: receivedObj.personalDetailObj.dateOfBirth,
            NATIONALITY: receivedObj.personalDetailObj.nationality,
            MARITAL_STATUS: receivedObj.personalDetailObj.marriedStatus,
            GENDER: receivedObj.personalDetailObj.gender,
            DISPLAY_NAME: receivedObj.personalDetailObj.displayName,
            DRIVING_LICENSE: receivedObj.personalDetailObj.drivingLicence,
            EMP_STATUS: 1,
            INITIALS: receivedObj.personalDetailObj.initial,
            SALARY_BANK_NAME: receivedObj.bankDetailObj.bankName,
            SALARY_BANK_SWIFTCODE: receivedObj.bankDetailObj.SWIFTCode,
            SALARY_BANK_CODE: receivedObj.bankDetailObj.bankCode,
            SALARY_BANK_BRANCH_NAME: receivedObj.bankDetailObj.branchName,
            SALARY_BANK_BRANCH_CODE: receivedObj.bankDetailObj.branchCode,
            SALARY_BANK_ACCOUNT_NUMBER: receivedObj.bankDetailObj.accountNo,
            ALTERNATE_BANK_NAME: receivedObj.bankDetailObj.alBankName,
            ALTERNATE_BANK_SWIFTCODE: receivedObj.bankDetailObj.alSWIFTCode,
            ALTERNATE_BANK_CODE: receivedObj.bankDetailObj.alBankCode,
            ALTERNATE_BANK_BRANCH_NAME: receivedObj.bankDetailObj.alBranchName,
            ALTERNATE_BANK_BRANCH_CODE: receivedObj.bankDetailObj.alBranchCode,
            ALTERNATE_BANK_ACCOUNT_NUMBER: receivedObj.bankDetailObj.alAccountNo,
            PAY_TYPE: receivedObj.bankDetailObj.payType,
            TYPE_OF_ACCOUNT: receivedObj.bankDetailObj.typeOfAccount,
            ACCOUNT_HOLDER_NAME: receivedObj.bankDetailObj.accountHolderName,
            ACCOUNT_HOLDER_RELATIONSHIP: receivedObj.bankDetailObj.accountHolderRelationship,
            CURRENT_ADDRESS: receivedObj.contactDetailObj.basicContactDetails.currentAddress,
            CURRENT_ADDRESS_COUNTRY_CODE: receivedObj.contactDetailObj.basicContactDetails.curCountryCode,
            CURRENT_ADDRESS_STATE_PROVINCE: receivedObj.contactDetailObj.basicContactDetails.curStateProvince,
            CURRENT_ADDRESS_CITY: receivedObj.contactDetailObj.basicContactDetails.curCity,
            CURRENT_ADDRESS_ZIPCODE: receivedObj.contactDetailObj.basicContactDetails.curZipCode,
            PERMANENT_ADDRESS: receivedObj.contactDetailObj.basicContactDetails.permanentAddress,
            PERMANENT_ADDRESS_COUNTRY_CODE: receivedObj.contactDetailObj.basicContactDetails.perCountryCode,
            PERMANENT_ADDRESS_STATE_PROVINCE: receivedObj.contactDetailObj.basicContactDetails.perStateProvince,
            PERMANENT_ADDRESS_CITY: receivedObj.contactDetailObj.basicContactDetails.perCity,
            PERMANENT_ADDRESS_ZIPCODE: receivedObj.contactDetailObj.basicContactDetails.perZipCode,
            MOBILE: receivedObj.contactDetailObj.basicContactDetails.mobile,
            HOME_TELEPHONE: receivedObj.contactDetailObj.basicContactDetails.homeTel,
            WORK_TELEPHONE: receivedObj.contactDetailObj.basicContactDetails.workTel,
            EMAIL: receivedObj.contactDetailObj.basicContactDetails.email,
            INSURANCE_NUMBER: receivedObj.insuranceDetailObj.basicInsuranceDetails.insuranceNumber,
            INSURANCE_NAME: receivedObj.insuranceDetailObj.basicInsuranceDetails.insuranceName,
            INSURANCE_START_DATE: receivedObj.insuranceDetailObj.basicInsuranceDetails.startDate,
            INSURANCE_EXPIRE_DATE: receivedObj.insuranceDetailObj.basicInsuranceDetails.expireDate,
            ROLE_AND_RESPONSIBILITY: receivedObj.officeDetailsObj.roleAndResponsibility,
            FRIST_LEVEL_APPROVAL_SUPERVISER: receivedObj.officeDetailsObj.firstLevelApproval,
            SECOND_LEVEL_APPROVAL_SUPERVISER: receivedObj.officeDetailsObj.secondLevelApproval,
            RESIGNATION_ACCEPTED_DURATION_DAYS: receivedObj.officeDetailsObj.resignationAcceptDuration,
            PROBATION_PERIOD_START_DATE: receivedObj.officeDetailsObj.probationPeriodStart,
            PROBATION_PERIOD_END_DATE: receivedObj.officeDetailsObj.probationPeriodEnd,
            JOINED_DATE: receivedObj.officeDetailsObj.joinedDate,
            CONFIRMATION_DATE: receivedObj.officeDetailsObj.confirmationDate,
            TERMINATION_DATE: receivedObj.officeDetailsObj.terminationDate,
            IS_RESIGNED: 0,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        }

        const [resultEmpProfile] = await conn.query('INSERT INTO hrm_employee_profile SET ?', employeePreofile);
        logger.info('Successfully saved employee profile record id = ' + resultEmpProfile.insertId);

        // insert data into hrm_emergency_contact
        if (0 < receivedObj.contactDetailObj.emergencyContactDetails.length) {
            for (var i = 0; i < receivedObj.contactDetailObj.emergencyContactDetails.length; i++) {

                var emergencyContact = {
                    ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
                    EMG_CON_NAME: receivedObj.contactDetailObj.emergencyContactDetails[i].EMG_CON_NAME,
                    EMG_CON_RELATIONSHIP: receivedObj.contactDetailObj.emergencyContactDetails[i].EMG_CON_RELATIONSHIP,
                    PHONE_NO_1: receivedObj.contactDetailObj.emergencyContactDetails[i].PHONE_NO_1,
                    PHONE_NO_2: receivedObj.contactDetailObj.emergencyContactDetails[i].PHONE_NO_2,
                    EMG_CON_EMAIL: receivedObj.contactDetailObj.emergencyContactDetails[i].EMG_CON_EMAIL,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultEmergencyContact] = await conn.query('INSERT INTO hrm_emergency_contact SET ?', emergencyContact);
                logger.info('Successfully saved employee emergency Contact record id = ' + resultEmergencyContact.insertId);
            }
        }

        // insert data into hrm_insurance_benifitted
        if (0 < receivedObj.insuranceDetailObj.benifittedPersonDetails.length) {
            for (var j = 0; j < receivedObj.insuranceDetailObj.benifittedPersonDetails.length; j++) {

                var benifittedPerson = {
                    ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
                    RELATIONSHIP: receivedObj.insuranceDetailObj.benifittedPersonDetails[j].RELATIONSHIP,
                    CARD: receivedObj.insuranceDetailObj.benifittedPersonDetails[j].CARD,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultInsuranceBenifitted] = await conn.query('INSERT INTO hrm_insurance_benifitted SET ?', benifittedPerson);
                logger.info('Successfully saved employee insurance benifitted record id = ' + resultInsuranceBenifitted.insertId);
            }
        }

        // insert data into hrm_employee_dependence
        if (0 < receivedObj.dependenceDetailObj.length) {
            for (var k = 0; k < receivedObj.dependenceDetailObj.length; k++) {

                var dependenceDetail = {
                    ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
                    DEPN_NAME: receivedObj.dependenceDetailObj[k].DEPN_NAME,
                    DEPN_RELATIONSHIP: receivedObj.dependenceDetailObj[k].DEPN_RELATIONSHIP,
                    DEPN_DATE_OF_BIRTH: receivedObj.dependenceDetailObj[k].DEPN_DATE_OF_BIRTH,
                    DEPN_NIC_PASSPORT: receivedObj.dependenceDetailObj[k].DEPN_NIC_PASSPORT,
                    IS_SPOUSE_WORKING: receivedObj.dependenceDetailObj[k].IS_SPOUSE_WORKING,
                    NO_OF_CHILDREAN: receivedObj.dependenceDetailObj[k].NO_OF_CHILDREAN,
                    SPOUSE_FIRSTNAME: receivedObj.dependenceDetailObj[k].SPOUSE_FIRSTNAME,
                    SPOUSE_MIDDLENAME: receivedObj.dependenceDetailObj[k].SPOUSE_MIDDLENAME,
                    SPOUSE_LASTNAME: receivedObj.dependenceDetailObj[k].SPOUSE_LASTNAME,
                    SPOUSE_NATIONALITY: receivedObj.dependenceDetailObj[k].SPOUSE_NATIONALITY,
                    SPOUSE_NIC: receivedObj.dependenceDetailObj[k].SPOUSE_NIC,
                    SPOUSE_BIRTHDAY: receivedObj.dependenceDetailObj[k].SPOUSE_BIRTHDAY,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultDependenceDetail] = await conn.query('INSERT INTO hrm_employee_dependence SET ?', dependenceDetail);
                logger.info('Successfully saved employee dependence Detail record id = ' + resultDependenceDetail.insertId);
            }
        }

        // insert data into hrm_higher_education_qualification
        var educationQualificationHI = {
            ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
            INSTITUTION: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.university_institution,
            COUNTRY: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.country,
            DEGREE: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.degree,
            LEVEL: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.level,
            FIELD_STUDY: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.field_study,
            STATUS: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.status,
            START_DATE: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.hi_start_date,
            END_DATE: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.hi_end_date,
            TYPE: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.type,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };

        const [resultEducationQualificationHI] = await conn.query('INSERT INTO hrm_higher_education_qualification SET ?', educationQualificationHI);
        logger.info('Successfully saved employee education Qualification HI record id = ' + resultEducationQualificationHI.insertId);

        // insert data into hrm_education_qualification OL
        var educationQualificationOL = {
            ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
            TYPE: 'OL',
            SCHOOL: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.school_od,
            COUNTRY: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.country_od,
            YEAR: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.year_od,
            INDEX: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.index_od,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };

        const [resultEducationQualificationOL] = await conn.query('INSERT INTO hrm_education_qualification SET ?', educationQualificationOL);
        logger.info('Successfully saved employee education Qualification OL record id = ' + resultEducationQualificationOL.insertId);

        if (resultEducationQualificationOL.insertId > 0 && 0 < receivedObj.educationQualificationDetailObj.odSubjectDetails.length) {

            // insert hrm_education_qualification_subjects OL subjects
            for (var l = 0; l < receivedObj.educationQualificationDetailObj.odSubjectDetails.length; l++) {

                var olSubjects = {
                    ID_EDUCATION_QUALIFICATION: resultEducationQualificationOL.insertId,
                    NAME: receivedObj.educationQualificationDetailObj.odSubjectDetails[l].odtSubject,
                    GRADE: receivedObj.educationQualificationDetailObj.odSubjectDetails[l].odtGrade,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultOlSubjects] = await conn.query('INSERT INTO hrm_education_qualification_subjects SET ?', olSubjects);
                logger.info('Successfully saved employee education Ol Subjects record id = ' + resultOlSubjects.insertId);
            }
        }

        // insert data into hrm_education_qualification AL 
        var educationQualificationAL = {
            ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
            TYPE: 'AL',
            SCHOOL: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.school_ad,
            COUNTRY: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.country_ad,
            YEAR: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.year_ad,
            INDEX: receivedObj.educationQualificationDetailObj.basicEducationQualificationDetails.index_ad,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };

        const [resultEducationQualificationAL] = await conn.query('INSERT INTO hrm_education_qualification SET ?', educationQualificationAL);
        logger.info('Successfully saved employee education Qualification AL record id = ' + resultEducationQualificationAL.insertId);

        if (resultEducationQualificationAL.insertId > 0 && 0 < receivedObj.educationQualificationDetailObj.adSubjectDetails.length) {

            // insert hrm_education_qualification_subjects AL subjects
            for (var m = 0; m < receivedObj.educationQualificationDetailObj.adSubjectDetails.length; m++) {

                var alSubjects = {
                    ID_EDUCATION_QUALIFICATION: resultEducationQualificationAL.insertId,
                    NAME: receivedObj.educationQualificationDetailObj.adSubjectDetails[m].adtSubject,
                    GRADE: receivedObj.educationQualificationDetailObj.adSubjectDetails[m].adtGrade,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultAlSubjects] = await conn.query('INSERT INTO hrm_education_qualification_subjects SET ?', alSubjects);
                logger.info('Successfully saved employee education Al Subjects record id = ' + resultAlSubjects.insertId);

            }
        }

        // insert data into hrm_proffessional_qualification
        if (0 == Object.keys(receivedObj.professionalQualificationDetailObj).length || undefined == receivedObj.professionalQualificationDetailObj || null == receivedObj.professionalQualificationDetailObj) {

        } else {
            for (var n = 0; n < receivedObj.professionalQualificationDetailObj.length; n++) {

                var professionalQul = {
                    ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
                    PREVIOUS_EMPLOYER_NAME: receivedObj.professionalQualificationDetailObj[n].PREVIOUS_EMPLOYER_NAME,
                    PREVIOUS_EMPLOYER_ADDRESS: receivedObj.professionalQualificationDetailObj[n].PREVIOUS_EMPLOYER_ADDRESS,
                    JOB_TITLE_DESIGNATION: receivedObj.professionalQualificationDetailObj[n].JOB_TITLE_DESIGNATION,
                    EMAIL: receivedObj.professionalQualificationDetailObj[n].EMAIL,
                    FROM_DATE: receivedObj.professionalQualificationDetailObj[n].FROM_DATE,
                    TO_DATE: receivedObj.professionalQualificationDetailObj[n].TO_DATE,
                    ROLE_AND_RESPONSIBILITIES: receivedObj.professionalQualificationDetailObj[n].ROLE_AND_RESPONSIBILITIES,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultProfessionalQul] = await conn.query('INSERT INTO hrm_proffessional_qualification SET ?', professionalQul);
                logger.info('Successfully saved employee professional Qulification record id = ' + resultProfessionalQul.insertId);

            }
        }

        // insert data into hrm_employee_documents
        if (receivedObj.documentDetailObj && 0 < receivedObj.documentDetailObj.length) {
            for (var k = 0; k < receivedObj.documentDetailObj.length; k++) {

                var documents = {
                    ID_EMPLOYEE_REGISTRY: insertEmpRegistryId,
                    DOCUMENT_TYPE: receivedObj.documentDetailObj[k].document,
                    DATE_ADDED: receivedObj.documentDetailObj[k].dateAdded,
                    VALIDATE_TILL: receivedObj.documentDetailObj[k].validTill,
                    STATUS: receivedObj.documentDetailObj[k].status,
                    DETAILS: receivedObj.documentDetailObj[k].detail,
                    ATTACHMENT_PATH: receivedObj.documentDetailObj[k].docPath,
                    DOCUMENT_NAME: receivedObj.documentDetailObj[k].docName,
                    IS_ACTIVE: 1,
                    CREATED_BY: 0,
                    REMARK: ''
                };

                const [resultDocument] = await conn.query('INSERT INTO hrm_employee_documents SET ?', documents);
                logger.info('Successfully saved employee document record id = ' + resultDocument.insertId);

            }
        }

        await conn.query('COMMIT');
        conn.release();
        logger.info('A NEW EMPLOYEE CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESS' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('A NEW EMPLOYEE CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});


// delete employee documents
router.post('/deleteEmployeeDocument', async function (req, res) {

    const API_NAME = 'deleteEmployeeDocument POST, ';
    logger.info(API_NAME + ' called');
    var receivedObj = req.body;

    try {
        const conn = await pool.getConnection();

        const [deleteDoc, fields] = await conn.query("UPDATE hrm_employee_documents SET IS_ACTIVE=0 WHERE ID_DOCUMENT=" + receivedObj.idDocument + "");

        conn.release();
        logger.info('EMPLOYEE DOCUMENT DELETED SUCCESSFULLY...');
        return res.status(200).send({ success: true });

    } catch (err) {
        if (conn !== undefined) {
            conn.release();
        }
        logger.error('CANNOT DELETE DOCUMENT DUE TO :- ', err);
        return res.status(200).send({ success: false });
    }

});

module.exports = router;
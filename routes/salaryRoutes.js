var express = require('express');
var lodash = require('lodash');
var cron = require("node-cron");
var Finance = require('financejs');
var moment = require('moment');
var router = express.Router();
var pool = require('../config/database').pool;

let finance = new Finance();

cron.schedule("38 17 19 * *", function () {
    salaryCalculation();
});

async function salaryCalculation() {

    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var day = today.getDate();

    var configYear = 2018;
    var configMonth = 11;
    var configDay = 19;
    var other = {
        'EPF-Employee': {
            'value': 8,
            'type': 'Deduction',
            'isPercentage': true,
            'use': 'Basic',
        },
        // 'EPF by Company': {
        //     'value':12,
        //     'type':'Deduction',
        //     'isPercentage':true,
        //     'use':'Basic',
        // },
        // 'ETF by Company': {
        //     'value':3,
        //     'type':'Deduction',
        //     'isPercentage':true,
        //     'use':'Basic',
        // },
    }

    if (month == configMonth && day == configDay) {

        logger.info('Loan Calculator Started');
        const conn = await pool.getConnection();

        try {

            const empData = await conn.execute(`SELECT * FROM hrm_employee_registry reg JOIN hrm_employee_profile pro ON reg.ID_EMPLOYEE_REGISTRY=pro.ID_EMPLOYEE_REGISTRY WHERE reg.IS_ACTIVE = 1 AND pro.IS_ACTIVE = 1 AND pro.IS_RESIGNED = 0`);

            for (let i = 0; empData[0].length > i; i++) {

                var empSalaryDetails = [];
                var loanPayments = [];

                console.log('Employee Code: ', empData[0][i].REGISTRY_CODE_PREFIX + empData[0][i].ID_EMPLOYEE_REGISTRY + empData[0][i].REGISTRY_CODE_SUFFIX);

                var response = await getBasicAllowanceDeduction(conn, empData[0][i], empSalaryDetails);

                if (response != false) {

                    empSalaryDetails = response;

                    var response = await getAdditionalAllowanceDeduction(conn, empData[0][i], empSalaryDetails);

                    if (response != false) empSalaryDetails = response;

                    var response = await getOTPayment(conn, empData[0][i], `${year}-${month}`, empSalaryDetails);

                    if (response != false) empSalaryDetails = response;

                    var response = await getLoanInstallments(conn, empData[0][i], `${year}-${month}-${day}`, empSalaryDetails);

                    if (response != false) empSalaryDetails = response['empSalaryDetails']; loanPayments = response['loanPayments'];

                    var response = await getNopay(conn, empData[0][i], empSalaryDetails);

                    if (response != false) empSalaryDetails = response;

                    var basicAmount = 0;
                    var netAmount = 0;

                    await empSalaryDetails.forEach((item, m) => {

                        if (item['REFERANCE'] == 'BASIC') {
                            basicAmount = item['AMOUNT'];
                            netAmount = netAmount + +item['AMOUNT'];
                        }

                        else {
                            if (item['TYPE'] == 'Allowance' || item['TYPE'] == 'Perks') {

                                netAmount = netAmount + +item['AMOUNT'];
                            }
                            else if (item['TYPE'] == 'Deduction') {

                                netAmount = netAmount + -item['AMOUNT'];
                            }
                        }
                    });

                    var response = await getOther(conn, empData[0][i], empSalaryDetails, other, basicAmount, netAmount);

                    if (response != false) empSalaryDetails = response;

                    // console.log('Salary: ', empSalaryDetails);
                    // console.log('Loan Payments: ', loanPayments);

                    var basicAmount = 0;
                    var netAmount = 0;

                    await empSalaryDetails.forEach((item, m) => {

                        if (item['REFERANCE'] == 'BASIC') {
                            basicAmount = item['AMOUNT'];
                            netAmount = netAmount + +item['AMOUNT'];
                        }

                        else {
                            if (item['TYPE'] == 'Allowance' || item['TYPE'] == 'Perks') {

                                netAmount = netAmount + +item['AMOUNT'];
                            }
                            else if (item['TYPE'] == 'Deduction') {

                                netAmount = netAmount + -item['AMOUNT'];
                            }
                        }
                    });

                    await conn.query('START TRANSACTION');
                    try {
                        await conn.query(`UPDATE hrm_salary_header SET ? WHERE ID_EMPLOYEE_REGISTRY=${empData[0][i].ID_EMPLOYEE_REGISTRY} AND PERIOD='${year}-${month}'`, { 'IS_ACTIVE': 0 });

                        const [response, fields] = await conn.query('INSERT INTO hrm_salary_header SET ?', { 'ID_EMPLOYEE_REGISTRY': empData[0][i].ID_EMPLOYEE_REGISTRY, 'BASIC_SALARY': basicAmount, 'NET_SALARY': netAmount, 'PERIOD': `${year}-${month}`, 'STATUS': 'Processed' });

                        await empSalaryDetails.forEach((item, m) => {

                            item['ID_SALARY_HEADER'] = response.insertId;

                            conn.query('INSERT INTO hrm_salary_detail SET ?', item);
                        });

                        if (loanPayments) {
                            await loanPayments.forEach((item, m) => {

                                conn.query('INSERT INTO hrm_loan_payment SET ?', item);
                            });
                        }

                        await conn.query('COMMIT');

                        console.log('Net Salary: ', netAmount);
                    }
                    catch (err) {

                        await conn.query('ROLLBACK');

                        console.log('Net Salary: ', err);
                    }
                }
            }
        }
        catch (err) {
            logger.info(err);
        }

        conn.release();
        logger.info('Salary Calculation Finished');
    }
    else {
        return false;
    }
}

async function getBasicAllowanceDeduction(conn, emp, empSalaryDetails) {

    try {
        const salaryPlan = await conn.execute(`SELECT * FROM hrm_salary sal WHERE sal.ID_EMPLOYEE_REGISTRY=${emp.ID_EMPLOYEE_REGISTRY} AND sal.IS_ACTIVE = 1`);

        if (salaryPlan[0].length > 0) {

            empSalaryDetails.push({
                'AMOUNT': salaryPlan[0][0].BASIC_SALARY,
                'PROCESSED_VALUE': salaryPlan[0][0].BASIC_SALARY,
                'TYPE': 'Allowance',
                'DESCRIPTION': 'Basic Salary',
                'REFERANCE': 'BASIC'
            });

            const allowanceDeduction = await conn.execute(`SELECT * FROM hrm_allowance_deduction_has_hrm_salary ad JOIN hrm_defaults_allowance_deduction dad ON ad.ID_ALLOWANCE_DEDUCTION=dad.ID_ALLOWANCE_DEDUCTION WHERE ad.ID_SALARY=${salaryPlan[0][0].ID_SALARY} AND ad.IS_ACTIVE = 1 AND dad.IS_ACTIVE = 1 ORDER BY dad.STATUS ASC`);

            for (var n = 0; allowanceDeduction[0].length > n; n++) {

                empSalaryDetails.push({
                    'AMOUNT': allowanceDeduction[0][n].AMOUNT,
                    'PROCESSED_VALUE': allowanceDeduction[0][n].AMOUNT,
                    'TYPE': allowanceDeduction[0][n].STATUS,
                    'DESCRIPTION': allowanceDeduction[0][n].NAME,
                    'REFERANCE': allowanceDeduction[0][n].ID_ALLOWANCE_DEDUCTION
                });
            }

            return empSalaryDetails;
        }
        else {
            console.log('Salary Plan: ', 'Not Available');
            return false;
        }
    }

    catch (err) {
        logger.info(err);
        return false;
    }
}

async function getAdditionalAllowanceDeduction(conn, emp, empSalaryDetails) {

    return empSalaryDetails;
}

async function getOTPayment(conn, emp, period, empSalaryDetails) {

    try {
        const otPayment = await conn.execute(`SELECT * FROM view_ot_with_details otd WHERE otd.PAYROLL_MONTH='${period}' AND otd.ID_EMPLOYEE_REGISTRY=${emp.ID_EMPLOYEE_REGISTRY}`);

        if (otPayment[0].length > 0) {

            empSalaryDetails.push({
                'AMOUNT': otPayment[0][0].OT_TOTAL,
                'PROCESSED_VALUE': otPayment[0][0].OT_TOTAL,
                'TYPE': 'Allowance',
                'DESCRIPTION': 'OT',
                'REFERANCE': 'OT'
            });

            return empSalaryDetails;
        }
        else {
            return false;
        }
    }

    catch (err) {
        logger.info(err);
        return false;
    }
}

async function getLoanInstallments(conn, emp, date, empSalaryDetails) {

    try {
        var loanPayments = [];
        const loanData = await conn.execute(`SELECT * FROM hrm_loan lon WHERE lon.ID_EMPLOYEE_REGISTRY=${emp.ID_EMPLOYEE_REGISTRY} AND lon.IS_ACTIVE = 1 AND lon.LOAN_STATUS='Issued'`);

        if (loanData[0].length > 0) {

            for (var n = 0; loanData[0].length > n; n++) {

                var installment = await finance.AM(loanData[0][n]['LOAN_AMOUNT'], loanData[0][n]['INTEREST_RATE'], loanData[0][n]['LOAN_TERM'], 1);

                empSalaryDetails.push({
                    'AMOUNT': installment,
                    'PROCESSED_VALUE': installment,
                    'TYPE': 'Deduction',
                    'DESCRIPTION': `Loan No: ${loanData[0][n]['ID_LOAN_REGISTRY']}`,
                    'REFERANCE': loanData[0][n]['ID_LOAN_REGISTRY']
                });

                loanPayments.push({
                    'ID_LOAN_REGISTRY': loanData[0][n]['ID_LOAN_REGISTRY'],
                    'AMOUNT': installment,
                    'TRANSACTION_DATE': date
                });
            }

            return {
                'empSalaryDetails': empSalaryDetails,
                'loanPayments': loanPayments
            };
        }
        else {
            return false;
        }
    }

    catch (err) {
        logger.info(err);
        return false;
    }
}

async function getNopay(conn, emp, empSalaryDetails) {

    return empSalaryDetails;
}

async function getOther(conn, emp, empSalaryDetails, other, basicAmount, netAmount) {

    try {
        var keys = Object.keys(other);

        if (keys.length > 0) {

            for (var n = 0; keys.length > n; n++) {

                var amount = 0;

                if (other[keys[n]]['use'] == 'Basic') {

                    if (other[keys[n]]['isPercentage'] == true) {

                        amount = (basicAmount / 100) * other[keys[n]]['value'];
                    }
                    else {
                        amount = other[keys[n]]['value'];
                    }
                }

                else if (other[keys[n]]['use'] == 'Net') {

                    if (other[keys[n]]['isPercentage'] == true) {

                        amount = (netAmount / 100) * other[keys[n]]['value'];
                    }
                    else {
                        amount = other[keys[n]]['value'];
                    }
                }

                empSalaryDetails.push({
                    'AMOUNT': amount,
                    'PROCESSED_VALUE': amount,
                    'TYPE': other[keys[n]]['type'],
                    'DESCRIPTION': other[keys[n]]['isPercentage'] ? keys[n] + ' (' + other[keys[n]]['value'] + '% from ' + other[keys[n]]['use'] + ')' : keys[n],
                    'REFERANCE': 'OTHER'
                });
            }

            return empSalaryDetails;
        }
        else {
            return false;
        }
    }

    catch (err) {
        logger.info(err);
        return false;
    }
}

router.get('/getEmployeeCountForSalary', async function (req, res, next) {
    const API_NAME = 'getEmployeeCountForSalary get, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        // Do something with the connection
        var sql = ` SELECT COUNT(ID_EMPLOYEE_PROFILE) AS TOTAL FROM hrm_employee_profile WHERE IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addSalaryAdvanceRqeuest', async function (req, res, next) {
    const API_NAME = 'addSalaryAdvanceRqeuest post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    logger.info(req.body);

    try {
        const conn = await pool.getConnection();

        await conn.query('INSERT INTO hrm_salary_advance_request SET ?', req.body);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Salary Advance Requested' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryAdvanceRqeuest', async function (req, res, next) {
    const API_NAME = 'getSalaryAdvanceRqeuest post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    logger.info(req.body);

    var result;

    try {
        const conn = await pool.getConnection();

        result = await conn.query('SELECT * FROM hrm_salary_advance_request WHERE IS_ACTIVE=1');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, data: result });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/getSalaryAdvanceList', async function (req, res, next) {
    const API_NAME = 'getSalaryAdvanceList post, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` SELECT * FROM view_salary_advance WHERE IS_ACTIVE = '1'   ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getSalaryAdvanceListCount', async function (req, res, next) {
    const API_NAME = 'getSalaryAdvanceListCount get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        // Do something with the connection
        var sql = ` SELECT COUNT(*) AS TOTAL FROM view_salary_advance WHERE IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/salaryAdvanceAppove', async function (req, res, next) {
    const API_NAME = 'salaryAdvanceAppove post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('UPDATE hrm_salary_advance_request SET ? where ID_SALARY_ADVANCE_REQUEST=' + req.body.id, req.body.data);

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Salary Advance Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addSalaryAllowence&Deduction', async function (req, res, next) {
    const API_NAME = 'addSalaryAllowence&Deduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('INSERT INTO hrm_defaults_allowance_deduction SET ?', req.body);

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateSalaryAllowence&Deduction', async function (req, res, next) {
    const API_NAME = 'updateSalaryAllowence&Deduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('UPDATE hrm_defaults_allowance_deduction SET ? WHERE ID_ALLOWANCE_DEDUCTION=' + req.body.id, req.body.data);

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/deleteSalaryAllowence&Deduction', async function (req, res, next) {
    const API_NAME = 'deleteSalaryAllowence&Deduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('UPDATE hrm_defaults_allowance_deduction SET ? WHERE ID_ALLOWANCE_DEDUCTION=' + req.body.ID, { IS_ACTIVE: 0 });

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Deleted' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryAllowence&Deduction', async function (req, res, next) {
    const API_NAME = 'getSalaryAllowence&Deduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_defaults_allowance_deduction WHERE IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addPayroll', async function (req, res, next) {
    const API_NAME = 'addPayroll post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    var netSalary = +req.body.BasicValue;
    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        const values = await conn.execute(`SELECT * FROM hrm_defaults_allowance_deduction WHERE IS_ACTIVE = '1'`);

        const [response, fields] = await conn.query('INSERT INTO hrm_salary SET ?', { 'BASIC_SALARY': req.body.BasicValue, 'ID_EMPLOYEE_REGISTRY': req.body.ID_EMPLOYEE_REGISTRY });
        if (req.body.FixedFields) {
            await req.body.FixedFields.forEach(function (item, i) {

                if (item != null) {

                    var picked = lodash.filter(values[0], x => x.ID_ALLOWANCE_DEDUCTION == item);

                    if (picked[0].STATUS == 'Allowance' || picked[0].STATUS == 'Perks') netSalary = netSalary + +req.body.FixedValues[i];
                    if (picked[0].STATUS == 'Deduction') netSalary = netSalary - +req.body.FixedValues[i];
                    conn.query('INSERT INTO hrm_allowance_deduction_has_hrm_salary SET ?', { 'ID_ALLOWANCE_DEDUCTION': item, 'ID_SALARY': response.insertId, 'AMOUNT': req.body.FixedValues[i] });
                }
            });
        }
        if (req.body.Fields) {
            await req.body.Fields.forEach(function (item, i) {

                if (item != null) {
                    var picked = lodash.filter(values[0], x => x.ID_ALLOWANCE_DEDUCTION == item);

                    if (picked[0].STATUS == 'Allowance' || picked[0].STATUS == 'Perks') netSalary = netSalary + +req.body.Values[i];
                    if (picked[0].STATUS == 'Deduction') netSalary = netSalary - +req.body.Values[i];

                    conn.query('INSERT INTO hrm_allowance_deduction_has_hrm_salary SET ?', { 'ID_ALLOWANCE_DEDUCTION': item, 'ID_SALARY': response.insertId, 'AMOUNT': req.body.Values[i] });
                }
            });
        }
        await conn.query('UPDATE hrm_salary SET ? WHERE ID_SALARY=' + response.insertId, { 'GROSS_SALARY': netSalary });

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
})

router.post('/getPayroll', async function (req, res, next) {
    const API_NAME = 'getPayroll post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute("SELECT * FROM hrm_salary sa JOIN view_all_emp_profile_details em ON sa.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY WHERE sa.IS_ACTIVE = 1");
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getPayrollAllowence&Deduction', async function (req, res, next) {
    const API_NAME = 'getSalaryAllowence&Deduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_allowance_deduction_has_hrm_salary sa JOIN hrm_defaults_allowance_deduction ad ON sa.ID_ALLOWANCE_DEDUCTION=ad.ID_ALLOWANCE_DEDUCTION WHERE sa.IS_ACTIVE = '1' AND sa.ID_SALARY=${req.body.ID}`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updatePayroll', async function (req, res, next) {
    const API_NAME = 'updatePayroll post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    var netSalary = +req.body.data.BasicValue;
    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        const values = await conn.execute(`SELECT * FROM hrm_defaults_allowance_deduction WHERE IS_ACTIVE = '1'`);
        await conn.query(`UPDATE hrm_salary SET ? WHERE ID_SALARY=${req.body.id}`, { 'IS_ACTIVE': 0 });
        await conn.query(`UPDATE hrm_allowance_deduction_has_hrm_salary SET ? WHERE ID_SALARY=${req.body.id}`, { 'IS_ACTIVE': 0 });
        const [response, fields] = await conn.query('INSERT INTO hrm_salary SET ?', { 'BASIC_SALARY': req.body.data.BasicValue, 'ID_EMPLOYEE_REGISTRY': req.body.data.ID_EMPLOYEE_REGISTRY });
        if (req.body.data.FixedFields) {
            await req.body.data.FixedFields.forEach(function (item, i) {

                if (item != null) {

                    var picked = lodash.filter(values[0], x => x.ID_ALLOWANCE_DEDUCTION == item);

                    if (picked[0].STATUS == 'Allowance' || picked[0].STATUS == 'Perks') netSalary = netSalary + +req.body.data.FixedValues[i];
                    if (picked[0].STATUS == 'Deduction') netSalary = netSalary - +req.body.data.FixedValues[i];
                    conn.query('INSERT INTO hrm_allowance_deduction_has_hrm_salary SET ?', { 'ID_ALLOWANCE_DEDUCTION': item, 'ID_SALARY': response.insertId, 'AMOUNT': req.body.data.FixedValues[i] });
                }
            });
        }
        if (req.body.data.Fields) {
            await req.body.data.Fields.forEach(function (item, i) {

                if (item != null) {
                    var picked = lodash.filter(values[0], x => x.ID_ALLOWANCE_DEDUCTION == item);

                    if (picked[0].STATUS == 'Allowance' || picked[0].STATUS == 'Perks') netSalary = netSalary + +req.body.data.Values[i];
                    if (picked[0].STATUS == 'Deduction') netSalary = netSalary - +req.body.data.Values[i];

                    conn.query('INSERT INTO hrm_allowance_deduction_has_hrm_salary SET ?', { 'ID_ALLOWANCE_DEDUCTION': item, 'ID_SALARY': response.insertId, 'AMOUNT': req.body.data.Values[i] });
                }
            });
        }
        await conn.query('UPDATE hrm_salary SET ? WHERE ID_SALARY=' + response.insertId, { 'GROSS_SALARY': netSalary });

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
})


router.post('/addIncrement', async function (req, res, next) {
    const API_NAME = 'addIncrement post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    // console.log(req.body);

    var netSalary = +req.body.BasicValue;
    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        if (req.body.employee) {

            await req.body.employee.forEach(function (emp, n) {

                var increments = [];
                var increment = [];

                if ((req.body.increment.BasicValue != null || req.body.increment.BasicPercentage != null) && req.body.increment.BasicDate != null) {
                    conn.query('INSERT INTO hrm_salary_increment SET ?', { 'ID_ALLOWANCE_DEDUCTION': 0, 'ID_EMPLOYEE_REGISTRY': emp.ID_EMPLOYEE_REGISTRY, 'INCREMENT_AMOUNT': req.body.increment.BasicValue, 'INCREMENT_PRECENTAGE': req.body.increment.BasicPercentage, 'EFFECTIVE_DATE': req.body.increment.BasicDate });
                }

                if (req.body.increment.Fields) {
                    req.body.increment.Fields.forEach(function (item, i) {

                        if ((req.body.increment.Values[i] != null || req.body.increment.Percentage[i] != null) && req.body.increment.Dates[i] != null) {

                            conn.query('INSERT INTO hrm_salary_increment SET ?', { 'ID_ALLOWANCE_DEDUCTION': item, 'ID_EMPLOYEE_REGISTRY': emp.ID_EMPLOYEE_REGISTRY, 'INCREMENT_AMOUNT': req.body.increment.Values[i], 'INCREMENT_PRECENTAGE': req.body.increment.Percentage[i], 'EFFECTIVE_DATE': req.body.increment.Dates[i] });

                            // increment = [];

                            // increment.push(item);
                            // increment.push(emp.ID_EMPLOYEE_PROFILE);
                            // increment.push(req.body.increment.Values[i]);
                            // increment.push(req.body.increment.Percentage[i]);
                            // increment.push(req.body.increment.Dates[i]);

                            // increments.push(increment);
                        }
                    });
                }

                //    conn.query('INSERT INTO hrm_salary_increment (ID_ALLOWANCE_DEDUCTION,ID_EMPLOYEE_PROFILE,INCREMENT_AMOUNT,INCREMENT_PRECENTAGE,EFFECTIVE_DATE) VALUES ?', increments);
            });
        }

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getIncrementList', async function (req, res, next) {
    const API_NAME = 'getIncrementList post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute("SELECT * FROM hrm_salary_increment sa JOIN view_all_emp_profile_details em ON sa.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY JOIN hrm_defaults_allowance_deduction ad ON sa.ID_ALLOWANCE_DEDUCTION = ad.ID_ALLOWANCE_DEDUCTION JOIN hrm_salary sal ON sal.ID_EMPLOYEE_REGISTRY = sa.ID_EMPLOYEE_REGISTRY WHERE sal.IS_ACTIVE=1 ORDER BY sa.CREATED_DATE DESC");
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateSalaryIncrement', async function (req, res, next) {
    const API_NAME = 'updateSalaryIncrement post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    console.log(req.body);

    try {

        var data = {};
        // data["INCREMENT_AMOUNT"] = req.body.data.INCREMENT;
        // data["EFFECTIVE_DATE"] = req.body.data.DATE;

        const conn = await pool.getConnection();

        await conn.query(`UPDATE hrm_salary_increment SET ? WHERE ID_SALARY_INCREMENT=${req.body.id}`, { 'EFFECTIVE_DATE': req.body.data.DATE, 'INCREMENT_AMOUNT': req.body.data.INCREMENT });
        // await conn.execute(`UPDATE hrm_salary_increment SET ? WHERE ID_SALARY_INCREMENT=${req.body.id}`, { EFFECTIVE_DATE: req.body.data.DATE, INCREMENT_AMOUNT: req.body.data.INCREMENT });
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, message: "Updated" });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryList', async function (req, res, next) {
    const API_NAME = 'getSalaryList post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        var values;

        if (req.body.STATUS) {

            values = await conn.execute("SELECT *," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Allowance' AND sd.REFERANCE !='BASIC' AND sd.REFERANCE !='OTHER' AND sd.REFERANCE !='OT' AND sd.IS_ACTIVE=1) AS ALLOWANCE," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Perks' AND sd.IS_ACTIVE=1) AS PERKS," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Deduction' AND sd.REFERANCE !='OTHER' AND sd.IS_ACTIVE=1) AS DEDUCTION," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.REFERANCE='OT' AND sd.IS_ACTIVE=1) AS OT," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.REFERANCE='BASIC' AND sd.IS_ACTIVE=1) AS BASIC_SALARY," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Allowance' AND sd.REFERANCE='OTHER' AND sd.IS_ACTIVE=1) AS OTHER_ALLOWANCE," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Deduction' AND sd.REFERANCE='OTHER' AND sd.IS_ACTIVE=1) AS OTHER_DEDUCTION " +
                "FROM hrm_salary_header sh JOIN view_all_emp_profile_details em ON sh.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY  WHERE sh.IS_ACTIVE = 1 AND sh.PERIOD = '" + req.body.PERIOD + "' AND sh.STATUS = '" + req.body.STATUS + "' ORDER BY sh.CREATED_DATE DESC");
        }

        else {
            values = await conn.execute("SELECT *," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Allowance' AND sd.REFERANCE !='BASIC' AND sd.REFERANCE !='OTHER' AND sd.REFERANCE !='OT' AND sd.IS_ACTIVE=1) AS ALLOWANCE," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Perks' AND sd.IS_ACTIVE=1) AS PERKS," +
                "(SELECT SUM(AMOUNT) FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Deduction' AND sd.REFERANCE !='OTHER' AND sd.IS_ACTIVE=1) AS DEDUCTION," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.REFERANCE='OT' AND sd.IS_ACTIVE=1) AS OT," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.REFERANCE='BASIC' AND sd.IS_ACTIVE=1) AS BASIC_SALARY," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Allowance' AND sd.REFERANCE='OTHER' AND sd.IS_ACTIVE=1) AS OTHER_ALLOWANCE," +
                "(SELECT AMOUNT FROM hrm_salary_detail sd WHERE sh.ID_SALARY_HEADER=sd.ID_SALARY_HEADER AND sd.TYPE='Deduction' AND sd.REFERANCE='OTHER' AND sd.IS_ACTIVE=1) AS OTHER_DEDUCTION " +
                "FROM hrm_salary_header sh JOIN view_all_emp_profile_details em ON sh.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY  WHERE sh.IS_ACTIVE = 1 AND sh.PERIOD = '" + req.body.PERIOD + "' ORDER BY sh.CREATED_DATE DESC");

        }

        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryDetails', async function (req, res, next) {
    const API_NAME = 'getSalaryDetails post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_salary_detail WHERE ID_SALARY_HEADER=${req.body.ID} AND IS_ACTIVE=1`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateSalary', async function (req, res, next) {
    const API_NAME = 'updateSalary post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    // console.log(req.body);

    const conn = await pool.getConnection();

    try {
        await conn.query('START TRANSACTION');

        var netSalary = 0;
        await req.body.data.ID.forEach(async function (item, i) {

            if (item != null) {

                const values = await conn.query(`SELECT * FROM hrm_salary_detail WHERE ID_SALARY_DETAIL=${item}`);
                // await conn.execute(`UPDATE hrm_salary_detail set ? WHERE ID_SALARY_DETAIL=${item}`, {IS_ACTIVE: 0});
                await conn.query('UPDATE hrm_salary_detail SET ? WHERE ID_SALARY_DETAIL=' + item, { IS_ACTIVE: 0 });

                var data = values[0][0];

                delete data.ID_SALARY_DETAIL;
                delete data.CREATED_DATE;
                delete data.EDITED_DATE;
                data['ADD'] = req.body.data.ADD[i];
                data['CUT_OFF'] = req.body.data.CUT[i];
                data['AMOUNT'] = req.body.data.VALUE[i];

                if (data.TYPE == "Allowance" || data.TYPE == "Perks") netSalary += +data.AMOUNT;

                //     netSalary += +data.AMOUNT;
                //     console.log("Allowance", data.AMOUNT);
                //     console.log(netSalary);

                // }
                if (data.TYPE == "Deduction") netSalary -= +data.AMOUNT;

                //     netSalary -= +data.AMOUNT;
                //     console.log("Deduction", data.AMOUNT);
                //     console.log(netSalary);

                // }
                // console.log(netSalary);
                await conn.query('INSERT INTO hrm_salary_detail SET ?', data);
                await conn.query('UPDATE hrm_salary_header SET ? WHERE ID_SALARY_HEADER=' + req.body.id, { NET_SALARY: netSalary, STATUS: req.body.status });
            }
        });

        conn.release();
        await conn.query('COMMIT');
        return res.status(200).send({ success: true, message: 'Updated' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/holdSalary', async function (req, res, next) {
    const API_NAME = 'holdSalary post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('UPDATE hrm_salary_header SET ? WHERE ID_SALARY_HEADER=' + req.body.ID, { STATUS: 'Hold' });

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Hold' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/unholdSalary', async function (req, res, next) {
    const API_NAME = 'holdSalary post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();


        await conn.query('UPDATE hrm_salary_header SET ? WHERE ID_SALARY_HEADER=' + req.body.ID, { STATUS: 'Processed' });

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Hold' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addAdditionalAllowanceDeduction', async function (req, res, next) {
    const API_NAME = 'addAdditionalAllowanceDeduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    // console.log(req.body);

    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        if (req.body.employee) {

            await req.body.employee.forEach(function (emp, n) {

                conn.query('INSERT INTO hrm_additional_allowance_deduction_has_hrm_salary SET ?',
                    { ID_ALLOWANCE_DEDUCTION: req.body.values.ID_ALLOWANCE_DEDUCTION, ID_SALARY: emp.ID_SALARY, AMOUNT: req.body.values.AMOUNT, REMARK: req.body.values.REMARK, EFFECTIVE_MONTH: req.body.values.EFFECTIVE_MONTH.substr(0, 7), ADDITIONAL_ALLOWANCE_DEDUCTION_STATUS: "Pending" });
            });
        }

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/getAdditionalAllowanceDeduction', async function (req, res, next) {
    const API_NAME = 'getAdditionalAllowanceDeduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT *, (sa.REMARK) as REMARK FROM hrm_additional_allowance_deduction_has_hrm_salary sa JOIN hrm_defaults_allowance_deduction ad ON sa.ID_ALLOWANCE_DEDUCTION=ad.ID_ALLOWANCE_DEDUCTION JOIN hrm_salary hs ON sa.ID_SALARY=hs.ID_SALARY  JOIN view_all_emp_profile_details ed ON ed.ID_EMPLOYEE_REGISTRY=hs.ID_EMPLOYEE_REGISTRY WHERE sa.IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/approveAdditionalAllowanceDeduction', async function (req, res, next) {
    const API_NAME = 'approveAdditionalAllowanceDeduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    // console.log(req.body);

    const conn = await pool.getConnection();

    try {
        await conn.query('START TRANSACTION');

        var netSalary = 0;
        await req.body.data.forEach(async function (item, i) {

            if (item != null) {

                await conn.query('UPDATE hrm_additional_allowance_deduction_has_hrm_salary SET ? WHERE ID_ADDITIONAL_ALLOWANCE_DEDUCTION=' + item.ID_ADDITIONAL_ALLOWANCE_DEDUCTION, { ADDITIONAL_ALLOWANCE_DEDUCTION_STATUS: "Approved" });
            }
        });

        conn.release();
        await conn.query('COMMIT');
        return res.status(200).send({ success: true, message: 'Updated' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateAdditionalAllowanceDeduction', async function (req, res, next) {
    const API_NAME = 'updateAdditionalAllowanceDeduction post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    console.log(req.body);

    try {

        var data = {};
        // data["INCREMENT_AMOUNT"] = req.body.data.INCREMENT;
        // data["EFFECTIVE_DATE"] = req.body.data.DATE;

        const conn = await pool.getConnection();

        await conn.query(`UPDATE hrm_additional_allowance_deduction_has_hrm_salary SET ? WHERE ID_ADDITIONAL_ALLOWANCE_DEDUCTION=${req.body.ID}`, { 'EFFECTIVE_MONTH': req.body.EFFECTIVE_MONTH.substr(0, 7), 'AMOUNT': req.body.AMOUNT, 'REMARK': req.body.REMARK });
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, message: "Updated" });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addSalaryCycle', async function (req, res, next) {
    const API_NAME = 'addSalaryCycle post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    // console.log(req.body);

    const conn = await pool.getConnection();

    try {

        var a = moment(req.body.PERIOD[0]);
        var b = moment(req.body.PERIOD[1]);
        var type = 'months';
        var count = 1;

        if (req.body.CYCLE_TYPE != 'Monthly'){
            type = 'weeks';
            a = moment(req.body.PERIOD[0]).day(req.body.CALCULATION_DATE);
        } 
        else{
            a = moment(req.body.PERIOD[0]).date(req.body.CALCULATION_DATE);
        }

        if (req.body.CYCLE_TYPE == 'Biweekly') count = 2;

        await conn.query('START TRANSACTION');

        const [response, fields] = await conn.query('INSERT INTO hrm_salary_cycle SET ?',
        { CYCLE_NAME: req.body.CYCLE_NAME, CYCLE_TYPE: req.body.CYCLE_TYPE, CYCLE_PERIOD: req.body.PERIOD.toString(), CALCULATION_DATE: req.body.CALCULATION_DATE });

        for (var m = moment(a); m.isBefore(b); m.add(count, type)) {
            // console.log(m.format('YYYY-MM'));

            await conn.query('INSERT INTO hrm_salary_cycle_details SET ?',
        { ID_SALARY_CYCLE: response.insertId, CYCLE_MONTH:m.format('YYYY-MM').toString(), CALCULATION_DATE: req.body.PERIOD.toString(), CALCULATION_DATE: m.format('YYYY-MM-DD').toString() });
        }

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryCycle', async function (req, res, next) {
    const API_NAME = 'getSalaryCycle post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_salary_cycle WHERE IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/deleteSalaryCycle', async function (req, res, next) {
    const API_NAME = 'deleteSalaryCycle post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {

        const conn = await pool.getConnection();

        await conn.query(`UPDATE hrm_salary_cycle SET ? WHERE ID_SALARY_CYCLE=${req.body.ID}`, { 'IS_ACTIVE': 0 });
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, message: "Updated" });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryCycleDetails', async function (req, res, next) {
    const API_NAME = 'getSalaryCycleDetails post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_salary_cycle_details WHERE ID_SALARY_CYCLE=${req.body.ID} AND IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getSalaryCycleEmployee', async function (req, res, next) {
    const API_NAME = 'getSalaryCycleEmployee post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_salary_cycle_has_employee sa JOIN view_all_emp_profile_details em ON sa.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY JOIN hrm_salary hs ON sa.ID_EMPLOYEE_REGISTRY = hs.ID_EMPLOYEE_REGISTRY WHERE sa.ID_SALARY_CYCLE=${req.body.ID} AND sa.IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getEmployeeForSalaryCycle', async function (req, res, next) {
    const API_NAME = 'getEmployeeForSalaryCycle post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_salary sa JOIN view_all_emp_profile_details em ON sa.ID_EMPLOYEE_REGISTRY = em.ID_EMPLOYEE_REGISTRY  WHERE em.ID_EMPLOYEE_REGISTRY NOT IN (SELECT ID_EMPLOYEE_REGISTRY FROM hrm_salary_cycle_has_employee sa WHERE sa.IS_ACTIVE=1) AND sa.IS_ACTIVE=1`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addEmployeeForSalaryCycle', async function (req, res, next) {
    const API_NAME = 'addEmployeeForSalaryCycle post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    console.log(req.body);

    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        if (req.body.employee) {

            await req.body.employee.forEach(function (emp, n) {

                conn.query('INSERT INTO hrm_salary_cycle_has_employee SET ?',
                    { ID_SALARY_CYCLE: req.body.values.ID_SALARY_CYCLE, ID_EMPLOYEE_REGISTRY: emp.ID_EMPLOYEE_REGISTRY });
            });
        }

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Added' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

router.get('/getLeaveTypes', async function (req, res, next) {
    const API_NAME = 'getLeaveTypes post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_leave_type WHERE IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addLeaveType', async function (req, res, next) {
    const API_NAME = 'addLeaveType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    req.body.ID_DEPARTMENTS = req.body.ID_DEPARTMENTS.toString();
    req.body.ID_EMPLOYEE_DESIGNATIONS = req.body.ID_EMPLOYEE_DESIGNATIONS.toString();
    req.body.ID_LOCATIONS = req.body.ID_LOCATIONS.toString();

    try {
        const conn = await pool.getConnection();
        
        await conn.query('INSERT INTO hrm_leave_type SET ?', req.body);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Type Added' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateLeaveType', async function (req, res, next) {
    const API_NAME = 'updateLeaveType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    req.body.data.ID_DEPARTMENTS = req.body.data.ID_DEPARTMENTS.toString();
    req.body.data.ID_EMPLOYEE_DESIGNATIONS = req.body.data.ID_EMPLOYEE_DESIGNATIONS.toString();
    req.body.data.ID_LOCATIONS = req.body.data.ID_LOCATIONS.toString();

    try {
        const conn = await pool.getConnection();

        
        await conn.query('UPDATE hrm_leave_type SET ? where `ID_LEAVE_TYPE`='+req.body.ID, req.body.data);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Type Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/deleteLeaveType', async function (req, res, next) {
    const API_NAME = 'deleteLeaveType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        await conn.query('UPDATE hrm_leave_type SET `IS_ACTIVE`=0 where `ID_LEAVE_TYPE`='+req.body.ID);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Type Deleted' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.get('/getLeaveEntitlementObject', async function (req, res, next) {
    const API_NAME = 'getLeaveEntitlementObject post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_employee_leave_type_object WHERE IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.get('/getEmployeeForLeaveEntitlenemt', async function (req, res, next) {
    const API_NAME = 'getEmployeeForLeaveEntitlenemt post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM view_all_emp_profile_details WHERE IS_ACTIVE = '1' AND ID_EMPLOYEE_REGISTRY NOT IN (SELECT DISTINCT ID_EMPLOYEE_REGISTRY FROM hrm_employee_has_leave_type WHERE IS_ACTIVE=1)`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addLeaveEntitlement', async function (req, res, next) {
    const API_NAME = 'addLeaveEntitlement post ';
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let LEAVES = [];

    try {
        const conn = await pool.getConnection();

        req.body.keys.forEach(function(element) {

            let data = {
                ID_EMPLOYEE_REGISTRY : req.body.ID_EMPLOYEE,
                ID_LEAVE_TYPE: req.body.LEAVE_TYPE[element],
                COUNT: req.body.LEAVE_ENTITLEMENT[element],
                START_DATE: req.body.DATE_RANGE[0],
                END_DATE: req.body.DATE_RANGE[1]
            }

            LEAVES.push({
                ID_LEAVE_TYPE: req.body.LEAVE_TYPE[element],
                COUNT: req.body.LEAVE_ENTITLEMENT[element],
                PERIOD: req.body.DATE_RANGE,
            })

             conn.query('INSERT INTO hrm_employee_has_leave_type SET ?', data);

          });

          let object = {
            ID_EMPLOYEE_REGISTRY : req.body.ID_EMPLOYEE,
              OBJECT : JSON.stringify({LEAVES})
          }

          conn.query('INSERT INTO hrm_employee_leave_type_object SET ?', object);

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Entitlement Added' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateLeaveEntitlement', async function (req, res, next) {
    const API_NAME = 'updateLeaveEntitlement post ';
// console.log(req.body);
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let LEAVES = [];
    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');

        req.body.data.keys.forEach(function(element) {

            let data = {
                ID_EMPLOYEE_REGISTRY : req.body.data.ID_EMPLOYEE,
                ID_LEAVE_TYPE: req.body.data.LEAVE_TYPE[element],
                COUNT: req.body.data.LEAVE_ENTITLEMENT[element],
                START_DATE: req.body.data.DATE_RANGE[0],
                END_DATE: req.body.data.DATE_RANGE[1]
            }

            LEAVES.push({
                ID_LEAVE_TYPE: req.body.data.LEAVE_TYPE[element],
                COUNT: req.body.data.LEAVE_ENTITLEMENT[element],
                PERIOD: req.body.data.DATE_RANGE,
            })

            //  conn.query('UPDATE hrm_employee_has_leave_type SET ?', data);

          });

          let object = {
            ID_EMPLOYEE_REGISTRY : req.body.data.ID_EMPLOYEE,
              OBJECT : JSON.stringify({LEAVES})
          }

          await conn.query('UPDATE hrm_employee_leave_type_object SET ? where ID_EMPLOYEE_REGISTRY = '+req.body.data.ID_EMPLOYEE, {IS_ACTIVE:0});
          await conn.query('INSERT hrm_employee_leave_type_object SET ? ', object);

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Entitlement Update' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

module.exports = router;
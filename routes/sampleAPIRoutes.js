var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


router.post('/sampleAPI', function (req, res, next) {
    const API_NAME='sampleAPI post, ';

    //IMPORTANT
    //*********WHEN LOG, ALWAYS START WITH `API: {API_NAME}' ` *********

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.body.someAttribute){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'someAttribute not found'});
    }

    //logic here
    pool.getConnection(function(err, conn) {
        if(err){
            logger.error('Error ,' + err.message);
            return res.status(500).send({success:false, message:'DB Error'});
        }
        // Do something with the connection
        conn.query('SELECT * FROM core_acc_account_category', (err, rows, fields)=>{
            if(err){
                return res.status(500).send({success:false, message:'Query Error'});
            }

            // Don't forget to release the connection when finished!
            conn.release();

            return res.status(200).send({success:true, message:rows});
            
        });
        
     });
    
    

});

router.get('/sampleAwaitAPI', async function (req, res, next) {
    const API_NAME='sampleAwaitAPI get, ';

    //IMPORTANT
    //*********WHEN LOG, ALWAYS START WITH `API: {API_NAME}' ` *********

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    /* if(!req.body.someAttribute){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'someAttribute not found'});
    } */

    //logic here
    const c = await pool.getConnection();

    const [rows, fields] = await c.query('show databases');
    console.log(rows);
        
    /*  */
    
    

});



module.exports = router;
var express = require('express');
var router = express.Router();
var cron = require("node-cron");
var Finance = require('financejs');

let finance = new Finance();

var pool = require('../config/database').pool;

cron.schedule("26 18 8 * *", function () {
    holdChecker();
    loanSchedule();
});

async function holdChecker() {
    logger.info('Hold Checker Started');
    const conn = await pool.getConnection();
    try {
        const loanData = await conn.execute(`SELECT * FROM hrm_loan WHERE IS_ACTIVE = 1 AND TO_BE_HOLD = 1`);

        for (let i = 0; loanData[0].length > i; i++) {

            var holdData = await conn.execute(`SELECT * FROM hrm_loan_hold WHERE IS_ACTIVE = 1 AND ID_LOAN_REGISTRY=` + loanData[0][i]['ID_LOAN_REGISTRY']);
        
            var status = true;
            var holdStatus = true;
            var today = new Date();
            var maxDay;
            var fromDate;
            var toDate;

            for (let n = 0; holdData[0].length > n; n++) {


                var date = holdData[0][n].PERIOD.split(',');
                fromDate = new Date(date[0]);
                toDate = new Date(date[1]);
                if(n==0) maxDay = toDate;
                if(maxDay < toDate) maxDay = toDate;

                if(today > fromDate && today < toDate){
                    status = false;
                }
            }

                console.log('From Date: ',fromDate);
                console.log('Today: ',today);
                console.log('To Date: ',toDate);
                console.log('Max Day: ',maxDay);
                console.log('Status: ',status);
                console.log('Hold: ',holdStatus);

            if(today > maxDay) holdStatus = false;

            await conn.query('UPDATE hrm_loan SET ? WHERE ID_LOAN=' + loanData[0][i]['ID_LOAN'], {'LOAN_STATUS': status ? 'Issued' : 'Hold', 'TO_BE_HOLD': holdStatus ? 1 : 0 });
        }
    }
    catch (err) {
        logger.info(err);
    }
    conn.release();
}

async function loanSchedule() {
    logger.info('Loan Schedule Started');
    const conn = await pool.getConnection();
    try {
        const loanData = await conn.execute(`SELECT * FROM hrm_loan WHERE IS_ACTIVE = 1 AND LOAN_STATUS='Issued' AND EFFECTIVE_DATE >= CURDATE()`);

        for (let i = 0; loanData[0].length > i; i++) {

            var paymentData = await conn.execute(`SELECT * FROM hrm_loan_payment WHERE IS_ACTIVE = 1 AND TRANSACTION_DATE <= CURDATE() AND ID_LOAN_REGISTRY=` + loanData[0][i]['ID_LOAN_REGISTRY']);

            var installment = finance.AM(loanData[0][i]['LOAN_AMOUNT'], loanData[0][i]['INTEREST_RATE'], loanData[0][i]['LOAN_TERM'], 1);

            var bal_principal = loanData[0][i]['LOAN_AMOUNT'];

            var loan_schedule = [];

            for (let m = 0; loanData[0][i]['LOAN_TERM'] > m; m++) {

                var curr_interest = (bal_principal / 100) * (loanData[0][i]['INTEREST_RATE'] / 12);
                var curr_principal = installment - curr_interest;
                bal_principal = bal_principal - curr_principal;

                loan_schedule.push({
                    'NO': m + 1,
                    'CURR_PRINCIPAL': curr_principal.toFixed(2),
                    'CURR_INTEREST': curr_interest.toFixed(2),
                    'INSTALLMENT': (curr_interest + curr_principal).toFixed(2),
                    'BAL_PRINCIPAL': bal_principal.toFixed(2)
                });
            }

            var interest_topay = +loan_schedule[loanData[0][i]['CURRENT_TERM']]['CURR_INTEREST'] + +loanData[0][i]['OVER_DUE_INTEREST'];
            var principal_topay = +loan_schedule[loanData[0][i]['CURRENT_TERM']]['CURR_PRINCIPAL'] + +loanData[0][i]['OVER_DUE_PRINCIPAL'];
            var penalty_topay = loanData[0][i]['PENALTY_AMOUNT'];
            let totalPay = +loanData[0][i]['OVER_PAY_AMOUNT'];
            let totalPayLedger = 0;
            var paymentIds = [];

            for (let n = 0; paymentData[0].length > n; n++) {

                totalPay = +totalPay + +paymentData[0][n]['AMOUNT'];
                totalPayLedger = +totalPayLedger + +paymentData[0][n]['AMOUNT'];
                paymentIds.push(paymentData[0][n]['ID_LOAN_PAYMENT']);
            }

            var over_paid = 0;
            var penalty_paid = 0;
            var interest_paid = 0;
            var principal_paid = 0;
            var penalty_balance = penalty_topay;
            var interest_balance = interest_topay;
            var principal_balance = principal_topay;

            if (totalPay < penalty_topay) {

                penalty_paid = totalPay;
                penalty_balance = penalty_topay - totalPay;
            }
            else {

                totalPay = totalPay - penalty_topay;
                penalty_paid = penalty_topay;
                penalty_balance = 0;

                if (totalPay < interest_topay) {

                    interest_paid = totalPay;
                    interest_balance = interest_topay - totalPay;
                }
                else {

                    totalPay = totalPay - interest_topay;
                    interest_paid = interest_topay;
                    interest_balance = 0;

                    if (totalPay < principal_topay) {

                        principal_paid = totalPay;
                        principal_balance = principal_topay - totalPay;
                    }
                    else {

                        totalPay = totalPay - principal_topay;
                        principal_paid = principal_topay;
                        principal_balance = 0;
                        over_paid = totalPay;
                    }
                }
            }

            var penalty;

            try {
                penalty = ((interest_balance + principal_balance + penalty_balance) / 100) * (loanData[0][i]['PENALTY_RATE'] / 12);
            }
            catch (err) {

                logger.error(err);
                penalty = 0;
            }

            var next_term;

            if (loanData[0][i]['LOAN_TERM'] > loanData[0][i]['CURRENT_TERM']) {

                next_term = +loanData[0][i]['CURRENT_TERM'] + 1;
            }
            else {
                next_term = loanData[0][i]['LOAN_TERM'];
            }

            var status;

            if (loanData[0][i]['LOAN_TERM'] == loanData[0][i]['CURRENT_TERM'] && principal_balance <= 0 && interest_balance <= 0 && penalty <= 0) {

                status = 'Settled';
            }

            else{

                status = loanData[0][i]['LOAN_STATUS'];
            }

            var insertLoanData = {
                'OVER_PAY_AMOUNT': over_paid,
                'OVER_DUE_PRINCIPAL': principal_balance,
                'OVER_DUE_INTEREST': interest_balance,
                'PENALTY_AMOUNT': penalty,
                'CURRENT_TERM': next_term,
                'LOAN_STATUS': status
            }

            await conn.query('UPDATE hrm_loan SET ? WHERE ID_LOAN=' + loanData[0][i]['ID_LOAN'], insertLoanData);

            var insertLedgerInstallmentData = {
                'ID_LOAN_REGISTRY': loanData[0][i]['ID_LOAN_REGISTRY'],
                'PRINCIPAL': loan_schedule[loanData[0][i]['CURRENT_TERM']]['CURR_PRINCIPAL'],
                'INTEREST': loan_schedule[loanData[0][i]['CURRENT_TERM']]['CURR_INTEREST'],
                'PENALTY': loanData[0][i]['PENALTY_AMOUNT'],
                'OVER_PAY': loanData[0][i]['OVER_PAY_AMOUNT'],
                'OVER_DUE': +loanData[0][i]['OVER_DUE_PRINCIPAL'] + +loanData[0][i]['OVER_DUE_INTEREST']
            }

            await conn.query('INSERT INTO hrm_loan_ledger SET ?', insertLedgerInstallmentData);

            var insertLedgerPaymentData = {
                'ID_LOAN_REGISTRY': loanData[0][i]['ID_LOAN_REGISTRY'],
                'PAYMENT': totalPayLedger,
                'ID_PAYMENTS': paymentIds.toString(),
                'PRINCIPAL': -principal_paid,
                'INTEREST': -interest_paid,
                'PENALTY': -penalty_paid,
                'OVER_PAY': over_paid,
                'OVER_DUE': principal_balance + +interest_balance
            }

            await conn.query('INSERT INTO hrm_loan_ledger SET ?', insertLedgerPaymentData);

            paymentIds.forEach( function(id) {

                conn.query('UPDATE hrm_loan_payment SET ? WHERE ID_LOAN_PAYMENT=' + id, {IS_PROCESSED:1});
            });
            

            logger.info('Loan ID: '+loanData[0][i]['ID_LOAN']+' Processed');
        }
    }
    catch (err) {
        logger.info(err);
    }
    conn.release();
    logger.info('Loan Schedule Finished');
}

router.get('/getLoanTypes', async function (req, res, next) {
    const API_NAME = 'getLoanTypes post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const values = await conn.execute(`SELECT * FROM hrm_loan_type WHERE IS_ACTIVE = '1'`);
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/addLoanType', async function (req, res, next) {
    const API_NAME = 'addLoanType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    delete req.body['IS_DURATION'];

    req.body.ID_DEPARTMENTS = req.body.ID_DEPARTMENTS.toString();
    req.body.ID_EMPLOYEE_DESIGNATIONS = req.body.ID_EMPLOYEE_DESIGNATIONS.toString();
    req.body.ID_LOCATIONS = req.body.ID_LOCATIONS.toString();
    req.body.DURATION = req.body.DURATION.toString();

    try {
        const conn = await pool.getConnection();

        await conn.query('INSERT INTO hrm_loan_type SET ?', req.body);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Loan Type Added' });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/deleteLoanType', async function (req, res, next) {
    const API_NAME = 'deleteLoanType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        await conn.query('UPDATE hrm_loan_type SET `IS_ACTIVE`=0 where `ID_LOAN_TYPE`=' + req.body.ID);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Leave Type Deleted' });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateLoanType', async function (req, res, next) {
    const API_NAME = 'updateLoanType post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    delete req.body.data['IS_DURATION'];

    req.body.data.ID_DEPARTMENTS = req.body.data.ID_DEPARTMENTS.toString();
    req.body.data.ID_EMPLOYEE_DESIGNATIONS = req.body.data.ID_EMPLOYEE_DESIGNATIONS.toString();
    req.body.data.ID_LOCATIONS = req.body.data.ID_LOCATIONS.toString();
    req.body.data.DURATION = req.body.data.DURATION.toString();

    try {
        const conn = await pool.getConnection();

        await conn.query('UPDATE hrm_loan_type SET ? where `ID_LOAN_TYPE`=' + req.body.ID, req.body.data);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Loan Type Updated' });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/requestLoan', async function (req, res, next) {
    const API_NAME = 'requestLoan post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    delete req.body['DESCRIPTION'];

    const conn = await pool.getConnection();

    try {

        await conn.query('START TRANSACTION');
        const [response, fields] = await conn.query('INSERT INTO hrm_loan_registry SET ?', { IS_ACTIVE: 1 });
        req.body["ID_LOAN_REGISTRY"] = response.insertId;
        await conn.query('INSERT INTO hrm_loan SET ?', req.body);
        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Loan Requested' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/getLoanList', async function (req, res, next) {
    const API_NAME = 'getLoanList post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        var values = [];
        if (req.body.type) {
            values = await conn.execute(`SELECT * FROM view_hrm_loan WHERE IS_ACTIVE = '1' and LOAN_STATUS='` + req.body.type + `'`);
        }
        else {
            values = await conn.execute(`SELECT * FROM view_hrm_loan WHERE IS_ACTIVE = '1'`);
        }
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/inspectLoan', async function (req, res, next) {
    const API_NAME = 'inspectLoan post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    const conn = await pool.getConnection();

    try {
        await conn.query('START TRANSACTION');

        await conn.query('UPDATE hrm_loan SET ? where ID_LOAN_REGISTRY=' + req.body.id, { IS_ACTIVE: 0 });
        await conn.query('INSERT INTO hrm_loan SET ?', req.body.data);

        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Loan Requested' });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/payment', async function (req, res, next) {
    const API_NAME = 'payment post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        await conn.query('INSERT INTO hrm_loan_payment SET ?', req.body);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Loan Requested' });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getPaymentList', async function (req, res, next) {
    const API_NAME = 'getPaymentList post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        var values = [];
        if (req.body.id) {
            values = await conn.execute(`SELECT * FROM hrm_loan_payment WHERE IS_ACTIVE = '1' and ID_LOAN_REGISTRY='` + req.body.id + `'`);
        }
        else {
            values = await conn.execute(`SELECT * FROM hrm_loan_payment WHERE IS_ACTIVE = '1'`);
        }
        conn.release();

        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: values[0] });
    }

    catch (err) {
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/settlement', async function (req, res, next) {
    const API_NAME = 'settlement post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    logger.info(req.body);

    const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

    try {

        const data1 = {
            LOAN_STATUS: req.body.LOAN_STATUS
        };

        values = await conn.execute(`SELECT * FROM hrm_loan WHERE IS_ACTIVE = '1' and ID_LOAN='` + req.body.ID_LOAN + `'`);

        await conn.query('UPDATE hrm_loan SET ? where ID_LOAN_REGISTRY=' + req.body.ID_LOAN_REGISTRY, { IS_ACTIVE: 0 });

        await delete values[0][0]['ID_LOAN'];

        if (req.body.LOAN_STATUS == "Hold") {

            values[0][0]['TO_BE_HOLD'] = 1;

            const data2 = {
                PERIOD: req.body.PERIOD.toString(),
                ID_LOAN_REGISTRY: req.body.ID_LOAN_REGISTRY
            };

            await conn.query('INSERT INTO hrm_loan_hold SET ?', data2);
        }

        else{
            values[0][0]['LOAN_STATUS'] = req.body.LOAN_STATUS;
            values[0][0]['TO_BE_HOLD'] = 0;
        }

        await conn.query('INSERT INTO hrm_loan SET ?', values[0][0]);
        await conn.query('COMMIT');
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: `Loan ${req.body.LOAN_STATUS}` });
    }

    catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

module.exports = router;
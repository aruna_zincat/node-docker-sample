var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


router.get('/getDayTypes',async function (req, res, next) {
    const API_NAME='getDayTypes get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            ID_DAY_TYPES,
            DAY_NAME
        FROM
            hrm_calendar_day_types 
        WHERE
            IS_ACTIVE = '1' AND DAY_TYPE='0' `);

        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.post('/getHolidaysOfDate',async function (req, res, next) {
    const API_NAME='getHolidaysOfDate post, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.body.date){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'date parameter not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            policy.*,
            dayType.DAY_NAME
        FROM
            hrm_calendar_holiday_policy policy 
        INNER JOIN
            hrm_calendar_day_types dayType
                ON policy.ID_DAY_TYPES = dayType.ID_DAY_TYPES
        WHERE
            policy.IS_ACTIVE = '1' AND
            DATE = ?
        `, [req.body.date]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        try{
            conn.release();
        }
        catch(error){
            logger.info("Cant release connection");
        }
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.post('/getHolidaysOfYear',async function (req, res, next) {
    const API_NAME='getHolidaysOfYear post, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.body.year){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'year not found'});
    }
    var conn;

    //logic here
    try{
        conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            CALENDAR.*,
            DEPARTMENT.DEPARTMENT,
            DESIGNATION.DESIGNATION,
            LOCATION.LOCATION_NAME,
            MARITAL_STATUS.MARITAL_STATUS
        FROM
            hrm_calendar_holiday_policy CALENDAR
                LEFT JOIN
            hrm_defaults_department DEPARTMENT ON CALENDAR.ID_DEPARTMENT = DEPARTMENT.ID_DEPARTMENT
                LEFT JOIN
            hrm_defaults_employee_designation DESIGNATION ON CALENDAR.ID_EMPLOYEE_DESIGNATION = DESIGNATION.ID_EMPLOYEE_DESIGNATION
                LEFT JOIN
            hrm_defaults_location LOCATION ON LOCATION.ID_LOCATION = CALENDAR.ID_LOCATION
                LEFT JOIN
            hrm_defaults_marital_status MARITAL_STATUS ON CALENDAR.ID_MARITAL_STATUS = MARITAL_STATUS.ID_MARITAL_STATUS
        WHERE
            YEAR(CALENDAR.DATE) = ? 
                AND
            CALENDAR.IS_ACTIVE = 1
        ORDER BY CALENDAR.date ASC
        ;
        `, [req.body.year]);


        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        
        logger.error(API_NAME + 'error :' + err);
        if(conn !== undefined){
            conn.release();
        }
        return res.status(500).send({success:false, message:'Query Error'});
    }
});


router.post('/makeHolidayInactive',async function (req, res, next) {
    const API_NAME='makeHolidayInactive post, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.body.ID_HOLIDAY_POLICY){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'year not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        UPDATE hrm_calendar_holiday_policy 
        SET 
            IS_ACTIVE = 0
        WHERE
            ID_HOLIDAY_POLICY = ?;
        `, [req.body.ID_HOLIDAY_POLICY]);

        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true});
    }

    catch(err){
        conn.release();
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.post('/createHoliday',async function (req, res, next) {
    const API_NAME='createHoliday post, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.body.holiday){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'Parameter not found'});
    }

    let holiday = req.body.holiday;
    holiday.IS_ACTIVE = 1;

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.query(`
        INSERT INTO hrm_calendar_holiday_policy SET ?;
        `, holiday);

        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true});
    }

    catch(err){
        /* if(conn){
            conn.release();
        } */
        
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

module.exports = router;
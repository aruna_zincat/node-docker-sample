var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

//////////////////////////////////Course////////////////////////////////


// get Courses all
router.get('/getAllCourses', async function (req, res) {

    const API_NAME = 'getAllCourses GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get Courses
        const [result, fields] = await conn.query('select ID_TRAINING_COURSES, NAME from hrm_training_courses where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL COURSES SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL COURSES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Course details
router.post('/getCourses', async function (req, res, next) {
    const API_NAME = 'getCourses POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get countries
        const [resultData, fields] = await conn.query("select * from view_training_course where IS_ACTIVE=1  and (CODE like '%" + receivedObj.searchVal + "%' or NAME like '%" + receivedObj.searchVal + "%') order by ID_TRAINING_COURSES desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from view_training_course where IS_ACTIVE=1 and (CODE like '%" + receivedObj.searchVal + "%' or NAME like '%" + receivedObj.searchVal + "%')");

        // resultData.count = resultDataCount[0].count_of;
        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT COURSE DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET COURSE DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save course
router.post('/saveCourse', async function (req, res, next) {
    const API_NAME = 'saveCourse POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await 
        conn.query("select * from hrm_training_courses where IS_ACTIVE=1 and CODE='"
         + receivedObj.code + "' and NAME='" + receivedObj.name + "'");
        
        if(receivedObj.type !== 'UPDATE' && resultData.length > 0) {
            return res.status(200).send({ success: true, message: 'EXIST' });
        } else {

            if (undefined == receivedObj.idCourse || null == receivedObj.idCourse || 0 == receivedObj.idCourse) {
    
                if(resultData.length > 0) {
                    logger.info('returned with EXIST');
                    return res.status(200).send({ success: true, message: 'EXIST' });
                }            
            } else {
                // update old course details row
                const [resultUpdateData, fields] = await conn.query("update hrm_training_courses set IS_ACTIVE=0  where ID_TRAINING_COURSES=" + receivedObj.idCourse + "");
                logger.info('Successfully Updated Course detail record ');
            } 
            // set data to insert 
            var course = {
                CODE: receivedObj.code,
                NAME: receivedObj.name,
                COORDINATOR: receivedObj.coordinator,
                TRAINER: receivedObj.trainer,
                TRAINER_DETAILS: receivedObj.trainerDetails,
                COST: receivedObj.cost,
                ID_CURRENCY: receivedObj.idCurrency,
                ID_PAY_TYPE: receivedObj.idPayType,
                STATUS: receivedObj.status ? 1 : 0,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_training_courses SET ?', course);
            logger.info('Successfully saved Course record id = ' + resultSave.insertId);
            await conn.query('COMMIT');
            conn.release();
            logger.info('COURSE CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }    

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('COURSE CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete course
router.post('/actionCourse', async function (req, res, next) {
    const API_NAME = 'actionCourse POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old course details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_training_courses set IS_ACTIVE=0 where ID_TRAINING_COURSES=" + receivedObj.idCourse + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_training_courses set STATUS=1 where ID_TRAINING_COURSES=" + receivedObj.idCourse + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_training_courses set STATUS=0 where ID_TRAINING_COURSES=" + receivedObj.idCourse + "");
                }

        logger.info('Successfully Updated Course detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('COURSE DELETION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('COURSE DELETION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

//////////////////////////////////Training Session////////////////////////////////
// get training sessions all
router.get('/getAllTrainingSession', async function (req, res) {

    const API_NAME = 'getAllTrainingSession GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get training sessions
        const [result, fields] = await conn.query('select ID_TRAINING_SESSION, NAME from hrm_training_session where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL TRAINING SESSIONS SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL TRAINING SESSIONS DUE TO :- ', err);
        return res.json({ success: false });
    }

});
// get Training Session details
router.post('/getTrainingSessions', async function (req, res, next) {
    const API_NAME = 'getTrainingSessions POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log('rec ', receivedObj)
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get countries
        const [resultData, fields] = await conn.query("select * from view_training_session where IS_ACTIVE=1 and (NAME like '%" + receivedObj.searchVal  + "%') order by ID_TRAINING_SESSION desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from view_training_session where IS_ACTIVE=1 and (NAME like '%" + receivedObj.searchVal  + "%')");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TRAINING SESSIONS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TRAINING SESSIONS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save training session
router.post('/saveTrainingSession', async function (req, res, next) {
    const API_NAME = 'saveTrainingSession POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await 
        conn.query("select * from hrm_training_session where IS_ACTIVE=1 and SCHEDULE_TIME='"
         + receivedObj.scheduleTime + "' and NAME='" + receivedObj.name + "'");
        
        if(receivedObj.type !== 'UPDATE' && resultData.length > 0) {
            return res.status(200).send({ success: true, message: 'EXIST' });
        } else {

            if (undefined == receivedObj.idTrainingSession || null == receivedObj.idTrainingSession || 0 == receivedObj.idTrainingSession) {
    
                if(resultData.length > 0) {
                    logger.info('returned with EXIST');
                    return res.status(200).send({ success: true, message: 'EXIST' });
                }            
            } else {
                // update old training session details row
                const [resultUpdateData, fields] = await conn.query("update hrm_training_session set IS_ACTIVE=0  where ID_TRAINING_SESSION=" + receivedObj.idTrainingSession + "");
                logger.info('Successfully Updated Training Session detail record ');
            } 
            // set data to insert 
            var session = {
                NAME: receivedObj.name,
                ID_COURSE: receivedObj.idCourse,
                DETAILS: receivedObj.details,
                SCHEDULE_TIME: receivedObj.scheduleTime,
                ASSIGNMENT_DUE_DATE: receivedObj.assignmentDueDate,
                ID_DELIVERY_METHOD: receivedObj.idDeliveryMethod,
                DELIVERY_LOCATION: receivedObj.deliveryLocation,
                ID_ATTENDANCE_TYPE: receivedObj.idAttendanceType,
                CERTIFICATE_REQUIRED: receivedObj.certificateRequired ? 1 : 0,
                STATUS: receivedObj.status ? 1 : 0,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_training_session SET ?', session);
            logger.info('Successfully saved Training Session record id = ' + resultSave.insertId);
            await conn.query('COMMIT');
            conn.release();
            logger.info('TRAINING SESSION CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }    

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('TRAINING SESSION CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete training session
router.post('/actionTrainingSession', async function (req, res, next) {
    const API_NAME = 'actionTrainingSession POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old training session details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_training_session set IS_ACTIVE=0 where ID_TRAINING_SESSION=" + receivedObj.idTrainingSession + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_training_session set STATUS=1 where ID_TRAINING_SESSION=" + receivedObj.idTrainingSession + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_training_session set STATUS=0 where ID_TRAINING_SESSION=" + receivedObj.idTrainingSession + "");
                }

        logger.info('Successfully Updated Training Session detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('TRAINING SESSION DELETION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('TRAINING SESSION DELETION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// ////////////////////////////////////Employee Training Session//////////////////////////////////////////

// get Employee Sessions all
router.get('/getAllEmployeeSession', async function (req, res) {

    const API_NAME = 'getAllEmployeeSession GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get Employee Sessions
        const [result, fields] = await conn.query('select * from hrm_training_employee_session where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL EMPLOYEE TRAINING SESSIONS SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL EMPLOYEE TRAINING SESSIONS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Employee Session details
router.post('/getEmployeeSession', async function (req, res, next) {
    const API_NAME = 'getEmployeeSession POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get Employee Sessions
        const [resultData, fields] = await conn.query("select * from view_training_employee_session where IS_ACTIVE=1  and (EMPLOYEE like '%" + receivedObj.searchVal + "%' or TRAINING_SESSION like '%" + receivedObj.searchVal + "%') order by ID_TRAINING_EMPLOYEE_SESSION desc limit 5 offset " + receivedObj.pageSize);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from view_training_employee_session where IS_ACTIVE=1 and (EMPLOYEE like '%" + receivedObj.searchVal + "%' or TRAINING_SESSION like '%" + receivedObj.searchVal + "%')");

        // resultData.count = resultDataCount[0].count_of;
        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE TRAINING SESSION DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET EMPLOYEE TRAINING SESSION DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// save Employee Session
router.post('/saveEmployeeSession', async function (req, res, next) {
    const API_NAME = 'saveEmployeeSession POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await 
        conn.query("select * from hrm_training_employee_session where IS_ACTIVE=1 and ID_EMPLOYEE_PROFILE='"
         + receivedObj.idEmployeeProfile + "' and ID_TRAINING_SESSION='" + receivedObj.idTrainingSession + "'");
        
        if(receivedObj.type !== 'UPDATE' && resultData.length > 0) {
            return res.status(200).send({ success: true, message: 'EXIST' });
        } else {

            if (undefined == receivedObj.idTrainingEmployeeSession || null == receivedObj.idTrainingEmployeeSession || 0 == receivedObj.idTrainingEmployeeSession) {
    
                if(resultData.length > 0) {
                    logger.info('returned with EXIST');
                    return res.status(200).send({ success: true, message: 'EXIST' });
                }            
            } else {
                // update old Employee Session details row
                const [resultUpdateData, fields] = await conn.query("update hrm_training_employee_session set IS_ACTIVE=0  where ID_TRAINING_EMPLOYEE_SESSION=" + receivedObj.idTrainingEmployeeSession + "");
                logger.info('Successfully Updated Course detail record ');
            } 
            // set data to insert 
            var session = {
                ID_EMPLOYEE_PROFILE: receivedObj.idEmployeeProfile,
                ID_TRAINING_SESSION: receivedObj.idTrainingSession,
                ID_TRAINING_EMPLOYEE_SESSION_STATUS: receivedObj.idTrainingEmployeeSessionStatus,
                STATUS: receivedObj.status ? 1 : 0,
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            };

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_training_employee_session SET ?', session);
            logger.info('Successfully saved Training Employee Session record id = ' + resultSave.insertId);
            await conn.query('COMMIT');
            conn.release();
            logger.info('EMPLOYEE TRAINING SESSION CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
            return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });
        }    

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('EMPLOYEE TRAINING SESSION CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete Employee Session
router.post('/actionEmployeeSession', async function (req, res, next) {
    const API_NAME = 'actionEmployeeSession POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old Employee Sessions details row
        if ('DELETE' == receivedObj.action) {
            const [resultUpdateData, fields] = await conn.query("update hrm_training_employee_session set IS_ACTIVE=0 where ID_TRAINING_COURSES=" + receivedObj.idTrainingEmployeeSession + "");
        } else

            if ('ACTIVE' == receivedObj.action) {
                const [resultUpdateData, fields] = await conn.query("update hrm_training_employee_session set STATUS=1 where ID_TRAINING_COURSES=" + receivedObj.idTrainingEmployeeSession + "");
            } else

                if ('DEACTIVE' == receivedObj.action) {
                    const [resultUpdateData, fields] = await conn.query("update hrm_training_employee_session set STATUS=0 where ID_TRAINING_COURSES=" + receivedObj.idTrainingEmployeeSession + "");
                }

        logger.info('Successfully Updated Training Employee Session detail record ');
        await conn.query('COMMIT');
        conn.release();
        logger.info('EMPLOYEE TRAINING SESSION DELETION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('EMPLOYEE TRAINING SESSION DELETION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});


///////////////////////////////Util//////////////////////////////////////////////////////////////

// get Training Employee Session Status
router.get('/getTrainingEmployeeSessionStatus', async function (req, res) {

    const API_NAME = 'getTrainingEmployeeSessionStatus GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get Training Employee Session Status
        const [result, fields] = await conn.query('select ID_TRAINING_EMPLOYEE_SESSION_STATUS, NAME from hrm_training_employee_session_ststus where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL TRAINING EMPLOYEE SESSIONS SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL TRAINING EMPLOYEE SESSIONS  DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Trainin gSession Delivery
router.get('/getTrainingSessionDelivery', async function (req, res) {

    const API_NAME = 'getTrainingSessionDelivery GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get Trainin gSession Delivery
        const [result, fields] = await conn.query('select ID_TRAINING_DELIVERY_METHOD, NAME from hrm_training_delivery_method where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL TRAINING SESSION DELIVERY SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL TTRAINING SESSION DELIVERY  DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get Training Session Attendance Type
router.get('/getTrainingSessionAttendanceType', async function (req, res) {

    const API_NAME = 'getTrainingSessionAttendanceType GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get Training Session Attendance Type
        const [result, fields] = await conn.query('select ID_TRAINING_ATTENDANCE_TYPE, NAME from hrm_training_attendance_type where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ALL TRAINING SESSION ATTENDANCE TYPE SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        if(conn !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET ALL TRAINING SESSION ATTENDANCE TYPE  DUE TO :- ', err);
        return res.json({ success: false });
    }

});

module.exports = router;
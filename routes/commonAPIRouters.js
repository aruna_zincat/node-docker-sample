/* API HISTORY */
/*

//HISTORY TABLE
=======================================================================================================================================================================
NO      API NAME                                METHOD                          PARMS(examples)                     AUTHOR                          DESCRIPTION
=======================================================================================================================================================================
01      /getDesignations                        GET                                                                 SHEYAN                          get all employee designations
------------------------------------------------------------------------------------------------------------------------------------------------------------------
02      /getEmployments                         GET                                                                 SHEYAN                          get all employee employments
------------------------------------------------------------------------------------------------------------------------------------------------------------------
03      /getDepartments                         GET                                                                 SHEYAN                          get all employee employments
------------------------------------------------------------------------------------------------------------------------------------------------------------------
05      /getDepartments                         GET                                                                 SHEYAN                          get all employee departments
------------------------------------------------------------------------------------------------------------------------------------------------------------------
06      /getGrades                              GET                                                                 SHYEAN                          get all employee grade
------------------------------------------------------------------------------------------------------------------------------------------------------------------
07      /getLocations                           GET                                                                 SHEYAN                          get all employee locations
------------------------------------------------------------------------------------------------------------------------------------------------------------------
08      /getEmpRegistryDetails                  POST                        {selectedEmpRegId} or {0}               SHEYAN                          get all employee registry details and get employee registry details where selected emp registry id  
------------------------------------------------------------------------------------------------------------------------------------------------------------------
09      /getEmployeeProfileDetails              POST                        {selectedEmpRegId} or {0}               SHEYAN                          get all employee profile details and get employee profile details where selected emp registry id  
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
10      /getEmployeeProfileDetailsCommonId      POST                        {
	                                                                            "idType": "DESIGNATION(any)",       SHEYAN                          get all employee profile details where selected common id  
	                                                                            "id": 1
                                                                            }
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
11      /getEmployeeProfileDetailsCommonId      POST                        {
	                                                                            "filters":
                                                                                    {
                                                                                    "ID_DEPARTMENT": 1              HASHAN
                                                                                    },
                                                                                "results":5,
                                                                                "page": 1,
                                                                                "sortField": "sorter.field",
                                                                                "sortOrder": "DESC"
                                                                            }
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
12      /getEmployeeProfileDetailsFromView      POST                        {
	                                                                            "filters":                                                         get all employee details from the view_all_emp_profile_details
                                                                                    {
                                                                                    "ID_DEPARTMENT": 1              ACHIRA
                                                                                    },
                                                                                "results":5,
                                                                                "page": 1,
                                                                                "sortField": "sorter.field",
                                                                                "sortOrder": "DESC"
                                                                            }
------------------------------------------------------------------------------------------------------------------------------------------------------------------
13      /getMaritalStatus                        GET                                                                 HASHAN                          get all employee grade
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
14      /validator                              POST                        {
                                                                                "table":"hrm_loan_type",
                                                                                "column": "LOAN_TYPE",               ARUNA
                                                                                "value": "Housing"
                                                                            }
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
15      /validatorEdit                          POST                        {
                                                                                "table":"hrm_loan_type",
                                                                                "column": "LOAN_TYPE",               ARUNA
                                                                                "value": "Housing"
                                                                                "idColumn": "ID_LOAN_TYPE"
                                                                                "id": 1
                                                                            }

====================================================================================================================================================================

*/

var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


// get all employee designations
router.get('/getDesignations', async function (req, res) {

    const API_NAME = 'getDesignation GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultEmployeeDesignations, fields] = await conn.query('select * from hrm_defaults_employee_designation where IS_ACTIVE=1 order by ID_EMPLOYEE_DESIGNATION desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE DESIGNATIONS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeeDesignations });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE DESIGNATIONS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee employments
router.get('/getEmployments', async function (req, res) {

    const API_NAME = 'getEmployments GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee employments
        const [resultEmployeEmployments, fields] = await conn.query('select * from hrm_employee_employment where IS_ACTIVE=1 order by ID_EMPLOYMENT desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE EMPLOYMRNTS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeEmployments });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE EMPLOYMRNTS DUE TO :- ', err);
        return res.json({ success: false });
    }

});




// get all employee departments
router.get('/getDepartments', async function (req, res) {

    const API_NAME = 'getDepartments GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee departments
        const [resultEmployeDepartments, fields] = await conn.query('select * from hrm_defaults_department where IS_ACTIVE=1 order by ID_DEPARTMENT desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE DEPARTMENTS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeDepartments });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE DEPARTMENTS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee grade
router.get('/getGrades', async function (req, res) {

    const API_NAME = 'getGrades GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultEmployeGrades, fields] = await conn.query('select * from hrm_defaults_employee_grade where IS_ACTIVE=1 order by ID_EMPLOYEE_GRADE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE GRADES SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeGrades });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE GRADES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee locations
router.get('/getLocations', async function (req, res) {

    const API_NAME = 'getLocations GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultEmployeLocations, fields] = await conn.query('select * from hrm_defaults_location where IS_ACTIVE=1 order by ID_LOCATION desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE LOCATIONS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeLocations });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE LOCATIONS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee groups
router.get('/getGroups', async function (req, res) {

    const API_NAME = 'getGroups GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultEmployeGroups, fields] = await conn.query('select * from hrm_employee_group where IS_ACTIVE=1 order by ID_EMPLOYEE_GROUP desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE GROUPS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeGroups });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE GROUP DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all lEmpRegistryDetails  
router.post('/getEmpRegistryDetails', async function (req, res) {

    const API_NAME = 'getEmpRegistryDetails GET, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get employee registry details
        var queryGetEmpRegistryDetails = ''
        if (0 == receivedObj.selectedEmpRegId) {
            queryGetEmpRegistryDetails = 'select * from hrm_employee_registry where IS_ACTIVE=1 order by ID_EMPLOYEE_REGISTRY desc';
        } else {
            queryGetEmpRegistryDetails = "select * from hrm_employee_registry where ID_EMPLOYEE_REGISTRY=" + receivedObj.selectedEmpRegId + " and IS_ACTIVE=1";
        }
        const [resultEmployeeRegistryDetails, fields] = await conn.query(queryGetEmpRegistryDetails);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE REGISTRY DETAILS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeeRegistryDetails });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE REGISTRY DETAILS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee profile details and get employee profile details where selected emp id  
router.post('/getEmployeeProfileDetails', async function (req, res) {

    const API_NAME = 'getEmployeeProfileDetails POST, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get all employee profile details
        var queryGetEmpProfileDetails = ''
        if (0 == receivedObj.selectedEmpRegId) {
            queryGetEmpProfileDetails = 'select * from view_all_emp_profile_details where IS_ACTIVE=1 order by ID_EMPLOYEE_REGISTRY desc';
        } else {
            queryGetEmpProfileDetails = "select * from view_all_emp_profile_details where ID_EMPLOYEE_REGISTRY=" + receivedObj.selectedEmpRegId + " and IS_ACTIVE=1";
        }
        const [resultEmployeeProfileDetails, fields] = await conn.query(queryGetEmpProfileDetails);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE PROFILE DETAILS SUCCESSFULLY...');
        return res.json({ success: true, resultEmployeeProfileDetails });

    } catch (err) {
        if (undefined != conn) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET EMPLOYEE PROFILE DETAILS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee shift details 
router.get('/getShifts', async function (req, res) {

    const API_NAME = 'getShifts GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultShifts, fields] = await conn.query('select * from hrm_shift where IS_ACTIVE=1 order by ID_SHIFT desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE SHIFT SUCCESSFULLY...');
        return res.json({ success: true, resultShifts });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE SHIFT DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee title details 
router.get('/getTitles', async function (req, res) {

    const API_NAME = 'getTitles GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultTitles, fields] = await conn.query('select * from hrm_defaults_title where IS_ACTIVE=1 order by ID_TITLE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE TITLE SUCCESSFULLY...');
        return res.json({ success: true, resultTitles });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE TITLE DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee Marital Status details 
router.get('/getMaritalStatus', async function (req, res) {

    const API_NAME = 'getMaritalStatus GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultMaritialStatus, fields] = await conn.query('select * from hrm_defaults_marital_status where IS_ACTIVE=1 order by ID_MARITAL_STATUS desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE MARITIAL STATUS SUCCESSFULLY...');
        return res.json({ success: true, resultMaritialStatus });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET EMPLOYEE MARITIAL STATUS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all banks
router.get('/geBanks', async function (req, res) {

    const API_NAME = 'geBanks GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultBanks, fields] = await conn.query('select * from hrm_defaults_bank where IS_ACTIVE=1 order by ID_BANK desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT BANKS SUCCESSFULLY...');
        return res.json({ success: true, resultBanks });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET BANKS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all pay types
router.get('/getPayTypes', async function (req, res) {

    const API_NAME = 'getPayTypes GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultPayTypes, fields] = await conn.query('select * from hrm_defaults_pay_typs where IS_ACTIVE=1 order by ID_PAY_TYPE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT PAY TYPES SUCCESSFULLY...');
        return res.json({ success: true, resultPayTypes });

    } catch (err) {
        if (con !== undefined) {
            await conn.query('ROLLBACK');
            conn.release();
        }
        logger.error('CANNOT GET PAY TYPES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all Type Of Accounts
router.get('/getTypeOfAccounts', async function (req, res) {

    const API_NAME = 'getTypeOfAccounts GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultAccountTypes, fields] = await conn.query('select * from hrm_defaults_account_type where IS_ACTIVE=1 order by ID_ACCOUNT_TYPE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TYPE OF ACCOUNT SUCCESSFULLY...');
        return res.json({ success: true, resultAccountTypes });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TYPE OF ACCOUNT DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// // get all employee profile details where selected common id  
// router.post('/getEmployeeProfileDetailsCommonId', async function (req, res) {

//     const API_NAME = 'getEmployeeProfileDetailsCommonId POST, ';
//     logger.info(API_NAME + ' called');

//     var receivedObj = req.body;

//     try {
//         const conn = await pool.getConnection();
//         await conn.query('START TRANSACTION');

//         // get all employee profile details
//         var queryGetEmpProfileDetails = ''
//         if('DESIGNATION' == receivedObj.idType){
//             queryGetEmpProfileDetails = "select * from view_all_emp_profile_details where IS_ACTIVE=1 and ID_EMPLOYEE_DESIGNATION="+receivedObj.id+" order by ID_EMPLOYEE_REGISTRY desc";
//         }else if('DEPARTMENT' == receivedObj.idType){
//             queryGetEmpProfileDetails = "select * from view_all_emp_profile_details where IS_ACTIVE=1 and ID_DEPARTMENT="+receivedObj.id+" order by ID_EMPLOYEE_REGISTRY desc";
//         }else if('GRADE' == receivedObj.idType){
//             queryGetEmpProfileDetails = "select * from view_all_emp_profile_details where IS_ACTIVE=1 and ID_EMPLOYEE_GRADE="+receivedObj.id+" order by ID_EMPLOYEE_REGISTRY desc";
//         }
//         const [resultEmployeeProfileDetailsCommonId, fields] = await conn.query(queryGetEmpProfileDetails);

//         await conn.query('COMMIT');
//         conn.release();
//         logger.info('GOT EMPLOYEE PROFILE DETAILS WHERE SELECTED COMMON ID SUCCESSFULLY...');
//         return res.json({ success: true, resultEmployeeProfileDetailsCommonId });

//     } catch (err) {
//         await conn.query('ROLLBACK');
//         conn.release();
//         logger.error('CANNOT GET PROFILE DETAILS WHERE SELECTED COMMON ID DUE TO :- ', err);
//         return res.json({ success: false });
//     }

// });


router.post('/getEmployeeProfileDetailsCommonId', async function (req, res, next) {
    const API_NAME = 'getEmployeeProfileDetailsCommonId post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` SELECT * FROM hrm_employee_profile WHERE IS_ACTIVE = '1'   ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

// get all employee employments
router.get('/getMaritalStatus', async function (req, res) {

    const API_NAME = 'getMaritalStatus GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        // get employee employments
        const [maritalStatusList, fields] = await conn.query('select * from hrm_defaults_marital_status where IS_ACTIVE=1');

        conn.release();
        logger.info('GOT MARITAL STATUS SUCCESSFULLY...');
        return res.json({ success: true, maritalStatusList });

    } catch (err) {
        conn.release();
        logger.error('CANNOT GET MARITAL STATUS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// get all employee nationality
router.get('/getNationality', async function (req, res) {

    const API_NAME = 'getNationality GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const [resultNationality, fields] = await conn.query('select * from hrm_defaults_natinolity where IS_ACTIVE=1 and STATUS=1');

        conn.release();
        logger.info('GOT NATIONALITY SUCCESSFULLY...');
        return res.json({ success: true, resultNationality });

    } catch (err) {
        conn.release();
        logger.error('CANNOT NATIONALITY DUE TO :- ', err);
        return res.json({ success: false });
    }
});

//get employee profile details from the view
router.post('/getEmployeeProfileDetailsFromView', async function (req, res, next) {
    const API_NAME = 'getEmployeeProfileDetailsFromView post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` select * from view_all_emp_profile_details where IS_ACTIVE=1 and IS_RESIGNED=0  ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        var val = values[0][0];

        if (val.length > 0 && val && null != val && undefined != val) {

            // get emgency contact details where employee registry ID
            var getEmergenContact = "select * from hrm_emergency_contact where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultEmgenContact, fields1] = await conn.query(getEmergenContact);

            // get benifited person details where employee registry ID
            var getBenifitedPerson = "select * from hrm_insurance_benifitted where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultBenifitedPerson, fields2] = await conn.query(getBenifitedPerson);

            // get dependancy person details where employee registry ID
            var getDependancy = "select * from hrm_employee_dependence where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultDependancy, fields3] = await conn.query(getDependancy);

            // get education qulifications where employee registry ID
            var getEduQul = "select * from hrm_education_qualification where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultEduQul, fields4] = await conn.query(getEduQul);

            // get education subjects where education qulification id
            var resultEduSubObjAr = [];
            for (var i = 0; i < resultEduQul.length; i++) {
                var getSubjects = "select * from hrm_education_qualification_subjects where IS_ACTIVE=1 and ID_EDUCATION_QUALIFICATION=" + resultEduQul[i].ID_EDUCATION_QUALIFICATION + "";
                const [resultEduSubjects, fields5] = await conn.query(getSubjects);
                resultEduSubObjAr.push(resultEduSubjects);
            }

            // get higher education where employee registry ID
            var getHighEduQul = "select * from hrm_higher_education_qualification where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultHighEduQul, fields6] = await conn.query(getHighEduQul);

            // get profffessinol details where employee registry ID
            var getProffessinol = "select * from hrm_proffessional_qualification where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultProffessinol, fields7] = await conn.query(getProffessinol);

            // get document details where employee registry ID
            var getDocument = "select * from hrm_employee_documents where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=" + val[0].ID_EMPLOYEE_REGISTRY + "";
            const [resultDocument, fields8] = await conn.query(getDocument);

            var dataObj = {
                data: values[0][0],
                emergencyContactData: resultEmgenContact,
                benifitedPersonData: resultBenifitedPerson,
                dependancyData: resultDependancy,
                eduQulData: resultEduQul,
                eduSubjectData: resultEduSubObjAr,
                higherEduQulData: resultHighEduQul,
                proffessinolQulData: resultProffessinol,
                documentData: resultDocument,
            }

            conn.release();
            return res.status(200).send({ success: true, data: dataObj });

        } else {
            conn.release();
            return res.status(200).send({ success: false, message: 'NO DATA FOUND' });
        }
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

// get all employee profile by registry id
router.post('/getEmployeeProfileByRegistryId', async function (req, res) {

    const API_NAME = 'getEmployeeProfileByRegistryId POST, ';
    logger.info(API_NAME + ' called');
    if (!req.body.regId) {
        return res.status(200).send({ success: false, message: 'params not found' });
    }

    var conn;

    try {

        conn = await pool.getConnection();
        // get employee profile
        // const [employeeProfile, fields] = await conn.query('select * from hrm_employee_profile where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=?', req.body.regId);
        const [employeeProfile, fields] = await conn.query('select * from view_all_emp_profile_details where IS_ACTIVE=1 and ID_EMPLOYEE_REGISTRY=?', req.body.regId);

        conn.release();
        logger.info('GOT EMPLOYEE PROFILE SUCCESSFULLY...');
        return res.status(200).send({ success: true, employeeProfile });

    } catch (err) {
        if (conn !== undefined) {
            conn.release();
        }
        logger.error('CANNOT GET MARITAL STATUS DUE TO :- ', err);
        return res.status(200).send({ success: false });
    }

});

// get all employee profiles with registry id
router.get('/getEmployeeProfilesWithRegistryId', async function (req, res) {

    const API_NAME = 'getEmployeeProfilesWithRegistryId GET, ';
    logger.info(API_NAME + ' called');

    var conn;
    try {
        conn = await pool.getConnection();

        // get employee profile
        const [employees, fields] = await conn.query(
            `
            SELECT 
                ep.ID_EMPLOYEE_REGISTRY,
                CONCAT('[',
                        er.REGISTRY_CODE_PREFIX,
                        er.ID_EMPLOYEE_REGISTRY,
                        er.REGISTRY_CODE_SUFFIX,
                        '] ',
                        ep.DISPLAY_NAME) AS NAME
            FROM
                hrm_employee_profile ep
                    INNER JOIN
                hrm_employee_registry er ON ep.ID_EMPLOYEE_REGISTRY = er.ID_EMPLOYEE_REGISTRY
            WHERE
                er.IS_ACTIVE = 1 AND ep.IS_ACTIVE = 1
            `);

        conn.release();
        logger.info('GOT EMPLOYEE REG LIST SUCCESSFULLY...');
        return res.status(200).send({ success: true, employees });

    } catch (err) {
        if (conn !== undefined) {
            conn.release();
        }
        logger.error('CANNOT GET EMPLOYEE REG LIST DUE TO :- ', err);
        return res.status(200).send({ success: false });
    }

});

// get employee document details
router.get('/getEmployeeDocumentTyps', async function (req, res) {

    const API_NAME = 'getEmployeeDocumentTyps GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();

        const [documentTypeList, fields] = await conn.query('select * from hrm_employee_document_typs where IS_ACTIVE=1 order by ID_DOCUMENT_TYPE desc');

        conn.release();
        logger.info('GOT DOCUMENT DETAILS SUCCESSFULLY...');
        return res.json({ success: true, documentTypeList });

    } catch (err) {
        if (undefined != conn) conn.release();

        logger.error('CANNOT GET DOCUMENT DETAILS DUE TO :- ', err);
        return res.json({ success: false });
    }
});

router.post('/validater', async function (req, res) {

    const API_NAME = 'validater POST, ';
    logger.info(API_NAME + ' called');

    const conn = await pool.getConnection();

    try {

        const table = req.body.table;
        const column = req.body.column;
        const value = req.body.value;

        const data = await conn.query(`select COUNT(*) AS COUNT from ${table} where IS_ACTIVE=1 AND ${column}='${value}'`);

        conn.release();

        if (data[0][0].COUNT > 0) {
            console.log(true);
            return res.json({ success: true });
        }
        else {
            console.log(false);
            return res.json({ success: false });
        }

    } catch (err) {

        conn.release();
        logger.error(err);
        return res.json({ success: false });
    }
});

router.post('/validaterEdit', async function (req, res) {

    const API_NAME = 'validater POST, ';
    logger.info(API_NAME + ' called');

    const conn = await pool.getConnection();

    try {

        const table = req.body.table;
        const column = req.body.column;
        const value = req.body.value;
        const idColumn = req.body.idColumn;
        const id = req.body.id;

        const data = await conn.query(`select COUNT(*) AS COUNT from ${table} where IS_ACTIVE=1 AND ${column}='${value}' AND ${idColumn} != ${id}`);

        conn.release();

        if (data[0][0].COUNT > 0) {
            console.log(true);
            return res.json({ success: true });
        }
        else {
            console.log(false);
            return res.json({ success: false });
        }

    } catch (err) {

        conn.release();
        logger.error(err);
        return res.json({ success: false });
    }
});

// search employee
router.post('/searchEmployee', async function (req, res, next) {
    const API_NAME = 'searchEmployee POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [employeeProfileResult, fields] = await conn.query("SELECT * FROM view_all_emp_profile_details WHERE IS_ACTIVE = 1 AND IS_RESIGNED = 0 AND (ID_EMPLOYEE_REGISTRY LIKE '%" + receivedObj.searchVal + "%' OR FIRSTNAME LIKE '%" + receivedObj.searchVal + "%' OR MIDDLENAME LIKE '%" + receivedObj.searchVal + "%' OR LASTNAME LIKE '%" + receivedObj.searchVal + "%' OR ETF_NO LIKE '%" + receivedObj.searchVal + "%' OR NIC_NO LIKE '%" + receivedObj.searchVal + "%' OR NATIONALITY LIKE '%" + receivedObj.searchVal + "%' OR GENDER LIKE '%" + receivedObj.searchVal + "%' OR DISPLAY_NAME LIKE '%" + receivedObj.searchVal + "%' OR MOBILE LIKE '%" + receivedObj.searchVal + "%' OR DESIGNATION LIKE '%" + receivedObj.searchVal + "%' OR LOCATION_NAME LIKE '%" + receivedObj.searchVal + "%' OR DEPARTMENT LIKE '%" + receivedObj.searchVal + "%' OR GRADE_NAME LIKE '%" + receivedObj.searchVal + "%' OR EMPLOYMENT_NAME LIKE '%" + receivedObj.searchVal + "%' OR GROUP_NAME LIKE '%" + receivedObj.searchVal + "%')");

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT SEARCH REMPLOYEE SUCCESSFULLY...');
        return res.status(200).send({ success: true, employeeProfileResult });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT SEARCH REMPLOYEE DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;
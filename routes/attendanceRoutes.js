var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

var moment = require('moment');

var readXlsxFile = require('read-excel-file/node');


router.get('/getTempAttendanceCount', async function (req, res, next) {
    const API_NAME = 'getTempAttendanceCount get, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = ` SELECT COUNT(ID_ATTENDANCE_TEMP) AS TOTAL FROM hrm_attendance_temp WHERE IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/getTempAttendance', async function (req, res, next) {
    const API_NAME = 'getTempAttendance post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` SELECT * FROM view_temp_attendance WHERE IS_ACTIVE = '1'   ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});





router.post('/AddAttendance', async function (req, res, next) {
    //console.log(req.body);
    const API_NAME = 'AddAttendance post ';
    //logger.info("requested data",req.body)
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

   console.log("requested data",req.body)

   const ID_EMPLOYEE_PROFILE=req.body.ID_EMPLOYEE_PROFILE
   const DATE=req.body.DATE
   const IN_TIME= req.body.IN_TIME
   const OUT_TIME=req.body.OUT_TIME
   const ID_SHIFT=req.body.ID_SHIFT
   const IS_ALLOW_LATE=req.body.IS_ALLOW_LATE
   const IS_STOP_OT=req.body.IS_STOP_OT
   const SHIFT_START_TIME=req.body.SHIFT_START_TIME
   const SHIFT_END_TIME=req.body.SHIFT_END_TIME
   const OT_START_TIME=req.body.OT_START_TIME
   const OT_END_TIME=req.body.OT_END_TIME
   const LATE_START_TIME= req.body.LATE_START_TIME

   //calculate working hours
    var  WORKING_HOURS = '';
   if(IS_ALLOW_LATE==1){
       if(SHIFT_END_TIME<OUT_TIME){
        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
       }else{
        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
       }
   }else{
       if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME>OUT_TIME){
        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
       }
       if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME<OUT_TIME){
        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
       }
       if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME>OUT_TIME){
        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
       }
       if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME<OUT_TIME){
        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
       }
   }
   console.log("Working Hours: ", WORKING_HOURS);

   //calculate late hours
   var LATE_HOURS ='';
   if(IS_ALLOW_LATE==1){
       LATE_HOURS = '00:00:00';
   }else{
       if(IN_TIME>LATE_START_TIME){
           LATE_HOURS = moment.utc(moment(IN_TIME,"HH:mm:ss").diff(moment(LATE_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
       }else{
            LATE_HOURS = '00:00:00';
       }
   }
   console.log("Late Hours", LATE_HOURS)

   //calculating OT hours
   var OT_HOURS = '';
   if(IS_STOP_OT==1){
       OT_HOURS = "00:00:00";
   }else{
       if(OT_START_TIME<OUT_TIME){
           if(OT_END_TIME>OUT_TIME){
               OT_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
           }else{
                OT_HOURS = moment.utc(moment(OT_END_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
           }
       }else{
           OT_HOURS ="00:00:00";
       }
   }

   console.log("OT hours: ", OT_HOURS);


  /*  var test = moment.utc(moment(LATE_HOURS,"HH:mm:ss").add(moment(OT_HOURS,"HH:mm:ss"))).format("HH:mm:ss");
   console.log("test: ", test); */

   var data = {
    ID_EMPLOYEE_PROFILE:ID_EMPLOYEE_PROFILE,
    DATE:DATE,
    IN_TIME:IN_TIME,
    OUT_TIME:OUT_TIME,
    WORKING_HOURS:WORKING_HOURS,
    OT_HOURS:OT_HOURS,
    LATE_HOURS:LATE_HOURS,
    ID_SHIFT:ID_SHIFT
   }


   // return res.status(200).send({ success: true, message: ' New Travel Request' });
    try {
    const conn = await pool.getConnection();
    const [announcementDetails,fields]=await conn.query('INSERT INTO hrm_attendance_temp SET ?', data);
    logger.info("SUCCESSFULLY INSERTED NEW ATTENDANCE")
   
    await conn.query('COMMIT');
    conn.release();
    return res.status(200).send({ success: true, message: ' NEW ATTENDANCE CREATED' });
    }
    catch (err) {
    logger.error(API_NAME + 'error :' + err);
    await conn.query('ROLLBACK');
    conn.release();
    return res.status(500).send({ success: false, message: 'Query Error' });
    }
    });


    
router.post('/deleteAttendance', async function (req, res, next) {
    const API_NAME = 'deleteAttendance post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();

        await conn.query('UPDATE hrm_attendance_temp SET `IS_ACTIVE`=0 where `ID_ATTENDANCE_TEMP`='+req.body.ID);
        logger.info("ATTENDANCE SUCCESSFULLY DELETED")

        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Attendance Deleted' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



router.post('/updateAttendance', async function (req, res, next) {
    const API_NAME = 'updateAttendance post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    console.log(req.body.data)
    console.log(req.body.ID)

    const ID_EMPLOYEE_PROFILE=req.body.data.ID_EMPLOYEE_PROFILE
    const DATE=req.body.data.DATE
    const IN_TIME= req.body.data.IN_TIME
    const OUT_TIME=req.body.data.OUT_TIME
    const ID_SHIFT=req.body.data.ID_SHIFT
    const IS_ALLOW_LATE=req.body.data.IS_ALLOW_LATE
    const IS_STOP_OT=req.body.data.IS_STOP_OT
    const SHIFT_START_TIME=req.body.data.SHIFT_START_TIME
    const SHIFT_END_TIME=req.body.data.SHIFT_END_TIME
    const OT_START_TIME=req.body.data.OT_START_TIME
    const OT_END_TIME=req.body.data.OT_END_TIME
    const LATE_START_TIME= req.body.data.LATE_START_TIME
 
    //calculate working hours
     var  WORKING_HOURS = '';
    if(IS_ALLOW_LATE==1){
        if(SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }else{
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }
    }else{
        if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME>OUT_TIME){
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME>OUT_TIME){
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
    }
    console.log("Working Hours: ", WORKING_HOURS);
 
    //calculate late hours
    var LATE_HOURS ='';
    if(IS_ALLOW_LATE==1){
        LATE_HOURS = '00:00:00';
    }else{
        if(IN_TIME>LATE_START_TIME){
            LATE_HOURS = moment.utc(moment(IN_TIME,"HH:mm:ss").diff(moment(LATE_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }else{
             LATE_HOURS = '00:00:00';
        }
    }
    console.log("Late Hours", LATE_HOURS)
 
    //calculating OT hours
    var OT_HOURS = '';
    if(IS_STOP_OT==1){
        OT_HOURS = "00:00:00";
    }else{
        if(OT_START_TIME<OUT_TIME){
            if(OT_END_TIME>OUT_TIME){
                OT_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
            }else{
                 OT_HOURS = moment.utc(moment(OT_END_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
            }
        }else{
            OT_HOURS ="00:00:00";
        }
    }
 
    console.log("OT hours: ", OT_HOURS);

    var data = {
        ID_EMPLOYEE_PROFILE:ID_EMPLOYEE_PROFILE,
        DATE:DATE,
        IN_TIME:IN_TIME,
        OUT_TIME:OUT_TIME,
        WORKING_HOURS:WORKING_HOURS,
        OT_HOURS:OT_HOURS,
        LATE_HOURS:LATE_HOURS,
        ID_SHIFT:ID_SHIFT
       }

    try {
        const conn = await pool.getConnection();
       await conn.query('UPDATE hrm_attendance_temp SET ? where `ID_ATTENDANCE_TEMP`='+req.body.ID, data);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Attendence Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/AddAttendancePermenant', async function (req, res, next) {
    //console.log(req.body);
    const API_NAME = 'AddAttendancePermenant post ';
    //logger.info("requested data",req.body)
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

   //console.log("requested data",req.body)

   // return res.status(200).send({ success: true, message: ' New Travel Request' });
    try {
    const conn = await pool.getConnection();
    const [announcementDetails,fields]=await conn.query('INSERT INTO hrm_attendance_synced (ID_EMPLOYEE_PROFILE,ID_SHIFT,DATE,IN_TIME,OUT_TIME,IS_ALLOW_LATE,IS_STOP_OT,WORKING_HOURS,OT_HOURS,LATE_HOURS) SELECT ID_EMPLOYEE_PROFILE,ID_SHIFT,DATE,IN_TIME,OUT_TIME,IS_ALLOW_LATE,IS_STOP_OT,WORKING_HOURS,OT_HOURS,LATE_HOURS FROM hrm_attendance_temp WHERE IS_ACTIVE="1"');
    
    logger.info("SUCCESSFULLY SAVED ATTENDENCE")
   
    await conn.query('UPDATE hrm_attendance_temp SET `IS_ACTIVE`=0');

    logger.info("SUCCESSFULLY UPDATED TEMPORARY ATTEMDANCE");

    await conn.query('COMMIT');
    conn.release();
    return res.status(200).send({ success: true, message: ' NEW ATTENDANCE CREATED' });
    }
    catch (err) {
    logger.error(API_NAME + 'error :' + err);
    await conn.query('ROLLBACK');
    conn.release();
    return res.status(500).send({ success: false, message: 'Query Error' });
    }
    });



    router.get('/getShifts', async function (req, res) {

        const API_NAME = 'getShifts GET, ';
        logger.info(API_NAME + ' called');
    
        try {
            const conn = await pool.getConnection();
            await conn.query('START TRANSACTION')
    
            // get employee designations
            const [resultShifts, fields] = await conn.query('SELECT * FROM hrm_shift where IS_ACTIVE=1');
    
            await conn.query('COMMIT');
            conn.release();
            logger.info('GOT SHIFTS SUCCESSFULLY...');
            return res.json({ success: true, resultShifts });
    
        } catch (err) {
            await conn.query('ROLLBACK');
            conn.release();
            logger.error('CANNOT GET SHIFTS DUE TO :- ', err);
            return res.json({ success: false });
        }
    
    });



    router.post('/getSyncAttendance', async function (req, res, next) {
        const API_NAME = 'getSyncAttendance post, ';
    
        logger.debug(JSON.stringify(req.body));
    
        //*********always log inside which api*********
        logger.info(API_NAME + 'called');
    
        let sqlFilter = ``;
    
        //*********always check req data before accessing them directly*********
    
        //logic here
        try {
            const conn = await pool.getConnection();
    
            let filtersArray = [];
            let filterColumnsArray = [];
            if (req.body.filters) {
                for (var key in req.body.filters) {
                    if (req.body.filters.hasOwnProperty(key)) {
                        sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                        filtersArray.push(req.body.filters[key]);
                    }
                }
            }
    
            if (req.body.sortOrder && req.body.sortField) {
                if (req.body.sortOrder == 'ASC') {
                    sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
                }
                if (req.body.sortOrder == 'DESC') {
                    sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                    filterColumnsArray.push(req.body.sortField);
                }
            }
            if (req.body.results) {
                sqlFilter = sqlFilter + ` LIMIT ?`;
                filtersArray.push(req.body.results)
            }
            if (req.body.page) {
                sqlFilter = sqlFilter + ` OFFSET ?`;
                filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
            }
    
            logger.debug(sqlFilter);
            logger.debug('Filter array ' + JSON.stringify(filtersArray));
            // Do something with the connection
            var sql = ` SELECT * FROM view_sync_attendance WHERE IS_ACTIVE = '1'   ` + sqlFilter;
            logger.debug(sql);
    
            var promise1 = conn.execute(sql
                , [...filtersArray]);
    
            //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');
    
            const values = await Promise.all([promise1]);
            logger.debug(promise1.sql);
            conn.release();
    
            return res.status(200).send({ success: true, data: values[0][0] });
        }
    
        catch (err) {
            logger.error(API_NAME + 'error :' + err);
            logger.error('Error ' + JSON.stringify(err));
            return res.status(500).send({ success: false, message: 'Query Error' });
        }
    });


    router.get('/getSyncAttendanceCount', async function (req, res, next) {
        const API_NAME = 'getSyncAttendanceCount get, ';
    
        logger.debug(JSON.stringify(req.body));
    
        //*********always log inside which api*********
        logger.info(API_NAME + 'called');
    
        try {
            const conn = await pool.getConnection();
            
            // Do something with the connection
            var sql = ` SELECT COUNT(ID_ATTENDANCE_SYNCED) AS TOTAL FROM hrm_attendance_synced WHERE IS_ACTIVE = '1'`;
            logger.debug(sql);
    
            var promise1 = conn.execute(sql);
    
            const values = await Promise.all([promise1]);
            logger.debug(promise1.sql);
            conn.release();
    
            return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
        }
    
        catch (err) {
            logger.error(API_NAME + 'error :' + err);
            logger.error('Error ' + JSON.stringify(err));
            return res.status(500).send({ success: false, message: 'Query Error' });
        }
    });


    router.post('/getRosterDetails', async function (req, res) {

        const API_NAME = 'getRosterDetails GET, ';
        logger.info(API_NAME + ' called');
    
        var receivedObj = req.body;

        console.log("request body roster",receivedObj);
    
        try {
            const conn = await pool.getConnection();
            await conn.query('START TRANSACTION');
    
            // get employee registry details
            var queryGetEmpRosterDetails = ''
           
            queryGetEmpRosterDetails = "SELECT r.*,s.SHIFT_CODE FROM hrm_roster_details r LEFT JOIN hrm_shift s ON r.ID_SHIFT=s.ID_SHIFT WHERE r.IS_ACTIVE = '1' AND SUBSTRING(DAY,1,10) = '"+receivedObj.DAY+"' AND ID_EMPLOYEE_PROFILE= "+receivedObj.ID_EMPLOYEE_PROFILE;
           
            console.log("query for roster details",queryGetEmpRosterDetails);

            const [resultRosterDetails, fields] = await conn.query(queryGetEmpRosterDetails);
    
            await conn.query('COMMIT');
            conn.release();
            logger.info('GOT ROSTER DETAILS SUCCESSFULLY...');
            return res.json({ success: true, resultRosterDetails });
    
        } catch (err) {
            await conn.query('ROLLBACK');
            conn.release();
            logger.error('CANNOT GET ROSTER DETAILS DUE TO :- ', err);
            return res.json({ success: false });
        }
    
    });


    router.post('/approveAttendance', async function (req, res, next) {
        const API_NAME = 'approveAttendance post ';
    
        //*********always log inside which api*********
        logger.info(API_NAME + 'called');
        console.log(req.body.ID)
    
        try {
            const conn = await pool.getConnection();
           await conn.query('UPDATE hrm_attendance_synced SET `STATUS`=1 where `ID_ATTENDANCE_SYNCED`='+req.body.ID);
            conn.release();
    
            logger.debug(API_NAME + 'success');
            return res.status(200).send({ success: true, message: 'Attendence Approved' });
        }
    
        catch (err) {
            logger.error(API_NAME + 'error :' + err);
            return res.status(500).send({ success: false, message: 'Query Error' });
        }
    });



    
router.post('/updateSyncedAttendance', async function (req, res, next) {
    const API_NAME = 'updateSyncedAttendance post ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    console.log(req.body.data)
    console.log(req.body.ID)

    const ID_EMPLOYEE_PROFILE=req.body.data.ID_EMPLOYEE_PROFILE
    const DATE=req.body.data.DATE
    const IN_TIME= req.body.data.IN_TIME
    const OUT_TIME=req.body.data.OUT_TIME
    const ID_SHIFT=req.body.data.ID_SHIFT
    const IS_ALLOW_LATE=req.body.data.IS_ALLOW_LATE
    const IS_STOP_OT=req.body.data.IS_STOP_OT
    const SHIFT_START_TIME=req.body.data.SHIFT_START_TIME
    const SHIFT_END_TIME=req.body.data.SHIFT_END_TIME
    const OT_START_TIME=req.body.data.OT_START_TIME
    const OT_END_TIME=req.body.data.OT_END_TIME
    const LATE_START_TIME= req.body.data.LATE_START_TIME
 
    //calculate working hours
     var  WORKING_HOURS = '';
    if(IS_ALLOW_LATE==1){
        if(SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }else{
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }
    }else{
        if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME>OUT_TIME){
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME>OUT_TIME){
         WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
        if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME<OUT_TIME){
         WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
        }
    }
    console.log("Working Hours: ", WORKING_HOURS);
 
    //calculate late hours
    var LATE_HOURS ='';
    if(IS_ALLOW_LATE==1){
        LATE_HOURS = '00:00:00';
    }else{
        if(IN_TIME>LATE_START_TIME){
            LATE_HOURS = moment.utc(moment(IN_TIME,"HH:mm:ss").diff(moment(LATE_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
        }else{
             LATE_HOURS = '00:00:00';
        }
    }
    console.log("Late Hours", LATE_HOURS)
 
    //calculating OT hours
    var OT_HOURS = '';
    if(IS_STOP_OT==1){
        OT_HOURS = "00:00:00";
    }else{
        if(OT_START_TIME<OUT_TIME){
            if(OT_END_TIME>OUT_TIME){
                OT_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
            }else{
                 OT_HOURS = moment.utc(moment(OT_END_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
            }
        }else{
            OT_HOURS ="00:00:00";
        }
    }
 
    console.log("OT hours: ", OT_HOURS);

    var data = {
        ID_EMPLOYEE_PROFILE:ID_EMPLOYEE_PROFILE,
        DATE:DATE,
        IN_TIME:IN_TIME,
        OUT_TIME:OUT_TIME,
        WORKING_HOURS:WORKING_HOURS,
        OT_HOURS:OT_HOURS,
        LATE_HOURS:LATE_HOURS,
        ID_SHIFT:ID_SHIFT
       }

    try {
        const conn = await pool.getConnection();
       await conn.query('UPDATE hrm_attendance_synced SET ? where `ID_ATTENDANCE_SYNCED`='+req.body.ID, data);
        conn.release();

        logger.debug(API_NAME + 'success');
        return res.status(200).send({ success: true, message: 'Attendence Updated' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.post('/getLateAttendance', async function (req, res, next) {
    const API_NAME = 'getLateAttendance post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = `SELECT * FROM view_late_attendance_with_detail WHERE IS_ACTIVE = '1' ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.get('/getLateAttendanceCount', async function (req, res, next) {
    const API_NAME = 'getLateAttendanceCount get, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = ` SELECT COUNT(*) AS TOTAL FROM view_late_attendance_with_detail WHERE IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/getLateAttendanceMoreDetails', async function (req, res) {

    const API_NAME = 'getLateAttendanceMoreDetails POST, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log("request body roster",receivedObj);

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get employee registry details
        var queryGetmoreDetailsLateAttendance = ''
       
        queryGetmoreDetailsLateAttendance =`SELECT 
                                ats.*,
                                rd.START_TIME,
                                rd.END_TIME,
                                s.SHIFT_CODE
                            FROM 
                                hrm_attendance_synced ats
                            left join 
                                hrm_roster_details rd on ats.ID_EMPLOYEE_PROFILE=rd.ID_EMPLOYEE_PROFILE and ats.DATE=substring(rd.DAY,1,10)
                            left join
                                hrm_shift s on ats.ID_SHIFT=s.ID_SHIFT
                            where 
                                ats.ID_EMPLOYEE_PROFILE=`+receivedObj.ID_EMPLOYEE_PROFILE +` and substring(ats.DATE,1,7)='`+receivedObj.DATE +`';`
       
        console.log("query for more details late attendance",queryGetmoreDetailsLateAttendance);

        const [resultMoreDetailsLateAttendance, fields] = await conn.query(queryGetmoreDetailsLateAttendance);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT MORE DETAILS LATE ATTENDANCE SUCCESSFULLY...');
        return res.json({ success: true, resultMoreDetailsLateAttendance });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET MORE DETAILS LATE ATTENDANCE DUE TO :- ', err);
        return res.json({ success: false });
    }

});


router.post('/getOT', async function (req, res, next) {
    const API_NAME = 'getOT post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = `SELECT * FROM view_ot_with_details WHERE IS_ACTIVE = '1' ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



router.post('/getOTMoreDetails', async function (req, res) {

    const API_NAME = 'getOTMoreDetails POST, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log("recieved obj",receivedObj);

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get employee registry details
        var queryGetmoreDetailsOT = ''
       
        queryGetmoreDetailsOT =`SELECT * FROM view_ot_amount WHERE ID_EMPLOYEE_PROFILE = ` +receivedObj.ID_EMPLOYEE_PROFILE +` AND substring(DATE,1,7)='`+receivedObj.DATE +`';`
       
        console.log("query for more details ot",queryGetmoreDetailsOT);

        const [resultMoreDetailsOT, fields] = await conn.query(queryGetmoreDetailsOT);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT MORE DETAILS OT SUCCESSFULLY...');
        return res.json({ success: true, resultMoreDetailsOT });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET MORE DETAILS OT DUE TO :- ', err);
        return res.json({ success: false });
    }

});


router.post('/getNoPay', async function (req, res, next) {
    const API_NAME = 'getNoPay post ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;


    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = `SELECT * FROM view_monthly_no_pay WHERE IS_ACTIVE = '1' ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



router.post('/getNoPayMoreDetails', async function (req, res) {

    const API_NAME = 'getOTMoreDetails POST, ';
    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log("recieved obj",receivedObj);

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // get employee registry details
        var queryGetmoreDetailsOT = ''
       
        queryGetmoreDetailsNoPay =`SELECT * FROM view_no_pay_full_details WHERE ID_EMPLOYEE_PROFILE = ` +receivedObj.ID_EMPLOYEE_PROFILE +` AND substring(DATE,1,7)='`+receivedObj.DATE +`';`
       
        console.log("query for more details ot",queryGetmoreDetailsNoPay);

        const [resultMoreDetailsNOPay, fields] = await conn.query(queryGetmoreDetailsNoPay);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT MORE DETAILS OT SUCCESSFULLY...');
        return res.json({ success: true, resultMoreDetailsNOPay });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET MORE DETAILS OT DUE TO :- ', err);
        return res.json({ success: false });
    }

});

router.post('/attachAttendance', async function (req, res, next) {
    const API_NAME = 'attachAttendance POST, ';

    logger.debug(JSON.stringify(req.body));
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    console.log("url",req.body)

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const schema = {
            'UID': {
                prop: 'EMPLOYEE_PROFILE_ID',
                type: Number
              },
            'AttDate': {
                prop: 'DATE',
                type: String
              },
            'IN': {
                prop: 'IN',
                type: String
              },
            'OUT': {
                prop: 'OUT',
                type: String
              }
        }

       /*  exceltojson({
            input: "http://192.169.143.105:3006/public/documents/1542362554208.xlsx",
           // output: "if you want output to be stored in a file",
            //sheet: "sheetname",  // specific sheetname inside excel file (if you have multiple sheets)
            lowerCaseHeaders:true //to convert all excel headers to lowr case in json
          }, function(err, result) {
            if(err) {
              console.error(err);
            } else {
              console.log(result);
              //result will contain the overted json data
            }
          }); */

        
        readXlsxFile((req.body.URL), {schema}).then(async ({rows,errors}) => {
                for(let i=0; i<rows.length;i++){
                    var ID_EMPLOYEE_PROFILE=rows[i].EMPLOYEE_PROFILE_ID
                    var DATE=moment(rows[i].DATE).format('YYYY-MM-DD')
                    var IN_TIME= moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")
                    var OUT_TIME=moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")
                    var ID_SHIFT=''
                    var IS_ALLOW_LATE=0
                    var IS_STOP_OT=0
                    var SHIFT_START_TIME=''
                    var SHIFT_END_TIME=''
                    var OT_START_TIME=''
                    var OT_END_TIME=''
                    var LATE_START_TIME=''
                    var rosterDetails='';
                    rosterDetails =`SELECT * FROM hrm_roster_details WHERE ID_EMPLOYEE_PROFILE = ` +rows[i].EMPLOYEE_PROFILE_ID +` AND substring(DAY,1,10)='`+moment(rows[i].DATE).format('YYYY-MM-DD')+`';`
                    const [resultrosterDetails] = await conn.query(rosterDetails);
                     console.log("query : ",rosterDetails)
                    console.log("result :",resultrosterDetails);
                    console.log("result length:",resultrosterDetails.length);
                    console.log("In time",moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss"))
                    console.log("Out time",moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")) 
                    if(resultrosterDetails.length==1){
                        //console.log("length 1 shifts: ",resultrosterDetails[0]);
                        ID_SHIFT =resultrosterDetails[0].ID_SHIFT
                        SHIFT_START_TIME = resultrosterDetails[0].START_TIME
                        SHIFT_END_TIME = resultrosterDetails[0].END_TIME
                        OT_START_TIME = resultrosterDetails[0].OT_START
                        OT_END_TIME = resultrosterDetails[0].OT_END
                        LATE_START_TIME = resultrosterDetails[0].LATE_START
                   }else if(resultrosterDetails.length>1){
                    for(let j=0;j<resultrosterDetails.length;j++){
                        if(moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")<resultrosterDetails[j].START_TIME && moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")>resultrosterDetails[j].END_TIME){
                            ID_SHIFT =resultrosterDetails[j].ID_SHIFT
                            SHIFT_START_TIME = resultrosterDetails[j].START_TIME
                            SHIFT_END_TIME = resultrosterDetails[j].END_TIME
                            OT_START_TIME = resultrosterDetails[j].OT_START
                            OT_END_TIME = resultrosterDetails[j].OT_END
                            LATE_START_TIME = resultrosterDetails[j].LATE_START
                        }else if(moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")>resultrosterDetails[j].START_TIME && moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")<resultrosterDetails[j].END_TIME && moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")>resultrosterDetails[j].END_TIME){
                            ID_SHIFT =resultrosterDetails[j].ID_SHIFT
                            SHIFT_START_TIME = resultrosterDetails[j].START_TIME
                            SHIFT_END_TIME = resultrosterDetails[j].END_TIME
                            OT_START_TIME = resultrosterDetails[j].OT_START
                            OT_END_TIME = resultrosterDetails[j].OT_END
                            LATE_START_TIME = resultrosterDetails[j].LATE_START
                        }else if(moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")<resultrosterDetails[j].START_TIME && moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")>resultrosterDetails[j].START_TIME &&  moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")<resultrosterDetails[j].END_TIME){
                            ID_SHIFT =resultrosterDetails[j].ID_SHIFT
                            SHIFT_START_TIME = resultrosterDetails[j].START_TIME
                            SHIFT_END_TIME = resultrosterDetails[j].END_TIME
                            OT_START_TIME = resultrosterDetails[j].OT_START
                            OT_END_TIME = resultrosterDetails[j].OT_END
                            LATE_START_TIME = resultrosterDetails[j].LATE_START
                        }else if(moment(rows[i].IN,"HH:mm:ss").format("HH:mm:ss")>resultrosterDetails[j].START_TIME && moment(rows[i].OUT,"HH:mm:ss").format("HH:mm:ss")<resultrosterDetails[j].END_TIME){
                            ID_SHIFT =resultrosterDetails[j].ID_SHIFT
                            SHIFT_START_TIME = resultrosterDetails[j].START_TIME
                            SHIFT_END_TIME = resultrosterDetails[j].END_TIME
                            OT_START_TIME = resultrosterDetails[j].OT_START
                            OT_END_TIME = resultrosterDetails[j].OT_END
                            LATE_START_TIME = resultrosterDetails[j].LATE_START
                        }
                    }
                   }else{
                       console.log("No Roster Details")
                   }
                   var  WORKING_HOURS = '';
                   if(IS_ALLOW_LATE==1){
                       if(SHIFT_END_TIME<OUT_TIME){
                        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
                       }else{
                        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
                       }
                   }else{
                       if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME>OUT_TIME){
                        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
                       }
                       if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME<OUT_TIME){
                        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
                       }
                       if(SHIFT_START_TIME>IN_TIME && SHIFT_END_TIME>OUT_TIME){
                        WORKING_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(SHIFT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
                       }
                       if(SHIFT_START_TIME<IN_TIME && SHIFT_END_TIME<OUT_TIME){
                        WORKING_HOURS = moment.utc(moment(SHIFT_END_TIME,"HH:mm:ss").diff(moment(IN_TIME,"HH:mm:ss"))).format("HH:mm:ss"); 
                       }
                   }
                   console.log("Working Hours: ", WORKING_HOURS);
                
                   //calculate late hours
                   var LATE_HOURS ='';
                   if(IS_ALLOW_LATE==1){
                       LATE_HOURS = '00:00:00';
                   }else{
                       if(IN_TIME>LATE_START_TIME){
                           LATE_HOURS = moment.utc(moment(IN_TIME,"HH:mm:ss").diff(moment(LATE_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
                       }else{
                            LATE_HOURS = '00:00:00';
                       }
                   }
                   console.log("Late Hours", LATE_HOURS)
                
                   //calculating OT hours
                   var OT_HOURS = '';
                   if(IS_STOP_OT==1){
                       OT_HOURS = "00:00:00";
                   }else{
                       if(OT_START_TIME<OUT_TIME){
                           if(OT_END_TIME>OUT_TIME){
                               OT_HOURS = moment.utc(moment(OUT_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
                           }else{
                                OT_HOURS = moment.utc(moment(OT_END_TIME,"HH:mm:ss").diff(moment(OT_START_TIME,"HH:mm:ss"))).format("HH:mm:ss");
                           }
                       }else{
                           OT_HOURS ="00:00:00";
                       }
                   }
                
                   console.log("OT hours: ", OT_HOURS);
                   console.log("IN time:",IN_TIME);
                   console.log("OUT time:",OUT_TIME);
                   console.log("SHIFT START",SHIFT_START_TIME);
                   console.log("SHIFT END",SHIFT_START_TIME);
                   console.log("Late start",LATE_START_TIME);
                   console.log("OT START",OT_START_TIME);
                   console.log("OT END",OT_END_TIME);
                    console.log("--------------------------------------------------------------------------------------------")
                    var data = {
                        ID_EMPLOYEE_PROFILE:ID_EMPLOYEE_PROFILE,
                        DATE:DATE,
                        IN_TIME:IN_TIME,
                        OUT_TIME:OUT_TIME,
                        WORKING_HOURS:WORKING_HOURS,
                        OT_HOURS:OT_HOURS,
                        LATE_HOURS:LATE_HOURS,
                        ID_SHIFT:ID_SHIFT
                       }
                       const [temporaryAttendance]=await conn.query('INSERT INTO hrm_attendance_temp SET ?', data);
                        logger.info("SUCCESSFULLY INSERTED NEW ATTENDANCE")
                } 
          }).catch(
              console.log("Excel file cannot be read")
              //return res.json({ success: false });
          )

        await conn.query('COMMIT');
        conn.release();
        return res.status(200).send({ success: true, message: 'Success' });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


    
module.exports = router;
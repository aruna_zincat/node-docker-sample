var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


router.get('/getAssetType', async function (req, res) {

    const API_NAME = 'getAssetType GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultAssetType, fields] = await conn.query(` SELECT * FROM hrm_asset_asset_type WHERE IS_ACTIVE = '1'`);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ASSET TYPES SUCCESSFULLY...');
        return res.json({ success: true, resultAssetType });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ASSET TYPES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

router.get('/getAssetStatus', async function (req, res) {

    const API_NAME = 'getAssetStatus GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultAssetStatus, fields] = await conn.query(` SELECT * FROM hrm_asset_status WHERE IS_ACTIVE = '1'`);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ASSET STATUS SUCCESSFULLY...');
        return res.json({ success: true, resultAssetStatus });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ASSET STATUS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

router.post('/addAssetType', async function (req, res, next) {
    console.log(req.body);
    const API_NAME = 'addAssetType post ';
    
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    
    try {
    const conn = await pool.getConnection();
    await conn.query('INSERT INTO hrm_asset_asset_type SET ?', req.body);
    conn.release();
    
    return res.status(200).send({ success: true, message: 'Asset Type Added' });
    }
    
    catch (err) {
    logger.error(API_NAME + 'error :' + err);
    return res.status(500).send({ success: false, message: 'Query Error' });
    }
    });


    
router.post('/addNewAsset', async function (req, res, next) {
    console.log(req.body);
    const API_NAME = 'addNewAsset post ';
    
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');
    console.log("request",req.body);
    
    try {
    const conn = await pool.getConnection();
    await conn.query('INSERT INTO hrm_asset_register SET ?', req.body);
    conn.release();
    
    return res.status(200).send({ success: true, message: ' New Asset Added' });
    }
    
    catch (err) {
    logger.error(API_NAME + 'error :' + err);
    return res.status(500).send({ success: false, message: 'Query Error' });
    }
    });


 
router.get('/getAssetRegister', async function (req, res) {

    const API_NAME = 'getAssetRegister GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee designations
        const [resultAssetRegister, fields] = await conn.query(` SELECT * FROM hrm_asset_register WHERE IS_ACTIVE = '1'`);

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT ASSET REGISTER SUCCESSFULLY...');
        return res.json({ success: true, resultAssetRegister });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET ASSET STATUS DUE TO :- ', err);
        return res.json({ success: false });
    }

});


router.post('/getAssetDetailsFromView', async function (req, res, next) {
    const API_NAME = 'getAssetDetailsFromView post, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //*********always check req data before accessing them directly*********

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` SELECT * FROM asset_view WHERE IS_ACTIVE = '1'   ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



router.get('/getAssetCount', async function (req, res, next) {
    const API_NAME = 'getAssetCount get, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = ` SELECT COUNT(ID_ASSET_REGISTER) AS TOTAL FROM asset_view WHERE IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


module.exports = router;
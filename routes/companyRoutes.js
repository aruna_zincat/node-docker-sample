var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


// get all employee designations
router.get('/getTrades', async function (req, res) {

    const API_NAME = 'getTrades GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultTrade, fields] = await conn.query('select * from hrm_default_trade where IS_ACTIVE=1 order by ID_TRADE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TRADES SUCCESSFULLY...');
        return res.json({ success: true, resultTrade });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TRADES DUE TO :- ', err);
        return res.json({ success: false });
    }

});


// get company details
router.get('/getCompanyDetail', async function (req, res) {

    const API_NAME = 'getCompanyDetail GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee grades
        const [resultCompany, fields] = await conn.query('select * from hrm_default_company where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT COMPANY DETAILS SUCCESSFULLY...');
        return res.json({ success: true, resultCompany });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET COMPANY DETAILS DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// create update company
router.post('/createCompany', async function (req, res, next) {
    const API_NAME = 'createCompany POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log(receivedObj);

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // update old company details row
        const [resultUpdateCompany, fields] = await conn.query("update hrm_default_company set IS_ACTIVE=0");
        logger.info('Successfully Updated Company detail record ');

        // set data to insert 
        var company = {
            ID_TRADE: receivedObj.tradeClassification,
            // ID_TRADE: 1,
            NAME: receivedObj.companyName,
            COMPANY_LOGO_URL: receivedObj.imgPath,
            NUMBER: receivedObj.companyNo,
            REGISTATION_NO: receivedObj.registationNo,
            ETF_EPF_REGISTATION_NO: receivedObj.etfEpfRegistationNo,
            TAX_REGISTATION_NO: receivedObj.taxRegistationNo,
            COUNTRY: receivedObj.country,
            FAX_NO: receivedObj.fax,
            TELEPHONE1: receivedObj.telephone1,
            TELEPHONE2: receivedObj.telephone2,
            WEB_SITE: receivedObj.webSite,
            FACEBOOK: receivedObj.facebook,
            INSTERGRAM: receivedObj.instargram,
            TWITER: receivedObj.twitter,
            LINKEDIN: receivedObj.linkedin,
            PHYSICAL_COMPANY_NAME: receivedObj.physicalCompanyName,
            PHYSICAL_STREET_NO:receivedObj.physicalStreetName,
            PHYSICAL_DISTRICT:receivedObj.physicalDistrict,
            PHYSICAL_CITY:receivedObj.physcialCity,
            PHYSICAL_POSTAL_CODE:receivedObj.physicalPostalCode,
            PHISICAL_COUNTRY:receivedObj.physicalCountry,
            POSTAL_ADDRESS1:receivedObj.address1,
            POSTAL_ADDRESS2:receivedObj.address2,
            POSTAL_ADDRESS3:receivedObj.address3,
            POSTAL_CODE:receivedObj.postalCodePostal,
            BASE_CURRENCY:receivedObj.baseCurrency,
            COMPANY_EMAIL: receivedObj.email,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };

        // insert data into leave request
        const [resultCompanyCreate, fields1] = await conn.query('INSERT INTO hrm_default_company SET ?', company);
        logger.info('Successfully saved Company record id = ' + resultCompanyCreate.insertId);


        await conn.query('COMMIT');
        conn.release();
        logger.info('COMPANY CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('COMPANY CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;
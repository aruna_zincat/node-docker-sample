var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

// get all groups
router.get('/getGroups', async function (req, res) {

    const API_NAME = 'getGroups GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        const [resultData, fields] = await conn.query('select * from hrm_employee_group where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT GROUP SUCCESSFULLY...');
        return res.json({ success: true, resultData });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GROUP DUE TO :- ', err);
        return res.json({ success: false });
    }

});

module.exports = router;
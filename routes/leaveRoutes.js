var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

var sendEmail = require('../utils/eMailUtil');

const rgbHex = require('rgb-hex');

// get all employee leave types
router.get('/getLeaveTypes', async function (req, res) {

    const API_NAME = 'getLeaveTypes GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee leave types
        const [resultLeaveTypes, fields] = await conn.query('select * from hrm_leave_type where IS_ACTIVE=1 order by ID_LEAVE_TYPE desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT LEAVE TYPES SUCCESSFULLY...');
        return res.json({ success: true, resultLeaveTypes });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET LEAVE TYPES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

//get leave reasons
router.get('/getLeaveReasons', async function (req, res) {

    const API_NAME = 'getLeaveReasons GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee leave types
        const [resultLeaveReasons, fields] = await conn.query('select * from hrm_leave_reason where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT LEAVE REASONS SUCCESSFULLY...');
        return res.json({ success: true, resultLeaveReasons });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET LEAVE REASONS DUE TO :- ', err);
        return res.json({ success: false });
    }

});



// get employee leave list
router.post('/getEmployeeLeaveList', async function (req, res, next) {
    const API_NAME = 'getEmployeeLeaveList post, ';

    logger.debug(JSON.stringify(req.body));
    logger.info(API_NAME + 'called');

    let sqlFilter = ``;

    //logic here
    try {
        const conn = await pool.getConnection();

        let filtersArray = [];
        let filterColumnsArray = [];
        if (req.body.filters) {
            for (var key in req.body.filters) {
                if (req.body.filters.hasOwnProperty(key)) {
                    sqlFilter = sqlFilter + ` AND ` + conn.escapeId(key) + `=?`
                    filtersArray.push(req.body.filters[key]);
                }
            }
        }

        if (req.body.sortOrder && req.body.sortField) {
            if (req.body.sortOrder == 'ASC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` ASC`;
            }
            if (req.body.sortOrder == 'DESC') {
                sqlFilter = sqlFilter + ` ORDER BY ` + conn.escapeId(req.body.sortField) + ` DESC`;
                filterColumnsArray.push(req.body.sortField);
            }
        }
        if (req.body.results) {
            sqlFilter = sqlFilter + ` LIMIT ?`;
            filtersArray.push(req.body.results)
        }
        if (req.body.page) {
            sqlFilter = sqlFilter + ` OFFSET ?`;
            filtersArray.push(parseInt(req.body.page - 1) * parseInt(req.body.results))
        }

        logger.debug(sqlFilter);
        logger.debug('Filter array ' + JSON.stringify(filtersArray));
        // Do something with the connection
        var sql = ` SELECT * FROM view_leave_emp_profile where IS_ACTIVE=1  ` + sqlFilter;
        logger.debug(sql);

        var promise1 = conn.execute(sql
            , [...filtersArray]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, data: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.post('/updateEmpLeaveStatus', async function (req, res, next) {
    const API_NAME = 'updateEmpLeaveStatus POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log(receivedObj);

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // update old employee leave request row
        const [resultUpdateLeaveRequest, fields] = await conn.query("update hrm_leave_request set IS_ACTIVE=0 where ID_LEAVE_REQUEST=" + receivedObj.empVals.ID_LEAVE_REQUEST + "");
        logger.info('Successfully Updated employee leave request record ', resultUpdateLeaveRequest.affectedRows);

        var hrUpdatedStatus = '';
        var hodUpdatedStatus = '';

        var hrReason = '';
        var hodReason = '';

        // create email 
        var receivedPersonEmail = '';
        var receivedPersonName = '';
        var receivedPersonCode = '';
        var subject = '';
        var body = '';
        var footer = '';

        if ('HR' == receivedObj.loginType) {
            hrUpdatedStatus = receivedObj.updateingVals.action;
            hodUpdatedStatus = receivedObj.empVals.HOD_STATUS;

            hrReason = receivedObj.updateingVals.reasonFor;
            hodReason = receivedObj.empVals.HOD_RESON_COMMENT;

            // email configuration
            receivedPersonEmail = receivedObj.empVals.EMAIL;
            receivedPersonName = receivedObj.empVals.FIRSTNAME + ' ' + receivedObj.empVals.MIDDLENAME + ' ' + receivedObj.empVals.LASTNAME;
            receivedPersonCode = receivedObj.empVals.REGISTRY_CODE_PREFIX + receivedObj.empVals.ID_EMPLOYEE_REGISTRY + receivedObj.empVals.REGISTRY_CODE_SUFFIX;
            subject = 'This E-mail from HR department about your leave request';

            var hrStatus = '';
            if ('approved' == receivedObj.updateingVals.action) {
                hrStatus = 'Approved';
            }
            if ('rejected' == receivedObj.updateingVals.action) {
                hrStatus = 'Rejected due to ' + receivedObj.empVals.HR_RESON_COMMENT;
            }
            if ('pending' == receivedObj.updateingVals.action) {
                hrStatus = 'Pending due to ' + receivedObj.empVals.HR_RESON_COMMENT;
            }

            body = "<p>Dear " + receivedPersonName + " ,</p><br><p>The employee " + receivedPersonName + "(" + receivedPersonCode + ") requested a leave from <b>" + receivedObj.empVals.START_DATE + "-" + receivedObj.empVals.START_TIME + "</b> to <b>" + receivedObj.empVals.END_DATE + "-" + receivedObj.empVals.END_TIME + " </b>."
                + "<br> <p>Reason: " + receivedObj.empVals.REASON + " (" + receivedObj.empVals.REASON_INDETAIL + ")</p>."
                + "was " + hrStatus + "<br><p><i>This is a auto generated mail.Please do not reply.</i></p>";

        } else {
            hrUpdatedStatus = receivedObj.empVals.HR_DEP_STATUS;
            hodUpdatedStatus = receivedObj.updateingVals.action;

            hrReason = receivedObj.empVals.HR_RESON_COMMENT;
            hodReason = receivedObj.updateingVals.reasonFor;

            // email configuration
            employeeName = receivedObj.empVals.FIRSTNAME + ' ' + receivedObj.empVals.MIDDLENAME + ' ' + receivedObj.empVals.LASTNAME;
            employeeCode = receivedObj.empVals.REGISTRY_CODE_PREFIX + receivedObj.empVals.ID_EMPLOYEE_REGISTRY + receivedObj.empVals.REGISTRY_CODE_SUFFIX;

            subject = 'This E-mail from ' + receivedObj.empVals.DEPARTMENT + ' department about your leave request';

            var hodStatus = '';
            if ('approved' == receivedObj.updateingVals.action) {
                receivedPersonEmail = 'hr@zincat.net';
                receivedPersonName = 'HR Department';
                hodStatus = 'Approved';

                body = "<p>Dear " + receivedPersonName + " ,</p><br><p>The employee " + employeeName + "(" + employeeCode + ") requested a leave from <b>" + receivedObj.empVals.START_DATE + "-" + receivedObj.empVals.START_TIME + "</b> to <b>" + receivedObj.empVals.END_DATE + "-" + receivedObj.empVals.END_TIME + " </b>."
                    + "<br> <p>Reason: " + receivedObj.empVals.REASON + " (" + receivedObj.empVals.REASON_INDETAIL + ")</p>."
                    + "was " + hodStatus + "<br><p><i>Please consider this leave.</i></p>";
            }

            if ('rejected' == receivedObj.updateingVals.action) {
                receivedPersonEmail = receivedObj.empVals.EMAIL;
                receivedPersonName = employeeName;
                receivedPersonCode = employeeCode;
                hodStatus = 'Rejected due to ' + receivedObj.empVals.HOD_RESON_COMMENT;

                body = "<p>Dear " + receivedPersonName + " ,</p><br><p>The employee " + receivedPersonName + "(" + receivedPersonCode + ") requested a leave from <b>" + receivedObj.empVals.START_DATE + "-" + receivedObj.empVals.START_TIME + "</b> to <b>" + receivedObj.empVals.END_DATE + "-" + receivedObj.empVals.END_TIME + " </b>."
                    + "<br> <p>Reason: " + receivedObj.empVals.REASON + " (" + receivedObj.empVals.REASON_INDETAIL + ")</p>."
                    + "was " + hodStatus + "<br><p><i>This is a auto generated mail.Please do not reply.</i></p>";

            }

            if ('pending' == receivedObj.updateingVals.action) {
                hodStatus = 'Pending due to ' + receivedObj.empVals.HOD_RESON_COMMENT;
                receivedPersonEmail = receivedObj.empVals.EMAIL;
                receivedPersonName = employeeName;
                receivedPersonCode = employeeCode;

                body = "<p>Dear " + receivedPersonName + " ,</p><br><p>The employee " + receivedPersonName + "(" + receivedPersonCode + ") requested a leave from <b>" + receivedObj.empVals.START_DATE + "-" + receivedObj.empVals.START_TIME + "</b> to <b>" + receivedObj.empVals.END_DATE + "-" + receivedObj.empVals.END_TIME + " </b>."
                    + "<br> <p>Reason: " + receivedObj.empVals.REASON + " (" + receivedObj.empVals.REASON_INDETAIL + ")</p>."
                    + "was " + hodStatus + "<br><p><i>This is a auto generated mail.Please do not reply.</i></p>";

            }
        }

        // call send email function
        sendEmail(receivedPersonEmail, subject, body, footer);

        var isCover = 0;
        var coverEmpId = 0;
        if (null == receivedObj.updateingVals.coverEmployeeID || '' == receivedObj.updateingVals.coverEmployeeID || undefined == receivedObj.updateingVals.coverEmployeeID) {
            isCover = 0;
        } else {
            isCover = 1;
            coverEmpId = receivedObj.updateingVals.coverEmployeeID;
        }

        // set data to insert 
        var leaveRequest = {
            ID_EMPLOYEE_PROFILE: receivedObj.empVals.ID_EMPLOYEE_PROFILE,
            ID_LEAVE_REASON: receivedObj.empVals.ID_LEAVE_REASON,
            ID_LEAVE_TYPE: receivedObj.empVals.ID_LEAVE_TYPE,
            START_DATE: receivedObj.empVals.START_DATE,
            START_TIME: receivedObj.empVals.START_TIME,
            END_DATE: receivedObj.empVals.END_DATE,
            END_TIME: receivedObj.empVals.END_TIME,
            REASON_INDETAIL: receivedObj.empVals.REASON_INDETAIL,
            COVERED_BY: coverEmpId,
            HR_DEP_STATUS: hrUpdatedStatus,
            HOD_STATUS: hodUpdatedStatus,
            ID_HOD: receivedObj.empVals.ID_HOD,
            IS_COVERED_BY_ANYONE: isCover,
            HR_RESON_COMMENT: hrReason,
            HOD_RESON_COMMENT: hodReason,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };

        // insert data into leave request
        const [resultLeaveRequest, fields1] = await conn.query('INSERT INTO hrm_leave_request SET ?', leaveRequest);
        logger.info('Successfully saved leave request record id = ' + resultLeaveRequest.insertId);


        await conn.query('COMMIT');
        conn.release();
        logger.info('A NEW LEAVE REQUEST CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('A NEW EMPLOYEE CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});




router.post('/addLeaveRequest', async function (req, res, next) {
    //console.log(req.body);
    const API_NAME = 'addLeaveRequest post ';
    //logger.info("requested data",req.body)
    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    console.log("requested data", req.body)

    // return res.status(200).send({ success: true, message: ' New Travel Request' });
    try {
        const conn = await pool.getConnection();
        const [announcementDetails, fields] = await conn.query('INSERT INTO hrm_leave_request SET ?', req.body);

        const HOD = await conn.query('SELECT ID_EMPLOYEE_REGISTRY,FIRSTNAME,MIDDLENAME,LASTNAME,REGISTRY_CODE_PREFIX,REGISTRY_CODE_SUFFIX,EMAIL FROM view_all_emp_profile_details where ID_EMPLOYEE_PROFILE = ' + req.body.ID_HOD);

        const employee = await conn.query('SELECT ID_EMPLOYEE_REGISTRY,FIRSTNAME,MIDDLENAME,LASTNAME,REGISTRY_CODE_PREFIX,REGISTRY_CODE_SUFFIX FROM view_all_emp_profile_details where ID_EMPLOYEE_PROFILE = ' + req.body.ID_EMPLOYEE_PROFILE);

        //console.log("HOD Email",HODEmail[0][0].EMAIL);
        var hod_email = HOD[0][0].EMAIL;

        var hod_name = HOD[0][0].FIRSTNAME + ' ' + HOD[0][0].MIDDLENAME + ' ' + HOD[0][0].LASTNAME

        var employeeCode = employee[0][0].REGISTRY_CODE_PREFIX + employee[0][0].ID_EMPLOYEE_REGISTRY + employee[0][0].REGISTRY_CODE_SUFFIX

        var employeeName = employee[0][0].FIRSTNAME + ' ' + employee[0][0].MIDDLENAME + ' ' + employee[0][0].LASTNAME

        var subject = 'A NEW LEAVE REQUEST FROM ' + employeeCode + ' - ' + employeeName

        var body = `<p>Dear ` + hod_name + ` ,</p><br><p>The employee ` + employeeName + ' requested a leave from <b>' + req.body.START_DATE + '</b> to <b>' + req.body.START_DATE + '</b>.' + '<br> <p>Reason: ' + req.body.REASON_INDETAIL + '</p>.<br><p><i>This is a auto generated mail.Please do not reply.</i></p>'


        var footer = 'This is a auto generated mail.Please do not reply.'
        sendEmail(hod_email, subject, body, footer);

        logger.info("SUCCESSFULLY INSERTED LEAVE REQUEST")

        await conn.query('COMMIT');
        conn.release();
        return res.status(200).send({ success: true, message: ' NEW LEAVE REQUEST CREATED', data: HOD[0][0] });
    }
    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        await conn.query('ROLLBACK');

        conn.release();
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});




router.post('/getLeaveEntitlementObject', async function (req, res, next) {
    const API_NAME = 'getLeaveEntitlementObject post ';

    //*********always log inside which api*********
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const leaveEntitleMents = await conn.execute(`SELECT * FROM hrm_employee_leave_type_object WHERE IS_ACTIVE = '1' AND ID_EMPLOYEE_REGISTRY=` + req.body.ID_EMPLOYEE_REGISTRY);

            const leaveTaken = await conn.execute(`SELECT 
                                                    ID_LEAVE_TYPE, SUM(NO_OF_DAYS) AS TAKEN_LEAVES
                                                FROM
                                                    hrm_leave_request
                                                WHERE
                                                    HR_DEP_STATUS != 'rejected'
                                                    AND HOD_STATUS != 'rejected' AND ID_EMPLOYEE_PROFILE=`+ req.body.ID_EMPLOYEE_PROFILE + ` GROUP BY ID_LEAVE_TYPE`);

        const LeaveTypes = await conn.query('select * from hrm_leave_type where IS_ACTIVE=1 order by ID_LEAVE_TYPE desc');


        //console.log("leave entitlements",leaveEntitleMents[0][0])

        // console.log("leave taken",leaveTaken[0][0])
        // console.log("leave Types",LeaveTypes[0][1])

        var JSON_OBJ_LEAVE_OBJECT = JSON.parse(leaveEntitleMents[0][0].OBJECT)


        var leaveSummary = [];



        for (let i = 0; i < JSON_OBJ_LEAVE_OBJECT.LEAVES.length; i++) {
            var LEAVE_ENTITLEMENTS = '';
            var LEAVE_TAKEN = 0;
            var LEAVE_NAME = '';
            var COLOR = '';
            LEAVE_ENTITLEMENTS = JSON_OBJ_LEAVE_OBJECT.LEAVES[i]
            //console.log("leave entitlements",LEAVE_ENTITLEMENTS)
            for (let m = 0; m < leaveTaken[0].length; m++) {
                if (JSON_OBJ_LEAVE_OBJECT.LEAVES[i].ID_LEAVE_TYPE == leaveTaken[0][m].ID_LEAVE_TYPE) {
                    LEAVE_TAKEN = leaveTaken[0][m].TAKEN_LEAVES
                }
            }
            for (let n = 0; n < LeaveTypes[0].length; n++) {
                if (JSON_OBJ_LEAVE_OBJECT.LEAVES[i].ID_LEAVE_TYPE == LeaveTypes[0][n].ID_LEAVE_TYPE) {
                    LEAVE_NAME = LeaveTypes[0][n].LEAVE_TYPE_NAME
                    COLOR = rgbHex(LeaveTypes[0][n].LEAVE_TYPE_COLOR)
                }
            }
            // console.log(JSON_OBJ_LEAVE_OBJECT.LEAVES[i].ID_LEAVE_TYPE);
            //  console.log(LEAVE_TAKEN);
            // console.log(LEAVE_NAME);
            //console.log(JSON_OBJ_LEAVE_OBJECT.LEAVES[i].COUNT);

            leaveSummary.push({
                ID: JSON_OBJ_LEAVE_OBJECT.LEAVES[i].ID_LEAVE_TYPE,
                NAME: LEAVE_NAME,
                ALLOCATED_LEAVE: JSON_OBJ_LEAVE_OBJECT.LEAVES[i].COUNT,
                TAKEN: LEAVE_TAKEN,
                BALANCE: JSON_OBJ_LEAVE_OBJECT.LEAVES[i].COUNT - LEAVE_TAKEN,
                PERCENTAGE: (LEAVE_TAKEN / JSON_OBJ_LEAVE_OBJECT.LEAVES[i].COUNT) * 100,
                START: JSON_OBJ_LEAVE_OBJECT.LEAVES[i].PERIOD[0],
                END: JSON_OBJ_LEAVE_OBJECT.LEAVES[i].PERIOD[1],
                COLOR: COLOR
            })

        }


        // console.log("data",leaveSummary)



        await conn.query('COMMIT');
        conn.release();

           // console.log("Color",COLOR)
        logger.debug(API_NAME + ' success');
        return res.status(200).send({ success: true, data: leaveSummary });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



          /*  for(let i=0; i<data.length;i++){
                result.push({
                    ID_LEAVE_TYPE : data[i].ID_LEAVE_TYPE,
                    LEAVE_TYPE_NAME :data[i].LEAVE_TYPE_NAME,
                    HOD_STATUS:data[i].HOD_STATUS,
                    HR_DEP_STATUS:data[i].HR_DEP_STATUS,
                    START_DATE:data[i].START_DATE,
                    END_DATE:data[i].END_DATE,
                    COLOR:rgbHex(data[i].LEAVE_TYPE_COLOR),
                })
            } */

router.post('/getLeaveRequest', async function (req, res) {

    const API_NAME = 'getLeaveRequest POST, ';
    logger.info(API_NAME + ' called');

    var conn
    try {
        conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        // get employee leave types
        const [data, fields] = await conn.query('select lr.*,lt.LEAVE_TYPE_COLOR,lt.LEAVE_TYPE_NAME from hrm_leave_request lr left join hrm_leave_type lt on lr.ID_LEAVE_TYPE=lt.ID_LEAVE_TYPE where lr.IS_ACTIVE=1 AND lr.ID_EMPLOYEE_PROFILE= ' + req.body.ID_EMPLOYEE_PROFILE);



        //console.log("Color",COLOR)

        var result = [];


        for (let i = 0; i < data.length; i++) {
            result.push({
                ID_LEAVE_TYPE: data[i].ID_LEAVE_TYPE,
                LEAVE_TYPE_NAME: data[i].LEAVE_TYPE_NAME,
                HOD_STATUS: data[i].HOD_STATUS,
                HR_DEP_STATUS: data[i].HR_DEP_STATUS,
                START_DATE: data[i].START_DATE,
                COLOR: rgbHex(data[i].LEAVE_TYPE_COLOR),
            })
        }


        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT LEAVE REQUESTS SUCCESSFULLY...');
        return res.json({ success: true, result });

    } catch (err) {
        await conn.query('ROLLBACK');
        if (conn != undefined) {
            conn.release();
        }
        logger.error('CANNOT GET LEAVE REQUESTS DUE TO :- ', err);
        return res.json({ success: false });
    }

});





module.exports = router;
var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


// get employee settle reason
router.get('/getLeavingReasons', async function (req, res) {

    const API_NAME = 'getLeavingReasons GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        const [resultData, fields] = await conn.query('select * from hrm_employee_leaving_reason where IS_ACTIVE=1 order by ID_LEAVING_REASON desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT LEAVING REASONS SUCCESSFULLY...');
        return res.json({ success: true, resultData });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET LEAVING REASONS DUE TO :- ', err);
        return res.json({ success: false });
    }
});

// save settle employee details
router.post('/saveSettleEmployee', async function (req, res, next) {
    const API_NAME = 'saveSettleEmployee POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    console.log(receivedObj)

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // if edit, old settlment id is_active=0
        if (undefined == receivedObj.idSettlement || 0 == receivedObj.idSettlement || null == receivedObj.idSettlement) {

        } else {
            var query = "update hrm_employee_settlement set IS_ACTIVE=0 where ID_EMPLOYEE_SETTLEMENT=" + receivedObj.idSettlement + "";
            const [resultUpdateSettlment, fields11] = await conn.query(query);
            console.log(query);
            
        }

        // if already has employee, id need to be is_active=0 it
        const [resultAlreadyHasEmployee, fields9] = await conn.query("select * from hrm_employee_settlement where IS_ACTIVE=1 and ID_EMPLOYEE_PROFILE=" + receivedObj.employeeDetailsObj.profileId + "");
        if (resultAlreadyHasEmployee.length > 0) {
            const [resultUpdate, fields10] = await conn.query("UPDATE hrm_employee_settlement SET IS_ACTIVE=0 WHERE ID_EMPLOYEE_PROFILE=" + receivedObj.employeeDetailsObj.profileId + "");
        }

        // set data to insert
        var settle = {
            ID_LEAVING_REASON: receivedObj.resignationDetailObj.leavingReason,
            ID_EMPLOYEE_PROFILE: receivedObj.employeeDetailsObj.profileId,
            RESIGNATION_SUBMIT_DATE: receivedObj.resignationDetailObj.resignationSubimitDate,
            LEAVING_DATE: receivedObj.resignationDetailObj.leavingDate,
            SETTLEMENT_DATE: receivedObj.resignationDetailObj.settlementDate,
            IS_NOTICE_PERIOD_REQUIRED: receivedObj.noticPeriodDetailObj.noticRequired,
            NOTICE_PERIOD_DAYS: receivedObj.noticPeriodDetailObj.noticPeriod,
            SERVED_DAYS: receivedObj.noticPeriodDetailObj.noOfDaysServed,
            EXCESS_IN_NOTICE_DAYS: receivedObj.noticPeriodDetailObj.excessInNotice,
            LAST_WORKING_DAY: receivedObj.workingDaysDetailObj.lastWorkingDay,
            LAST_SALARY_PAID_MONTH: receivedObj.workingDaysDetailObj.lastSalaryPaidMonth,
            NO_OF_TOTAL_WORKED_DAYS: receivedObj.workingDaysDetailObj.noOfWorkedDays,
            IS_ACTIVE: 1,
            REMARK: receivedObj.remarkDetailObj.remark,
        };

        // insert data into table
        const [resultSaveSettle, fields1] = await conn.query('INSERT INTO hrm_employee_settlement SET ?', settle);
        logger.info('Successfully saved employee settlment record id = ' + resultSaveSettle.insertId);

        // update employee profile table as IS_RESIGNED =1 
        const [resultUpdateEmployee, fields12] = await conn.query("UPDATE hrm_employee_profile SET IS_RESIGNED=1 WHERE ID_EMPLOYEE_PROFILE=" + receivedObj.employeeDetailsObj.profileId + "");

        // insert settle asset collection
        for (var i = 0; i < receivedObj.assetCollectionDetailObj.assetData.length; i++) {
            var assetCollection = {
                ID_EMPLOYEE_SETTLEMENT: resultSaveSettle.insertId,
                ASSET_TYPE_ID: receivedObj.assetCollectionDetailObj.assetData[i].assetType,
                ASSET_TYPE: receivedObj.assetCollectionDetailObj.assetData[i].assetType,
                ASSET_CODE: receivedObj.assetCollectionDetailObj.assetData[i].assetCode,
                ASSET_NAME: receivedObj.assetCollectionDetailObj.assetData[i].assetName,
                SERIAL_NO: receivedObj.assetCollectionDetailObj.assetData[i].serialNo,
                BARCODE_NO: receivedObj.assetCollectionDetailObj.assetData[i].barcodeNo,
                VALUE: receivedObj.assetCollectionDetailObj.assetData[i].value,
                ISSUED_DATE: receivedObj.assetCollectionDetailObj.assetData[i].issuedDate,
                STATUS: receivedObj.assetCollectionDetailObj.assetData[i].status,
                ADDITINAL_REMARK: receivedObj.assetCollectionDetailObj.assetData[i].additinalRemark,
                REMARK: receivedObj.assetCollectionDetailObj.assetData[i].remark,
                IS_ACTIVE: 1
            };

            // insert data into table
            const [resultSaveAssetCollection, fields1] = await conn.query('INSERT INTO hrm_settle_asset_collection SET ?', assetCollection);
            logger.info('Successfully saved employee asset collection record id = ' + resultSaveAssetCollection.insertId);

            //////////////////////////////////////////////////////////////////////////////////////////////////////
            // update asset register table availability = 1
            // get selected data row where asset register id

            if (receivedObj.assetCollectionDetailObj.assetData[i].status == 'return') {

                const [resultAssetRegister, fields2] = await conn.query("select * from hrm_asset_register where IS_ACTIVE=1 and ID_ASSET_REGISTER=" + receivedObj.assetCollectionDetailObj.assetData[i].idAssetRegister + "");

                var assetRegister = {
                    ID_ASSET_TYPE: resultAssetRegister[0].ID_ASSET_TYPE,
                    ID_PARENT_ASSET: resultAssetRegister[0].ID_PARENT_ASSET,
                    ASSET_CODE: resultAssetRegister[0].ASSET_CODE,
                    DESCRIPTION: resultAssetRegister[0].DESCRIPTION,
                    EXTERNAL_DESCRIPTION: resultAssetRegister[0].EXTERNAL_DESCRIPTION,
                    SERIAL_NUMBER: resultAssetRegister[0].SERIAL_NUMBER,
                    ASSOCIATED_PRODUCT_CODE: resultAssetRegister[0].ASSOCIATED_PRODUCT_CODE,
                    BARCODE_OR_NUMBER: resultAssetRegister[0].BARCODE_OR_NUMBER,
                    QUANTITY: resultAssetRegister[0].QUANTITY,
                    PER_UNIT_COST: resultAssetRegister[0].PER_UNIT_COST,
                    IS_HIRED: resultAssetRegister[0].IS_HIRED,
                    TRANSACTION_DATE: resultAssetRegister[0].TRANSACTION_DATE,
                    AVAILABILITY: 1,
                    ID_ASSET_STATUS: resultAssetRegister[0].ID_ASSET_STATUS,
                    IMAGE_URL: resultAssetRegister[0].IMAGE_URL,
                    IS_ACTIVE: 1
                };

                // insert data into table
                const [resultSaveAssetRegister, fields3] = await conn.query('INSERT INTO hrm_asset_register SET ?', assetRegister);
                logger.info('Successfully saved employee asset registation record id = ' + resultSaveAssetRegister.insertId);

                // update asset register table is active = 0
                const [resultUpdateAssetRegister, fields4] = await conn.query("UPDATE hrm_asset_register SET IS_ACTIVE=0 WHERE ID_ASSET_REGISTER=" + receivedObj.assetCollectionDetailObj.assetData[i].idAssetRegister + "");

                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // update asset allocation table return status = 1
                // get selected data row where asset allocation id

                const [resultAssetAllocation, fields5] = await conn.query("select * from hrm_asset_allocation where IS_ACTIVE=1 and ID_ASSET_ALLOCATION=" + receivedObj.assetCollectionDetailObj.assetData[i].idAssetAllocation + "");

                var assetAllocation = {
                    ID_EMPLOYEE_PROFILE: resultAssetAllocation[0].ID_EMPLOYEE_PROFILE,
                    ID_ASSET_REGISTER: resultSaveAssetRegister.insertId,
                    ISSUED_DATE: resultAssetAllocation[0].ISSUED_DATE,
                    ASSET_TYPE: resultAssetAllocation[0].ASSET_TYPE,
                    ASSET_STATUS: resultAssetAllocation[0].ASSET_STATUS,
                    ASSET_VALUE: resultAssetAllocation[0].ASSET_VALUE,
                    RETURNED_DATE: resultAssetAllocation[0].RETURNED_DATE,
                    SERIAL_NUMBER: resultAssetAllocation[0].SERIAL_NUMBER,
                    BAR_CODE_NUMBER: resultAssetAllocation[0].BAR_CODE_NUMBER,
                    ASSET_REMARK: resultAssetAllocation[0].ASSET_REMARK,
                    RETURNED_STATUS: 1,
                    SETTLEMENT_ADDITINAL_REMARK: receivedObj.assetCollectionDetailObj.assetData[i].additinalRemark,
                    SETTLEMENT_STATUS: receivedObj.assetCollectionDetailObj.assetData[i].status,
                    IS_ACTIVE: 1
                };

                // insert data into table
                const [resultSaveAssetAllocation, fields6] = await conn.query('INSERT INTO hrm_asset_allocation SET ?', assetAllocation);
                logger.info('Successfully saved employee asset allocation record id = ' + resultSaveAssetAllocation.insertId);

                // update asset allocation table is active = 0
                const [resultUpdateAssetAllocation, fields7] = await conn.query("UPDATE hrm_asset_allocation SET IS_ACTIVE=0 WHERE ID_ASSET_ALLOCATION=" + receivedObj.assetCollectionDetailObj.assetData[i].idAssetAllocation + "");

            } else {
                logger.info('This asset is not returned ! ');
            }

        }

        // get last inserted data obj for viewing
        const [resultSettleDetail, fields8] = await conn.query("select * from view_settlement_emp where IS_ACTIVE=1 and ID_EMPLOYEE_SETTLEMENT=" + resultSaveSettle.insertId + "");

        await conn.query('COMMIT');
        conn.release();
        logger.info('EMPLOYEE SETTLEMENT TRANSACTION COMPETED SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESS', resultSettleDetail });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('EMPLOYEE SETTLEMENT TRANSACTION FAIL DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get all settled emp details
router.get('/getSettledEmpDetails', async function (req, res) {

    const API_NAME = 'getSettledEmpDetails GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        const [resultData, fields] = await conn.query('select * from view_settlement_emp where IS_ACTIVE=1 order by ID_EMPLOYEE_SETTLEMENT desc');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT SETTLED EMPLOYEE DETAILS SUCCESSFULLY...');
        return res.json({ success: true, resultData });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET SETTLED EMPLOYEE DETAILS DUE TO :- ', err);
        return res.json({ success: false });
    }
});

// get employee settlment details where settlement id
router.post('/getSettledEmpDetailsWhereId', async function (req, res, next) {
    const API_NAME = 'getSettledEmpDetailsWhereId POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get last inserted data obj for viewing
        const [resultSettleDetail, fields] = await conn.query("select * from view_settlement_emp where IS_ACTIVE=1 and ID_EMPLOYEE_SETTLEMENT=" + receivedObj.rowObj.settlmentId + "");

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT EMPLOYEE SETTLEMENT DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultSettleDetail });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('GETTING EMPLOYEE SETTLEMENT DETAILS FAIL DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// delete employee settlment
router.post('/deleteEmployeeSettlment', async function (req, res, next) {
    const API_NAME = 'deleteEmployeeSettlment POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get last inserted data obj for viewing
        const [resultSettleDelete, fields] = await conn.query("update hrm_employee_settlement set IS_ACTIVE=0 where ID_EMPLOYEE_SETTLEMENT=" + receivedObj.rowObj.settlmentId + "");

        await conn.query('COMMIT');
        conn.release();
        logger.info('EMPLOYEE SETTLMENT DELETED SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultSettleDetail });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('EMPLOYEE SETTLMENT DELETED FAIL DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;
var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;

// save shift
router.post('/saveShift', async function (req, res, next) {
    const API_NAME = 'saveShift POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_shift where IS_ACTIVE=1 and SHIFT_NAME='" + receivedObj.shiftName + "'");
        var shift = {
            ID_LOCATION: receivedObj.location,
            SHIFT_NAME: receivedObj.shiftName,
            NO_OF_SUPERVISORS: receivedObj.noOfSupervisor,
            NO_OF_EMPLOYEES: receivedObj.noOfEmployee,
            IS_ACTIVE: 1,
            CREATED_BY: 0,
            REMARK: ''
        };


        if (undefined == receivedObj.idShift || null == receivedObj.idShift || 0 == receivedObj.idShift) {

            // set shift code to insert in new create
            shift.SHIFT_CODE = '';

        } else {
            // update old shift details row
            const [resultUpdateData, fields] = await conn.query("update hrm_shift set IS_ACTIVE=0  where ID_SHIFT=" + receivedObj.idShift + "");
            logger.info('Successfully Updated Shift detail record is_active=0');

            // set shift code to insert in update
            shift.SHIFT_CODE = receivedObj.shiftCode;

        }

        var save = 0;

        if (resultData.length > 0 && (undefined == receivedObj.idShift || null == receivedObj.idShift || 0 == receivedObj.idShift)) {
            await conn.query('COMMIT');
            conn.release();
            logger.info('SHIFT ALREADY EXIST');
            return res.status(200).send({ success: true, message: 'EXIST' });

        } else {

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_shift SET ?', shift);
            logger.info('Successfully saved Shift record id = ' + resultSave.insertId);

            save = resultSave.insertId;

            if (undefined == receivedObj.idShift || null == receivedObj.idShift || 0 == receivedObj.idShift) {

                // update inserted shift code 
                const [resultUpdateData2, fields2] = await conn.query("update hrm_shift set SHIFT_CODE='S" + save + "'  where ID_SHIFT=" + save + "");
                logger.info('Successfully Updated Shift Code is = S' + save);

            } else {

            }
        }

        console.log(receivedObj.departmentsAr);

        // save shift departments
        for (var i = 0; i < receivedObj.departmentsAr.length; i++) {

            // set data to insert
            var department = {
                ID_SHIFT: save,
                ID_DEPARTMENT: 0,
                DEPARTMENT_NAME: receivedObj.departmentsAr[i],
                IS_ACTIVE: 1,
                CREATED_BY: 0,
                REMARK: ''
            }

            // insert data into table
            const [resultSave2, fields3] = await conn.query('INSERT INTO hrm_shift_department SET ?', department);
            logger.info('Successfully saved Shift department record id = ' + resultSave2.insertId);
        }

        await conn.query('COMMIT');
        conn.release();
        logger.info('SHIFT CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('SHIFT CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get Shift details
router.post('/getShifts', async function (req, res, next) {
    const API_NAME = 'getShifts POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get Shifts
        var getQuery = "";

        if (undefined == receivedObj.searchVal || null == receivedObj.searchVal) {
            getQuery = "select * from view_shift_location where IS_ACTIVE=1 order by ID_SHIFT desc";
        } else {
            getQuery = "select * from view_shift_location where IS_ACTIVE=1 and (SHIFT_NAME like '%" + receivedObj.searchVal + "%' or SHIFT_CODE like '%" + receivedObj.searchVal + "%' or LOCATION_NAME like '%" + receivedObj.searchVal + "%') order by ID_SHIFT desc limit 10 offset " + receivedObj.pageSize
        }

        const [resultData, fields] = await conn.query(getQuery);

        const [resultDataCount, fields1] = await conn.query("select count(*) as count_of from view_shift_location where IS_ACTIVE=1 and (SHIFT_NAME like '%" + receivedObj.searchVal + "%' or SHIFT_CODE like '%" + receivedObj.searchVal + "%' or LOCATION_NAME like '%" + receivedObj.searchVal + "%')");

        var resultDataObj = {
            resultData,
            count: resultDataCount[0].count_of
        }
        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT SHIFT DETAILS SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultDataObj });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET SHIFT DETAILS DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get all departments where shift id
router.post('/getDepartmentsWhereShiftId', async function (req, res, next) {
    const API_NAME = 'getDepartmentsWhereShiftId POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();

        await conn.query('START TRANSACTION');

        // get Depertments
        const [resultData, fields] = await conn.query("select * from hrm_shift_department where IS_ACTIVE=1 and ID_SHIFT=" + receivedObj.id + "");

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT DEPAARTMENTS WHERE SHIFT ID SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultData });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET DEPAARTMENTS WHERE SHIFT ID DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});


// delete shift
router.post('/deleteShift', async function (req, res, next) {
    const API_NAME = 'deleteShift POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        // update old shift details row
        const [resultUpdateData, fields] = await conn.query("update hrm_shift set IS_ACTIVE=0 where ID_SHIFT=" + receivedObj.idShift + "");
        logger.info('Successfully Updated Shift detail record ');

        await conn.query('COMMIT');
        conn.release();
        logger.info('SHIFT DELETE TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('SHIFT DELETE TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get day types
router.get('/getDayTypes', async function (req, res) {

    const API_NAME = 'getDayTypes GET, ';
    logger.info(API_NAME + ' called');

    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION')

        const [resultData, fields] = await conn.query('select * from hrm_calendar_day_types where IS_ACTIVE=1');

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT DAY TYPES SUCCESSFULLY...');
        return res.json({ success: true, resultData });

    } catch (err) {
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT DAY TYPES DUE TO :- ', err);
        return res.json({ success: false });
    }

});

// save shift time allocation
router.post('/saveTimeAllocation', async function (req, res, next) {
    const API_NAME = 'saveTimeAllocation POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;
    console.log(receivedObj);
    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        for (var j = 0; j < receivedObj.timeAr.length; j++) {

            // set is_active=0 to the old shift id
            const [resultUpdateData, fields] = await conn.query("update hrm_shift_time_allocation set IS_ACTIVE=0 where ID_SHIFT=" + receivedObj.timeAr[j].shiftId + "");
            logger.info('Successfully Updated Shift time detail record ');

        }

        for (var i = 0; i < receivedObj.timeAr.length; i++) {

            var time = {
                ID_SHIFT: receivedObj.timeAr[i].shiftId,
                DAY: receivedObj.timeAr[i].day,
                START_TIME: receivedObj.timeAr[i].inTime,
                END_TIME: receivedObj.timeAr[i].outTime,
                OT_START: receivedObj.timeAr[i].otStart,
                OT_END: receivedObj.timeAr[i].otEnd,
                LATE_START: receivedObj.timeAr[i].lateStart,
                OT_RATE: receivedObj.timeAr[i].otRate,
                IS_OVER_NIGHT: receivedObj.overNight,
                IS_ACTIVE: 1,
            }

            // insert data into table
            const [resultSave, fields1] = await conn.query('INSERT INTO hrm_shift_time_allocation SET ?', time);
            logger.info('Successfully saved Shift time allocation record id = ' + resultSave.insertId);

        }

        await conn.query('COMMIT');
        conn.release();
        logger.info('SHIFT TIME CREATION TRANSACTION COMPLETLY SUCCESSFULLY...');
        return res.status(200).send({ success: true, message: 'SUCCESSFULLY' });


    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('SHIFT TIME CREATION TRANSACTION FAIL !');
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

// get times where shift id
router.post('/getTimesWhereShiftId', async function (req, res, next) {
    const API_NAME = 'getTimesWhereShiftId POST, ';

    logger.info(API_NAME + ' called');

    var receivedObj = req.body;

    if (!receivedObj) {
        logger.info(API_NAME + ' Parameter(s) not found');
        return res.status(200).send({ success: false, message: 'someAttribute not found' });
    }

    //logic here
    try {
        const conn = await pool.getConnection();
        await conn.query('START TRANSACTION');

        const [resultData, fields] = await conn.query("select * from hrm_shift_time_allocation where IS_ACTIVE=1 and ID_SHIFT=" + receivedObj.id + "");

        await conn.query('COMMIT');
        conn.release();
        logger.info('GOT TIMES WHERE SHIFT ID SUCCESSFULLY...');
        return res.status(200).send({ success: true, resultData });

    } catch (err) {
        console.log('err : ', err);
        await conn.query('ROLLBACK');
        conn.release();
        logger.error('CANNOT GET TIMES WHERE SHIFT ID DUE TO :- ' + err);
        return res.status(200).send({ success: false, message: 'FAILD' });
    }
});

module.exports = router;
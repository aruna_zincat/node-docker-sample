var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


// router.get('/getEmployeeData',async function (req, res, next) {
//     const API_NAME='getEmployeeData get, ';

//     //IMPORTANT
//     //*********WHEN LOG, ALWAYS START WITH `API: {API_NAME}' ` *********

//     //*********always log inside which api*********
//     logger.info(API_NAME + 'called');

//     //*********always check req data before accessing them directly*********
//     /* if(!req.query.employeeProfileId){
//         logger.info(API_NAME + 'Parameter not found');
//         return res.status(200).send({success:false, message:'employeeProfileId not found'});
//     } */

//     //logic here
//     try{
//         const conn = await pool.getConnection();
//             // Do something with the connection
//         var promise1 = conn.query(`
//                 SELECT 
//                     profile.*,
//                     grade.GRADE,
//                     department.DEPARTMENT,
//                     designation.DESIGNATION
//                 FROM
//                     hrm_employee_profile profile
//                         INNER JOIN
//                     hrm_defaults_employee_grade grade
//                         INNER JOIN
//                     hrm_defaults_department department
//                         INNER JOIN
//                     hrm_defaults_employee_designation designation
//                 WHERE
//                     profile.ID_EMPLOYEE_PROFILE = '1'
//         `);

//         //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

//         const values = await Promise.all([promise1]);
//         conn.release();

//         /* const profileResult = values[0][0][0];
//         let officeDetails = {};
//         officeDetails.DESIGNATION = profileResult.DESIGNATION;
//         officeDetails.GRADE = profileResult.GRADE;
//         officeDetails.DEPARTMENT = profileResult.DEPARTMENT;
//         officeDetails.DESIGNATION = profileResult.DESIGNATION;
//         officeDetails.DESIGNATION = profileResult.DESIGNATION; */
        

//         return res.status(200).send({success:true, message1:values[0][0]});
//             //console.log(values);
//     }

//     catch(err){
//         logger.error(API_NAME + 'error :' + err);
//         return res.status(500).send({success:false, message:'Query Error'});
//     }
                
// });

router.get('/getEmployeeOfficeDetails',async function (req, res, next) {
    const API_NAME='getEmployeeOfficeDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            profile.ID_EMPLOYEE_PROFILE,
            profile.ROLE_AND_RESPONSIBILITY,
            grade.GRADE_NAME,
            employment.EMPLOYMENT_NAME,
            department.DEPARTMENT,
            designation.DESIGNATION,
            location.LOCATION_NAME,
            profilejoin.ID_EMPLOYEE_PROFILE as REPORTING_TO,
            profile.IS_LEAVE_ACCEPT_AUTHORIZED
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
                LEFT JOIN
            hrm_defaults_employee_grade grade ON profile.ID_EMPLOYEE_GRADE = grade.ID_EMPLOYEE_GRADE
                LEFT JOIN
                hrm_defaults_department department ON profile.ID_DEPARTMENT = department.ID_DEPARTMENT
                LEFT JOIN
            hrm_defaults_location location ON profile.ID_LOCATION = location.ID_LOCATION
                LEFT JOIN
            hrm_defaults_employee_designation designation ON profile.ID_EMPLOYEE_DESIGNATION = designation.ID_EMPLOYEE_DESIGNATION
				left join
			hrm_employee_employment employment ON profile.ID_EMPLOYMENT = employment.ID_EMPLOYMENT
                left join
            (select profile2.* from hrm_employee_registry registry 
                INNER JOIN
            hrm_employee_profile profile2 ON registry.ID_EMPLOYEE_REGISTRY = profile2.ID_EMPLOYEE_REGISTRY where profile2.IS_ACTIVE=1) profilejoin
            ON profile.REPORTING_TO = profilejoin.ID_EMPLOYEE_REGISTRY
        WHERE
            profile.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        //var promise2 = conn.query('SELECT * FROM core_acc_account_type WHERE ID_ACCOUNT_TYPE=1');

        const values = await Promise.all([promise1]);
        conn.release();

        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});
    
router.get('/getEmployeePersonalDetails',async function (req, res, next) {
    const API_NAME='getEmployeePersonalDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            profile.ID_EMPLOYEE_PROFILE,
            profile.FIRSTNAME,
            profile.MIDDLENAME,
            profile.LASTNAME,
            profile.NIC_NO,
            profile.NIC_ISSUED_DATE,
            profile.DATE_OF_BIRTH,
            profile.NATIONALITY,
            profile.GENDER,
            profile.MARITAL_STATUS,
            profile.DRIVING_LICENSE,
            profile.ETF_NO,
            profile.INCOME_TAX_NO,
            title.TITLE_NAME,
            profile.PASSPORT_NO,
            profile.PASSPORT_COUNTRY,
            profile.INITIALS,
            profile.DISPLAY_NAME
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
                LEFT JOIN
			hrm_defaults_title title ON profile.ID_TITLE = title.ID_TITLE
        WHERE
            profile.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1]);
        conn.release();
        
        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.get('/getEmployeeBankDetails',async function (req, res, next) {
    const API_NAME='getEmployeeBankDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            profile.SALARY_BANK_NAME,
            profile.SALARY_BANK_SWIFTCODE,
            profile.SALARY_BANK_CODE,
            profile.SALARY_BANK_BRANCH_NAME,
            profile.SALARY_BANK_BRANCH_CODE,
            profile.SALARY_BANK_ACCOUNT_NUMBER,
            profile.ALTERNATE_BANK_NAME,
            profile.ALTERNATE_BANK_SWIFTCODE,
            profile.ALTERNATE_BANK_CODE,
            profile.ALTERNATE_BANK_BRANCH_NAME,
            profile.ALTERNATE_BANK_BRANCH_CODE,
            profile.ALTERNATE_BANK_ACCOUNT_NUMBER,
            paytype.PAY_TYPE_NAME,
            acctype.ACCOUNT_TYPE_NAME,
            profile.ACCOUNT_HOLDER_NAME,
            profile.ACCOUNT_HOLDER_RELATIONSHIP
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
                LEFT JOIN 
			hrm_defaults_pay_typs paytype ON profile.PAY_TYPE = paytype.ID_PAY_TYPE
				LEFT JOIN 
			hrm_defaults_account_type acctype ON profile.TYPE_OF_ACCOUNT = acctype.ID_ACCOUNT_TYPE
        WHERE
            profile.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1]);
        conn.release();
        
        return res.status(200).send({success:true, data:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.get('/getEmployeeContactDetails',async function (req, res, next) {
    const API_NAME='getEmployeeContactDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            profile.CURRENT_ADDRESS,
            profile.CURRENT_ADDRESS_COUNTRY_CODE,
            profile.CURRENT_ADDRESS_STATE_PROVINCE,
            profile.CURRENT_ADDRESS_CITY,
            profile.CURRENT_ADDRESS_ZIPCODE,
            profile.PERMANENT_ADDRESS,
            profile.PERMANENT_ADDRESS_COUNTRY_CODE,
            profile.PERMANENT_ADDRESS_STATE_PROVINCE,
            profile.PERMANENT_ADDRESS_CITY,
            profile.PERMANENT_ADDRESS_ZIPCODE,
            profile.MOBILE,
            profile.HOME_TELEPHONE,
            profile.WORK_TELEPHONE,
            profile.EMAIL
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
        WHERE
            profile.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        var promise2 = conn.execute(`
        SELECT 
        emergencyContact.*
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_emergency_contact emergencyContact ON registry1.ID_EMPLOYEE_REGISTRY= emergencyContact.ID_EMPLOYEE_REGISTRY
        WHERE
            emergencyContact.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1,promise2]);
        conn.release();
        
        return res.status(200).send({success:true, contact:values[0][0], emergencyContact: values[1][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

router.get('/getInsuranceDetails',async function (req, res, next) {
    const API_NAME='getInsuranceDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            profile.INSURANCE_NUMBER,
            profile.INSURANCE_NAME,
            CAST(profile.INSURANCE_START_DATE AS CHAR) AS INSURANCE_START_DATE,
            CAST(profile.INSURANCE_EXPIRE_DATE AS CHAR) AS INSURANCE_EXPIRE_DATE
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
        WHERE
            profile.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        var promise2 = conn.execute(`
        SELECT 
            benifitted.RELATIONSHIP,
            benifitted.CARD
        FROM
            hrm_employee_registry registry1
                INNER JOIN
            hrm_employee_profile profile ON registry1.ID_EMPLOYEE_REGISTRY = profile.ID_EMPLOYEE_REGISTRY
                INNER JOIN
            hrm_insurance_benifitted benifitted ON registry1.ID_EMPLOYEE_REGISTRY = benifitted.ID_EMPLOYEE_REGISTRY
        WHERE
            profile.IS_ACTIVE = '1' AND
            benifitted.IS_ACTIVE = '1' AND
            registry1.ID_EMPLOYEE_REGISTRY = ?
            
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1,promise2]);
        conn.release();
        
        return res.status(200).send({success:true, insuranceData:values[0][0], insuranceBenefitted:values[1][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});
    
router.get('/getEmployeeDependantDetails',async function (req, res, next) {
    const API_NAME='getEmployeeDependantDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            *
        FROM
            hrm_employee_dependence dependence
        WHERE
            dependence.IS_ACTIVE = '1' AND
            dependence.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1]);
        conn.release();
        
        return res.status(200).send({success:true, dependantDetails:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});


router.get('/getEmployeeEducationQualificationDetails',async function (req, res, next) {
    const API_NAME='getEmployeeEducationQualificationDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            *
        FROM
            hrm_education_qualification qualification
            INNER JOIN
            hrm_education_qualification_subjects subjects ON qualification.ID_EDUCATION_QUALIFICATION = subjects.ID_EDUCATION_QUALIFICATION
        WHERE
            qualification.IS_ACTIVE = '1' AND
            subjects.IS_ACTIVE = '1' AND
            qualification.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        var promise2 = conn.execute(`
        SELECT 
            *
        FROM
            hrm_higher_education_qualification qualification
        WHERE
            qualification.IS_ACTIVE = '1' AND
            qualification.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1,promise2]);
        conn.release();

        let educationQulification = {};
        for(let i=0;i<values[0][0].length;i++){
            let pushObject = {
                NAME : values[0][0][i].NAME,
                GRADE : values[0][0][i].GRADE
            };
            if(educationQulification[values[0][0].ID_EDUCATION_QUALIFICATION]){
                educationQulification[values[0][0].ID_EDUCATION_QUALIFICATION].push(pushObject);
            }
            else{
                educationQulification[values[0][0].ID_EDUCATION_QUALIFICATION] = [pushObject];
            }
        }
        
        return res.status(200).send(
            {
                success:true,
                educationQulification:educationQulification, 
                highereducationQulification: values[1][0]
            }
        );
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});


router.get('/getEmployeeProffesionalQualificationDetails',async function (req, res, next) {
    const API_NAME='getEmployeeProffesionalQualificationDetails get, ';

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    //*********always check req data before accessing them directly*********
    if(!req.query.employeeProfileId){
        logger.info(API_NAME + 'Parameter not found');
        return res.status(200).send({success:false, message:'employeeProfileId not found'});
    }

    //logic here
    try{
        const conn = await pool.getConnection();
            // Do something with the connection
        var promise1 = conn.execute(`
        SELECT 
            *
        FROM
            hrm_proffessional_qualification qualification
        WHERE
            qualification.IS_ACTIVE = '1' AND
            qualification.ID_EMPLOYEE_REGISTRY = ?
        `, [req.query.employeeProfileId]);

        const values = await Promise.all([promise1]);
        conn.release();
        
        return res.status(200).send({success:true, proffesionalQualifiactions:values[0][0]});
    }

    catch(err){
        logger.error(API_NAME + 'error :' + err);
        return res.status(500).send({success:false, message:'Query Error'});
    }
});

module.exports = router;
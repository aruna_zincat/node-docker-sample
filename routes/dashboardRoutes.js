var express = require('express');
var router = express.Router();

var pool = require('../config/database').pool;


router.get('/getTotalEmployees', async function (req, res, next) {
    const API_NAME = 'getTotalEmployees GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = ` SELECT count(*) as TOTAL FROM hrm_employee_profile WHERE IS_RESIGNED = '0' AND IS_ACTIVE = '1'`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, total: values[0][0][0].TOTAL });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});

router.get('/getPresentToday', async function (req, res, next) {
    const API_NAME = 'getPresentToday GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = `SELECT COUNT(*) AS ACCEPTED_ATTENDANCE FROM hrm_attendance_synced WHERE STATUS='1' AND DATE=substring(sysdate(),1,10);`;
        var sql2 = `SELECT COUNT(*) AS PENDING_ATTENDANCE FROM hrm_attendance_synced WHERE STATUS='0' AND DATE=substring(sysdate(),1,10);`;
        logger.debug(sql);
        logger.debug(sql2);

        var promise1 = conn.execute(sql);
        var promise2 = conn.execute(sql2);

        const values = await Promise.all([promise1,promise2]);
        logger.debug(promise1.sql);
        logger.debug(promise2.sql2);
        conn.release();

        return res.status(200).send({ success: true, total_accepted: values[0][0][0].ACCEPTED_ATTENDANCE,total_pending :values[1][0][0].PENDING_ATTENDANCE });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getOnLeaves', async function (req, res, next) {
    const API_NAME = 'getOnLeaves GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = `SELECT COUNT(*) as LEAVES FROM hrm_leave_request WHERE HR_DEP_STATUS='approved' AND HOD_STATUS='approved' AND substring(sysdate(),1,10) BETWEEN START_DATE AND END_DATE;`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, leaves: values[0][0][0].LEAVES });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getLate', async function (req, res, next) {
    const API_NAME = 'getLate GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = `SELECT COUNT(*) AS LATE FROM hrm_attendance_synced WHERE DATE=substring(sysdate(),1,10) AND LATE_HOURS>'00:00:00';`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, late: values[0][0][0].LATE });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getTodayBirthday', async function (req, res, next) {
    const API_NAME = 'getTodayBirthday GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql = `SELECT * FROM hrm_employee_profile WHERE substring(DATE_OF_BIRTH,1,10) = substring(sysdate(),1,10);`;
        logger.debug(sql);

        var promise1 = conn.execute(sql);

        const values = await Promise.all([promise1]);
        logger.debug(promise1.sql);
        conn.release();

        return res.status(200).send({ success: true, birthday: values[0][0] });
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getWeeklyAttendanceCount', async function (req, res, next) {
    const API_NAME = 'getWeeklyAttendanceCount GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql1 = `SELECT COUNT(*) AS WEEKLY_IN_TIME FROM hrm_attendance_synced WHERE WEEK(DATE)=WEEK(sysdate()) AND LATE_HOURS='00:00:00';`;
        logger.debug(sql1);
        var sql2 = `SELECT COUNT(*) AS WEEKLY_LATE FROM hrm_attendance_synced WHERE WEEK(DATE)=WEEK(sysdate()) AND LATE_HOURS>'00:00:00';`;
        logger.debug(sql2);
        var sql3 = `SELECT COUNT(*) AS ABSENT FROM hrm_roster_details WHERE WEEK(substring(DAY,1,10))=WEEK(sysdate()) AND SUBSTRING(DAY,1,10) NOT IN (SELECT DATE FROM hrm_attendance_synced);`;
        logger.debug(sql3);

        var promise1 = conn.execute(sql1);
        var promise2 = conn.execute(sql2);
        var promise3 = conn.execute(sql3);

        const values = await Promise.all([promise1,promise2,promise3]);
        logger.debug(promise1.sql1);
        logger.debug(promise2.sql2);
        logger.debug(promise3.sql3);
        conn.release();

        return res.status(200).send({ success: true, weekly_in_time: values[0][0][0].WEEKLY_IN_TIME, weekly_late: values[1][0][0].WEEKLY_LATE,absent: values[2][0][0].ABSENT});
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});


router.get('/getMonthlyAttendanceCount', async function (req, res, next) {
    const API_NAME = 'getMonthlyAttendanceCount GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql1 = `SELECT COUNT(*) AS MONTHLY_IN_TIME FROM hrm_attendance_synced WHERE substring(DATE,1,7)=substring(sysdate(),1,7) AND LATE_HOURS='00:00:00';`;
        logger.debug(sql1);
        var sql2 = `SELECT COUNT(*) AS MONTHLY_LATE FROM hrm_attendance_synced WHERE substring(DATE,1,7)=substring(sysdate(),1,7) AND LATE_HOURS>'00:00:00';`;
        logger.debug(sql2);
        var sql3 = `SELECT COUNT(*) AS MONTHLY_ABSENT FROM hrm_roster_details WHERE substring(DAY,1,7)=substring(sysdate(),1,7) AND SUBSTRING(DAY,1,10) NOT IN (SELECT substring(DATE,1,10) FROM hrm_attendance_synced);`;
        logger.debug(sql3);

        var promise1 = conn.execute(sql1);
        var promise2 = conn.execute(sql2);
        var promise3 = conn.execute(sql3);

        const values = await Promise.all([promise1,promise2,promise3]);
        logger.debug(promise1.sql1);
        logger.debug(promise2.sql2);
        logger.debug(promise3.sql3);
        conn.release();

        return res.status(200).send({ success: true, monthly_in_time: values[0][0][0].MONTHLY_IN_TIME, monthly_late: values[1][0][0].MONTHLY_LATE,monthly_absent: values[2][0][0].MONTHLY_ABSENT});
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});



router.get('/getYearlyAttendanceCount', async function (req, res, next) {
    const API_NAME = 'getYearlyAttendanceCount GET, ';

    logger.debug(JSON.stringify(req.body));

    //*********always log inside which api*********
    logger.info(API_NAME + 'called');

    try {
        const conn = await pool.getConnection();
        
        // Do something with the connection
        var sql1 = `SELECT COUNT(*) AS YEARLY_IN_TIME FROM hrm_attendance_synced WHERE substring(DATE,1,4)=substring(sysdate(),1,4) AND LATE_HOURS='00:00:00';`;
        logger.debug(sql1);
        var sql2 = `SELECT COUNT(*) AS YEARLY_LATE FROM hrm_attendance_synced WHERE substring(DATE,1,4)=substring(sysdate(),1,4) AND LATE_HOURS>'00:00:00';`;
        logger.debug(sql2);
        var sql3 = `SELECT COUNT(*) AS YEARLY_ABSENT FROM hrm_roster_details WHERE substring(DAY,1,4)=substring(sysdate(),1,4) AND SUBSTRING(DAY,1,10) NOT IN (SELECT substring(DATE,1,10) FROM hrm_attendance_synced);`;
        logger.debug(sql3);

        var promise1 = conn.execute(sql1);
        var promise2 = conn.execute(sql2);
        var promise3 = conn.execute(sql3);

        const values = await Promise.all([promise1,promise2,promise3]);
        logger.debug(promise1.sql1);
        logger.debug(promise2.sql2);
        logger.debug(promise3.sql3);
        conn.release();

        return res.status(200).send({ success: true, yearly_in_time: values[0][0][0].YEARLY_IN_TIME, yearly_late: values[1][0][0].YEARLY_LATE,yearly_absent: values[2][0][0].YEARLY_ABSENT});
    }

    catch (err) {
        logger.error(API_NAME + 'error :' + err);
        logger.error('Error ' + JSON.stringify(err));
        return res.status(500).send({ success: false, message: 'Query Error' });
    }
});




module.exports = router;
var express = require('express');
var router = express.Router();

var mysqlPool = require('../config/database').pool;


router.get('/getMySqlStats', function (req, res, next) {

    logger.info('Inside /getMySqlStats');

    return res.status(200).send({ success: true, stats: 
        {
            connectionLimit :mysqlPool.config.connectionLimit,
            _freeConnections :mysqlPool._freeConnections.length,
            _allConnections :mysqlPool._allConnections.length,
            _acquiringConnections :mysqlPool._acquiringConnections.length
        }
    });
});

module.exports = router;